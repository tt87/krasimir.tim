#!/usr/bin/env bash
cwd=$(pwd)
twd=~/tmp/krasimir.tim

echo deploy start
echo from $cwd 
echo with middle path $twd

if [ ! -d $twd ]; then 
  cd ~/tmp
  git clone https://tt87@bitbucket.org/tt87/krasimir.tim
  cd $twd
  composer install --no-interaction --no-dev --prefer-dist
else
  cd $twd
  rm _ide_helper_models.php
  git pull --force
  composer update --no-interaction --no-dev --prefer-dist
fi
npm install
$twd/gulp default
cp $cwd/paths.php $twd/bootstrap/
cp $cwd/index.php $twd/public/
cp $cwd/../.env.* $twd
php artisan migrate:refresh --seed
sudo -u postgres pg_dump krasimir > $twd/etc/krasimir_db
ssh-keygen -f "/home/tim/.ssh/known_hosts" -R 109.120.162.69
rsync $twd/public/ tt87@109.120.162.69:/var/www/vhosts/22/132916/webspace/httpdocs/qqq/krasimir.su --verbose --recursive --rsh="ssh" --exclude=".git" --copy-links
rsync $twd/ tt87@109.120.162.69:/var/www/vhosts/22/132916/webspace/httpdocs/qqq/dev.krasimir --verbose --recursive --rsh="ssh" --exclude=".git" --copy-links

