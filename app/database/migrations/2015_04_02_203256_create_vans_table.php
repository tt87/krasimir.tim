<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVansTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('vans', function(Blueprint $table){
            $table->increments('id');
            $table->string('address')->nullable();
            $table->string('text')->nullable();
            $table->string('photoBefore')->nullable();
            $table->string('photoAfter')->nullable();
            $table->unsignedInteger('user')->nullable();
            $table->foreign('user')->references('id')->on('users');
            $table->unsignedInteger('problem')->nullable();
            $table->foreign('problem')->references('id')->on('problems');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop("vans");
	}

}
