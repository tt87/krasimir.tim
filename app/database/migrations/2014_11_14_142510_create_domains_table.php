<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDomainsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('domains', function(Blueprint $table) {
            $table->increments('id');
            $table->string('url')->unique();
            $table->string('name')->nullable();
            $table->string('logoName')->nullable();
            $table->unsignedInteger('region');
            $table->foreign('region')->references('id')->on('regions');
            $table->string('tw')->nullable();
            $table->string('vk')->nullable();
            $table->string('fb')->nullable();
            $table->string('lj')->nullable();
            $table->enum('season',['summer', 'winter'])->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('domains');
	}

}
