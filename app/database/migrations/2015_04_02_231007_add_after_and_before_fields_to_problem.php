<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAfterAndBeforeFieldsToProblem extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('problems', function (Blueprint $table) {
            $table->unsignedInteger('photoBefore')->nullable();
            $table->foreign('photoBefore')->references('id')->on('photos');
            $table->unsignedInteger('photoAfter')->nullable();
            $table->foreign('photoAfter')->references('id')->on('photos');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('problems', function (Blueprint $table) {
            $table->dropColumn('photoBefore');
            $table->dropColumn('photoAfter');
        });
	}

}
