<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryGroup extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
    {
        Schema::create('category_groups', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->string('iconUrl')->nullable();
            $table->text('description')->nullable();
            $table->enum('season',['summer', 'winter'])->nullable();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('category_groups');
	}

}
