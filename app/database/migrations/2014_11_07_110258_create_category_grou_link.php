<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryGrouLink extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('category_group_links', function(Blueprint $table)
        {
            $table->increments('id');
            $table->unsignedInteger("category_group_id");
            $table->foreign("category_group_id")->references("id")->on("category_groups");
            $table->unsignedInteger("category_id");
            $table->foreign("category_id")->references("id")->on("categories");
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('category_group_links');
	}

}
