<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOptionsAndLinkTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('options', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->string('tag');
            $table->boolean('isSingleChoice')->default(true);
            $table->boolean('hasCustomValue')->default(true);
            $table->boolean('canBeNull')->default(false);
            $table->string('prefix')->nullable();
            $table->string('suffix')->nullable();
            $table->timestamps();
        });

        Schema::create('category_option_links', function(Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger("option_id");
            $table->foreign("option_id")->references("id")->on("options");
            $table->unsignedInteger("category_id");
            $table->foreign("category_id")->references("id")->on("categories");
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('category_option_links');
        Schema::drop('options');

	}

}
