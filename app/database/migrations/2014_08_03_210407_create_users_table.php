<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('email')->unique()->nullable();
            $table->string('password', 255)->nullable();
            $table->integer('accessLevel')->default(0);
            $table->string('nickname')->unique()->nullable();
            $table->string('name')->nullable();
            $table->string('surname')->nullable();
            $table->string('patronymic')->nullable();
            $table->boolean('hasPatronymic')->default(true);
            $table->string('phone', 20)->nullable();
            $table->string('address')->nullable();
            $table->boolean('sendCopyToEmail')->default(false);
            $table->boolean('onlyEmailAnswers')->default(true);
            $table->boolean('notifyEmailDeadline')->default(true);
            $table->boolean('emailApproved')->default(false);
            $table->double('homeLat')->nullable();
            $table->double('homeLon')->nullable();
            $table->string('remember_token', 100)->nullable();
            $table->string('avatar')->nullable();
            $table->string('vkontakte')->nullable();
            $table->string('twitter')->nullable();
            $table->string('facebook')->nullable();
            $table->string('google')->nullable();
            $table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
