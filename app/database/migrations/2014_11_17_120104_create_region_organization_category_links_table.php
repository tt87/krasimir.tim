<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegionOrganizationCategoryLinksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('reg_org_links', function (Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('region');
            $table->foreign('region')->references('id')->on('regions');
            $table->unsignedInteger('organization');
            $table->foreign('organization')->references('id')->on('organizations');
            $table->timestamps();
        });

        Schema::create('cat_org_links', function (Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('category');
            $table->foreign('category')->references('id')->on('categories');
            $table->unsignedInteger('organization');
            $table->foreign('organization')->references('id')->on('organizations');
            $table->timestamps();
        });

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('reg_org_links');
        Schema::drop('cat_org_links');
	}

}
