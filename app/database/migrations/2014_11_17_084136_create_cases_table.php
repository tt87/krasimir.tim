<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCasesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('problems', function(Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('user');
            $table->foreign('user')->references('id')->on('users');
            $table->double('lat')->nullable();
            $table->double('long')->nullable();
            $table->string('address')->nullable();
            $table->string('optionValues')->nullable();
            $table->unsignedInteger('category');
            $table->foreign('category')->references('id')->on('categories');
            $table->enum('status', array('Created','InProgress','NotRelevant','NeedCheck','Success','Closed'))->default('Created');
            $table->date('deadline')->nullable();
            $table->date('statusChanged')->nullable();
            $table->boolean('remindDeadline')->nullable();
            $table->timestamps();
        });

        Schema::create('region_problem_links', function(Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('region');
            $table->foreign('region')->references('id')->on('regions');
            $table->unsignedInteger('problem');
            $table->foreign('problem')->references('id')->on('problems');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('region_problem_links');
		Schema::drop('problems');
	}

}
