<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('letters', function (Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->text('text');
            $table->text('header');
            $table->text('footer');
            $table->unsignedInteger('problem_id');
            $table->foreign('problem_id')->references('id')->on('problems');
            $table->unsignedInteger('organization_id');
            $table->foreign('organization_id')->references('id')->on('organizations');
            $table->date('answerDeadline')->nullable();
            $table->boolean('remindDeadline')->default(true);
            $table->enum('status',['Draft','Waiting','SendError','Rejection','Promise','Solution','Kissoff'])->default('Draft');;
            $table->timestamps();
            $table->softDeletes();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('letters');
	}

}
