<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModerationRequestTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('moderation_requests', function(Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('sender');
            $table->foreign('sender')->references('id')->on('users');
            $table->unsignedInteger('receiver')->nullable();
            $table->foreign('receiver')->references('id')->on('users');
            $table->unsignedInteger('problem');
            $table->foreign('problem')->references('id')->on('problems');
            $table->boolean('isActive')->default(true);
            $table->boolean('isProblemClosed')->default(false);
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('moderation_requests');
	}

}
