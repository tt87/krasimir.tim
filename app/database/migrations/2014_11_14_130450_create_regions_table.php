<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('regions', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('osmTag')->nullable();
            $table->string('osmValue')->nullable();
            $table->integer('osmLevel')->nullable();
            $table->integer('govNumber')->nullable();
            $table->double('centerLat')->nullable();
            $table->double('centerLong')->nullable();
            $table->integer('zoom')->default(12);
            $table->timestamps();
            $table->softDeletes();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop("regions");
	}

}
