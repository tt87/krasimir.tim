<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class CategorySeeder extends Seeder
{
    private $categories = [
        ['name' => 'Незаконная торговля алкоголем', 'groups' => ['advertisement']],
        ['name' => 'Ларьки на колёсах', 'groups' => ['advertisement']],
        ['name' => 'Сомнительные торговые павильоны', 'groups' => ['advertisement']],
        ['name' => 'Летние веранды ресторанов', 'groups' => ['pedestrian','advertisement']],
        ['name' => 'Крупногабаритный мусор', 'groups' => ['waste']],
        ['name' => 'Незаконная свалка', 'groups' => ['waste']],
        ['name' => 'Урны для мусора', 'groups' => ['waste']],
        ['name' => 'Контейнеры для дог-пакетов', 'groups' => ['waste']],
        ['name' => 'Загрязнённый водоём', 'groups' => ['waste']],
        ['name' => 'Замусоренная территория', 'groups' => ['waste']],
        ['name' => 'Уличная грязь', 'groups' => ['transport', 'pedestrian', 'waste', 'winter']],
        ['name' => 'Люки', 'groups' => ['pedestrian', 'transport']],
        ['name' => 'Площадки для выгула собак', 'groups' => ['green', 'rest']],
        ['name' => 'Спортивные площадки', 'groups' => ['rest']],
        ['name' => 'Детские площадки', 'groups' => ['rest']],
        ['name' => 'Ограждения', 'groups' => ['pedestrian', 'green']],
        ['name' => 'Безбарьерная среда', 'groups' => ['pedestrian']],
        ['name' => 'Препятствия на тротуаре', 'groups' => ['pedestrian']],
        ['name' => 'Отсутствие тротуара', 'groups' => ['pedestrian']],
        ['name' => 'Повреждение тротуара', 'groups' => ['pedestrian']],
        ['name' => 'Повреждение дорожного покрытия', 'groups' => ['transport']],
        ['name' => 'Лужи', 'groups' => ['transport', 'pedestrian', 'green']],
        ['name' => 'Парковка на газоне', 'groups' => ['transport', 'green']],
        ['name' => 'Парковка на тротуаре', 'groups' => ['pedestrian', 'green']],
        ['name' => 'Парковка с нарушением ПДД', 'groups' => ['transport']],
        ['name' => 'Захват парковки', 'groups' => ['transport']],
        ['name' => 'Светофоры', 'groups' => ['transport', 'pedestrian']],
        ['name' => 'Пешеходные переходы', 'groups' => ['transport', 'pedestrian']],
        ['name' => 'Автохлам', 'groups' => ['transport']],
        ['name' => 'Лежачие полицейские', 'groups' => ['transport']],
        ['name' => 'Дорожная разметка', 'groups' => ['transport']],
        ['name' => 'Дорожные знаки', 'groups' => ['transport']],
        ['name' => 'Остановки общественного транспорта', 'groups' => ['transport']],
        ['name' => 'Козырьки зданий', 'groups' => ['buildings']],
        ['name' => 'Фасады зданий', 'groups' => ['buildings']],
        ['name' => 'Водосточные трубы', 'groups' => ['buildings']],
        ['name' => 'Таблички с номером дома', 'groups' => ['buildings']],
        ['name' => 'Сосульки', 'groups' => ['winter']],
        ['name' => 'Гололедица', 'groups' => ['winter']],
        ['name' => 'Наледь и снег на ступенях', 'groups' => ['winter']],
        ['name' => 'Злоупотребление реагентами', 'groups' => ['winter']],
        ['name' => 'Грязные сугробы на газоне', 'groups' => ['winter']],
        ['name' => 'Неубранный снег', 'groups' => ['winter']],
        ['name' => 'Оголённые провода', 'groups' => ['pedestrian', 'buildings']],
        ['name' => 'Уличное освещение', 'groups' => ['pedestrian', 'buildings']],
        ['name' => 'Отсутствие освещения', 'groups' => ['pedestrian', 'buildings']],
        ['name' => 'Повреждённый столб', 'groups' => ['pedestrian', 'buildings']],
        ['name' => 'Протечка крыши', 'groups' => ['buildings']],
        ['name' => 'Проложить велодорожку', 'groups' => ['rest']],
        ['name' => 'Установить велопарковку', 'groups' => ['rest']],
        ['name' => 'Отреставрировать памятник', 'groups' => ['buildings']],
        ['name' => 'Очистить памятник', 'groups' => ['buildings']],
    ];

    public function run()
    {
        DB::table('categories')->delete();

        $categoryGroups = [];

        foreach ($this->categories as $category) {
            $groups = $category['groups'];
            unset($category['groups']);
            $category = Category::create($category);

            foreach ($groups as $groupName) {
                /**
                 * @var $group CategoryGroup
                 */
                $group = isset($categoryGroups[$groupName]) ? $categoryGroups[$groupName] : null;

                if (!isset($group)) {
                    $group = CategoryGroup::where('slug', $groupName)->first();
                    $categoryGroups[$groupName] = $group;
                }
                $group->categories()->save($category);
            }
        }
    }

}
