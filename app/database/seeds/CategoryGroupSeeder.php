<?php


class CategoryGroupSeeder extends Seeder
{
    private $categoryGroups = [
        [
            'name' => 'Пешеходная среда',
            'slug' => 'pedestrian',
            'iconUrl' => '/images/categories/category-1.png',
        ],
        [
            'name' => 'Дороги и транспорт',
            'slug' => 'transport',
            'iconUrl' => '/images/categories/category-2.png',
        ],
        [
            'name' => 'Реклама и торговля',
            'slug' => 'advertisement',
            'iconUrl' => '/images/categories/category-3.png',
        ],
        [
            'name' => 'Отдых и спорт',
            'slug' => 'rest',
            'iconUrl' => '/images/categories/category-4.png',
        ],
        [
            'name' => 'Мусор и загрязнения',
            'slug' => 'waste',
            'iconUrl' => '/images/categories/category-5.png',
        ],
        [
            'name' => 'Зелёные зоны',
            'slug' => 'green',
            'iconUrl' => '/images/categories/category-6.png',
        ],
        [
            'name' => 'Здания и памятники',
            'slug' => 'buildings',
            'iconUrl' => '/images/categories/category-7.png',
        ],
        [
            'name' => 'Зимние проблемы',
            'slug' => 'winter',
            'iconUrl' => '/images/categories/category-8.png',
        ],
    ];

    public function run()
    {
        DB::table('category_groups')->delete();

        foreach ($this->categoryGroups as $categoryGroup) {
            CategoryGroup::create($categoryGroup);
        }
    }
}
