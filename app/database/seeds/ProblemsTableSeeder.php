<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class ProblemsTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create('ru_RU');
		foreach(range(1, 10) as $index)
		{
			Problem::create([
                'user' => $faker->randomDigit + 1,
                'lat' => $faker->randomFloat(),
                'long' => $faker->randomFloat(),
                'address'=> $faker->address,
                'category' => $faker->randomNumber()%4 +1,

			]);
		}
	}

}