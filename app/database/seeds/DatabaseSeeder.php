<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();
		$this->call('CategoryGroupSeeder');
        $this->call('CategorySeeder');
        $this->call('RegionsTableSeeder');
        $this->call('UserSeeder');
        $this->call('OrganizationSeeder');
        $this->call('OptionsTableSeeder');

        $this->call('DomainsTableSeeder');
        $this->call('ProblemsTableSeeder');
        $this->call('VanTableSeeder');
	}
}





