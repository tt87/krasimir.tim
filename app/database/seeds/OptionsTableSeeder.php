<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class OptionsTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

        $q1 = Option::create([ "tag" => 'date', 'name' => 'date']);
        $q2 = Option::create([ "tag" => 'time', 'name' => 'time']);
        $q3 = Option::create([ "tag" => 'auto', 'name' => 'auto']);
        $q4 = Option::create([ "tag" => 'cover', 'name' => 'cover']);

        Category::find(4)->options()->save($q1);
        Category::find(4)->options()->save($q2);
        Category::find(4)->options()->save($q3);
        Category::find(3)->options()->save($q4);
        Category::find(2)->options()->save($q4);

        (OptionValue::create(['value' => 'asphalt', 'option' => $q4->id]));
        (OptionValue::create(['value' => 'concrete', 'option' => $q4->id]));
        (OptionValue::create(['value' => 'curb', 'option' => $q4->id]));
	}

}