<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class VanTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create('ru_RU');

        Van::create([
            'address' => $faker->address,
            'text' => 'Убрали площадку после ремонта',
            'photoBefore' => "http://krasimir.su/photos/test/vanBefore.JPG",
            'photoAfter' => "http://krasimir.su/photos/test/vanAfter.JPG",
            'user' => 2
        ]);

		foreach(range(1, 20) as $index)
		{
            Van::create([
                'address' => $faker->address,
                'text' => 'Убрали площадку после ремонта',
                'photoBefore' => "http://krasimir.su/photos/vanBefore.JPG",
                'photoAfter' => "http://krasimir.su/photos/vanAfter.JPG",
                'user' => 1
            ]);
		}
	}

}