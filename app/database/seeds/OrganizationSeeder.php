<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class OrganizationSeeder extends Seeder {

	public function run()
	{
        $org = Organization::create([
            'name' => 'УОЖ губернатора Санкт-Петербурга',
            'email' => 'uozh@example.com',
            'letterHeader' => 'Губернатору Санкт-Петербурга',
            'isPrefect' => false,
            'isOfficial' => true,
            'isHidden' => false,
            'isActive' => false,
            'isRegionDefault' => true,
            'description' => 'Управление по обращениям и жалобам губернатора Санкт-Петербурга',
            'address' => 'Смольный',
            'phone' => '+79832569944'
        ]);
        $org->categories()->save(Region::find(1));


        $org = Organization::create([
            'name' => 'Комитет по благоустройству',
            'email' => 'kb@example.com',
            'letterHeader' => 'В Комитет по благоустройству',
            'isPrefect' => false,
            'isOfficial' => true,
            'isHidden' => false,
            'isActive' => false,
            'isRegionDefault' => false,
            'description' => 'Комитет по благоустройству',
            'address' => 'Смольный, к1',
            'phone' => '+79832569955'
        ]);
        $org->categories()->saveMany(Category::whereIn('id', [1,5])->get()->all());
        $org->categories()->save(Region::find(1));


        Organization::create([
            'name' => 'КРТИ',
            'email' => 'krti@example.com',
            'letterHeader' => 'В Комитет по развитию транспортнной инфраструктуры',
            'isPrefect' => false,
            'isOfficial' => true,
            'isHidden' => false,
            'isActive' => false,
            'isRegionDefault' => false,
            'description' => 'Комитет по развитию транспортнной инфраструктуры',
            'address' => 'Смольный, к2',
            'phone' => '+79832569966'
        ]);
        $org->categories()->saveMany(Category::whereIn('id', [2,3])->get()->all());
        $org->categories()->save(Region::find(1));

        Organization::create([
            'name' => 'УОЖ губернатора ЛО',
            'email' => 'uozhlo@example.com',
            'letterHeader' => 'Губернатору ЛО',
            'isPrefect' => false,
            'isOfficial' => true,
            'isHidden' => false,
            'isActive' => false,
            'isRegionDefault' => true,
            'description' => 'Управление по обращениям и жалобам губернатора Ленинградской области',
            'address' => 'Смольный, к3',
            'phone' => '+79832568844'
        ]);
        $org->categories()->save(Region::find(4));

        Organization::create([
            'name' => 'УОЖ главы Петроградского района',
            'email' => 'petradm@example.com',
            'letterHeader' => 'Главе Петроградского района',
            'isPrefect' => false,
            'isOfficial' => true,
            'isHidden' => false,
            'isActive' => false,
            'isRegionDefault' => true,
            'description' => 'Управление по обращениям и жалобам Петроградского района',
            'address' => 'Каменноостровский проспект',
            'phone' => '+71111111'
        ]);
        $org->categories()->save(Region::find(2));

        Organization::create([
            'name' => 'УОЖ главы Выборского района',
            'email' => 'admvyb@example.com',
            'letterHeader' => 'Главе Выборского района',
            'isPrefect' => false,
            'isOfficial' => true,
            'isHidden' => false,
            'isActive' => false,
            'isRegionDefault' => true,
            'description' => 'Управление по обращениям и жалобам Выборского района',
            'address' => 'Большой Сампсониевксий, д.13',
            'phone' => '+73333333'
        ]);
        $org->categories()->save(Region::find(3));

        Organization::create([
            'name' => 'Комитет благоустройства Петроградского района',
            'email' => 'kbpetr@example.com',
            'letterHeader' => 'В Комитет по благоустройству Петроградского района',
            'isPrefect' => false,
            'isOfficial' => true,
            'isHidden' => false,
            'isActive' => false,
            'isRegionDefault' => false,
            'description' => 'Комитет по благоустройству Петроградского района Санкт-Петербурга',
            'address' => 'Монетная, 17',
            'phone' => '+76666666'
        ]);
        $org->categories()->saveMany(Category::whereIn('id', [1,5])->get()->all());
        $org->categories()->save(Region::find(2));

//
//        $faker = Faker::create('ru_RU');
//		foreach(range(1, 10) as $index)
//		{
//			Organization::create([
//
//			]);
//		}
	}

}