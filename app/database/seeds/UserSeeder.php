<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class UserSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();
        User::create(array(
            'nickname' => 'tim',
            'email' => 'tchurovtim@gmail.com',
            'password' => Hash::make('12345'),
            'name' => 'Тимофей',
            'surname' => 'Чуров',
            'patronymic' => 'Николаевич',
            'accessLevel' => 10
        ));

        User::create(array(
            'nickname' => 'user1',
            'email' => 'user1@example.com',
            'password' => Hash::make('user1'),
            'name' => 'Гыук',
            'surname' => 'Гыуков',
            'patronymic' => 'Гыукович',
            'accessLevel' => User::CommonUserLevel
        ));

        User::create(array(
            'nickname' => 'user2',
            'email' => 'user2@example.com',
            'password' => Hash::make('user2'),
            'name' => 'Юзер',
            'surname' => 'Юзерин',
            'patronymic' => 'Юзерович',
            'accessLevel' => User::CommonUserLevel
        ));

        User::create(array(
            'nickname' => 'root',
            'email' => 'root@example.com',
            'password' => Hash::make('root'),
            'name' => 'Рут',
            'surname' => 'Рутов',
            'patronymic' => 'Рутович',
            'accessLevel' => User::RootLevel
        ));


        $faker = Faker::create('ru_RU');
        for ($i = 0; $i < 10; $i++)
        {
            $fn = $faker->name;
            User::create(array(
              //  'nickname' => $faker->userName,
                'email' => $faker->email,
                'password' => Hash::make($faker->word),
                'name' => $fn,
                'surname' => $faker->lastName,
                'patronymic' => $faker->name,
                'accessLevel' => 0,
            ));
        }
    }
}