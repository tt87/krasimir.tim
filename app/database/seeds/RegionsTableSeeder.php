<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class RegionsTableSeeder extends Seeder {

	public function run()
	{
        Region::create([
            'name' => 'Санкт-Петербург',
            'osmTag' => 'state',
            'osmValue' => 'Saint Petersburg',
            'osmLevel' => 2,
            'zoom' => 12,
            'centerLat' => 59.9705253,
            'centerLong' => 30.3648796
        ]);

        Region::create([
            'name' => 'Петроградский район',
            'osmTag' => 'state_district',
            'osmValue' => 'Петроградский район',
            'osmLevel' => 4,
            'zoom' => 13,
            'centerLat' => 59.96224995,
            'centerLong' => 30.30714433
        ]);

        Region::create([
            'name' => 'Выборгский район',
            'osmTag' => 'state_district',
            'osmValue' => 'Выборгский район',
            'osmLevel' => 4,
            'zoom' => 12,
            'centerLat' => 59.9705253,
            'centerLong' => 30.3648796
        ]);

        Region::create([
            'name' => 'Leningrad oblast',
            'osmTag' => 'state',
            'osmValue' => 'Leningrad oblast',
            'osmLevel' => 4,
            'zoom' => 12,
            'centerLat' => 59.956723,
            'centerLong' => 30.691014
        ]);

	}

}