<?php

/**
 * OptionValue
 *
 * @property integer $id
 * @property integer $option
 * @property string $value
 * @method static \Illuminate\Database\Query\Builder|\OptionValue whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\OptionValue whereOption($value)
 * @method static \Illuminate\Database\Query\Builder|\OptionValue whereValue($value)
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\OptionValue whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OptionValue whereUpdatedAt($value)
 */
class OptionValue extends \Eloquent
{
    protected $guarded = ['id'];
    protected $table = 'optionValues';
    protected $visible = ["value"];

    function getOption()
    {
        return $this->belongsTo('Option', 'option');
    }

}