<?php

/**
 * Domain
 *
 * @property integer $id
 * @property string $name
 * @property string $logoUrl
 * @property integer $region
 * @property integer $neighbour
 * @property string $twitter
 * @property string $vkontakte
 * @property string $facebook
 * @property string $livejournal
 * @method static \Illuminate\Database\Query\Builder|\Domain whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Domain whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Domain whereLogoUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\Domain whereRegion($value)
 * @method static \Illuminate\Database\Query\Builder|\Domain whereNeighbour($value)
 * @method static \Illuminate\Database\Query\Builder|\Domain whereTwitter($value)
 * @method static \Illuminate\Database\Query\Builder|\Domain whereVkontakte($value)
 * @method static \Illuminate\Database\Query\Builder|\Domain whereFacebook($value)
 * @method static \Illuminate\Database\Query\Builder|\Domain whereLivejournal($value)
 * @property string $deleted_at
 * @method static \Illuminate\Database\Query\Builder|\Domain whereDeletedAt($value)
 * @property string $url
 * @property string $logoName
 * @property string $tw
 * @property string $vk
 * @property string $fb
 * @property string $lj
 * @property string $season
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\User[] $admins
 * @method static \Illuminate\Database\Query\Builder|\Domain whereUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\Domain whereLogoName($value)
 * @method static \Illuminate\Database\Query\Builder|\Domain whereTw($value)
 * @method static \Illuminate\Database\Query\Builder|\Domain whereVk($value)
 * @method static \Illuminate\Database\Query\Builder|\Domain whereFb($value)
 * @method static \Illuminate\Database\Query\Builder|\Domain whereLj($value)
 * @method static \Illuminate\Database\Query\Builder|\Domain whereSeason($value)
 * @method static \Illuminate\Database\Query\Builder|\Domain whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Domain whereUpdatedAt($value)
 */
class Domain extends \Eloquent
{
    protected $guarded = array('id', 'created_at', 'updated_at', 'deleted_at');
    protected $table = 'domains';

    public function getRegion()
    {
        return $this->belongsTo('Region', 'region');
    }

    public function admins()
    {
        return $this->belongsToMany('User', 'admins', 'domain', 'user')->withTimestamps();
    }
}