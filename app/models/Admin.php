<?php

/**
 * Admin
 *
 * @property integer $id
 * @property integer $user
 * @property integer $domain
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Admin whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Admin whereUser($value)
 * @method static \Illuminate\Database\Query\Builder|\Admin whereDomain($value)
 * @method static \Illuminate\Database\Query\Builder|\Admin whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Admin whereUpdatedAt($value)
 */
class Admin extends \Eloquent
{
    protected $guarded = array('id', 'created_at', 'updated_at');
}