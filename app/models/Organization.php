<?php


/**
 * Organization
 *
 * @property integer $id 
 * @property string $name 
 * @property string $email 
 * @property string $letterHeader 
 * @property boolean $isPrefect 
 * @property boolean $isOfficial 
 * @property boolean $isHidden 
 * @property boolean $isActive 
 * @property string $description 
 * @property string $address 
 * @property string $phone 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @property string $deleted_at 
 * @property-read \Illuminate\Database\Eloquent\Collection|\Region[] $regions 
 * @property-read \Illuminate\Database\Eloquent\Collection|\Category[] $categories 
 * @property-read \Illuminate\Database\Eloquent\Collection|\Letter[] $letters 
 * @method static \Illuminate\Database\Query\Builder|\Organization whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Organization whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Organization whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\Organization whereLetterHeader($value)
 * @method static \Illuminate\Database\Query\Builder|\Organization whereIsPrefect($value)
 * @method static \Illuminate\Database\Query\Builder|\Organization whereIsOfficial($value)
 * @method static \Illuminate\Database\Query\Builder|\Organization whereIsHidden($value)
 * @method static \Illuminate\Database\Query\Builder|\Organization whereIsActive($value)
 * @method static \Illuminate\Database\Query\Builder|\Organization whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\Organization whereAddress($value)
 * @method static \Illuminate\Database\Query\Builder|\Organization wherePhone($value)
 * @method static \Illuminate\Database\Query\Builder|\Organization whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Organization whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Organization whereDeletedAt($value)
 */
class Organization extends \Eloquent
{
    protected $guarded = array('id', 'created_at', 'updated_at', 'deleted_at');
    protected $table = 'organizations';

    function regions()
    {
        return $this->belongsToMany('Region', 'reg_org_links', 'organization', 'region')->withTimestamps();
    }

    function categories()
    {
        return $this->belongsToMany('Category', 'cat_org_links', 'organization', 'category')->withTimestamps();
    }

    function letters()
    {
        return $this->hasMany('Letter', 'organization_id');
    }

    function syncLinks($regs, $cats)
    {
        $this->regions()->sync($regs);
        $this->categories()->sync($cats);
    }

    function attachLinks($regs, $cats)
    {
        $this->regions()->attach($regs);
        $this->categories()->attach($cats);
    }

    //todo make one-query function
    public static function searchOrganizationWithCat($lat, $long, $cat)
    {
        $collection = Region::searchRegions($lat, $long)->get()->all();
        $regs = array_map(function ($x) { return $x->id; }, $collection);
        $res = Organization::select('organizations.*')->join('reg_org_links', 'organizations.id', '=', 'reg_org_links.organization')
            ->whereIn('reg_org_links.region', $regs);
        $res = $res->leftJoin('cat_org_links', 'organizations.id', '=', 'cat_org_links.organization')
            ->where(function($query) use ($cat) {
                $query->where('cat_org_links.category', $cat->id)->orWhere('organizations.isRegionDefault', true);
            });
        return $res;
    }

    //todo make one-query function
    public static function searchOrganization($lat, $long)
    {
        $collection = Region::searchRegions($lat, $long)->get()->all();
        $regs = array_map(function ($x) { return $x->id; }, $collection);
        $res = Organization::select('organizations.*')->join('reg_org_links', 'organizations.id', '=', 'reg_org_links.organization')
            ->whereIn('reg_org_links.region', $regs);
        return $res;
    }

}