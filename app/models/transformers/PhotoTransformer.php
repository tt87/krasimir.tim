<?php
;

/**
 * Created by PhpStorm.
 * User: tim
 * Date: 04.01.15
 * Time: 14:49
 */
class PhotoTransformer extends \League\Fractal\TransformerAbstract
{
    protected $availableIncludes = [
        //  'options', 'option_values'
    ];

    protected $defaultIncludes = [
        //  'options', 'option_values'
    ];


    public function transform(Photo $photo)
    {
        return [
            'id' => (int)$photo->id,
            'owner' => (String)$photo->owner->nickname,
            'owner_id' => (int)$photo->owner->id,
            'case' => (int)$photo->problem_id,
            'name' => (String)$photo->name,
            'path' => (String)$photo->path,
            'is_in_album' => (Boolean)$photo->approved,
            'is_before' => (Boolean)$photo->before,
            'is_after' => (Boolean)$photo->after,
            'uploaded' => (String)($photo->created_at),
        ];
    }

    static function transform_back(Photo $problem, $data)
    {
//todo not implemented yet
        return $problem;
    }


}