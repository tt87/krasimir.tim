<?php

/**
 * Created by PhpStorm.
 * User: tim
 * Date: 04.04.15
 * Time: 20:32
 */
class VanTransformer extends \League\Fractal\TransformerAbstract
{

    protected $availableIncludes = [
        'owner',
    ];

    protected $defaultIncludes = [
        'owner'
    ];


    public function transform(Van $van)
    {
        $res = [
            'id' => (int) $van->id,
            'address' => (String)$van->address,
            'text' => (String)$van->text,
            'photo_before' => (String)$van->photoBefore,
            'photo_after' => (String)$van->photoAfter,
            'created' => (String)($van->created_at),
            'changed' => (String)($van->updated_at),
            'case' => (int)($van->problem)
        ];

        return $res;
    }

    public function includeOwner(Van $van)
    {
        return $this->item($van->owner, new PublicUserTransformer());
    }

    static function transform_back(Van $van, $data)
    {
        if (array_key_exists('address', $data))
            $van->address = $data["address"];

        if (array_key_exists('text', $data))
            $van->text = $data["text"];

        return $van;
    }

}