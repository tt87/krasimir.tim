<?php
/**
 * Created by PhpStorm.
 * User: tim
 * Date: 04.02.15
 * Time: 16:10
 */


use League\Fractal\TransformerAbstract;


class PublicUserTransformer extends \League\Fractal\TransformerAbstract
{
    public function transform(User $user)
    {
        $arr = [
            'id' => (int)$user->id,
            'screen_name' => (string)$user->nickname,
        ];

        return $arr;
    }

}