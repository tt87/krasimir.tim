<?php
/**
 * Created by PhpStorm.
 * User: tim
 * Date: 21.05.15
 * Time: 22:14
 */

use League\Fractal\TransformerAbstract;


class PublicLetterTransformer extends TransformerAbstract{

    protected $availableIncludes = [
        'owner', 'organization', 'case'
    ];

    protected $defaultIncludes = [
        'owner',
    ];

    public function transform(Letter $req)
    {
        return [
            'id' => (int)$req->id,
            'text' => (string)$req->text,
            'header' => (string)$req->header,
            'status' => (String)($req->status),
            'remind_deadline' => (Boolean)($req->remindDeadline),
            'deadline' => (String)($req->answerDeadline),
            'is_deleted' => (Boolean)(isset($req->deleted_at)),
            'created' => (String)($req->created_at),
            'changed' => (String)($req->updated_at)
        ];
    }

    public function includeOwner(Letter $letter)
    {
        return $this->item($letter->owner, new PublicUserTransformer());
    }

    public function includeOrganization(Letter $letter)
    {
        return $this->item($letter->organization, new OrganizationTransformer());
    }

    public function includeCase(Letter $letter)
    {
        return $this->item($letter->problem, new ProblemTransformer());
    }
}