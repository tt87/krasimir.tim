<?php
/**
 * Created by PhpStorm.
 * User: tim
 * Date: 04.02.15
 * Time: 16:53
 */


use League\Fractal\TransformerAbstract;


class CategoryGroupTransformer extends TransformerAbstract
{

    protected $availableIncludes = [
        'categories'
    ];

    protected $defaultIncludes = [
        'categories'
    ];

    public function transform(CategoryGroup $cg)
    {
        return [
            'id' => (int)$cg->id,
            'name' => (String)$cg->name,
            'description' => (String)$cg->description,
            'season' => (String)$cg->season,
            'icon_url' => (String)$cg->iconUrl,
            'is_deleted' => (Boolean)(isset($cg->deleted_at)),
        ];
    }

    public function includeCategories(CategoryGroup $cg)
    {
        $categories = $cg->categories;

        return $this->collection($categories, new CategoryTransformer());
    }

}