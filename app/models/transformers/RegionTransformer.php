<?php
/**
 * Created by PhpStorm.
 * User: tim
 * Date: 04.02.15
 * Time: 16:53
 */


use League\Fractal\TransformerAbstract;


class RegionTransformer extends TransformerAbstract
{

    protected $availableIncludes = [
        'organizations'

    ];

    protected $defaultIncludes = [

    ];

    public function transform(Region $reg)
    {
        return [
            'id' => (int)$reg->id,
            'name' => (string)$reg->name,
            'osm_tag' => (string)$reg->osmTag,
            'osm_value' => (string)$reg->osmValue,
            'osm_level' => (int)$reg->osmLevel,
            'government_number' => (int)$reg->govNumber,
            'center_latitude' => (float)$reg->centerLat,
            'center_longitude' => (float)$reg->centerLong,
            'zoom' => (int)$reg->zoom,
            'is_deleted' => (boolean)(isset($reg->deleted_at)),
            'created' => (string)($reg->created_at),
            'changed' => (string)($reg->updated_at)
        ];
    }

    public static function transform_back(Region $reg, $data)
    {
        if (array_key_exists('name', $data)) {
            $reg->name = $data['name'];
        }

        if (array_key_exists('osm_tag', $data)) {
            $reg->osmTag = $data['osm_tag'];
        }

        if (array_key_exists('osm_value', $data)) {
            $reg->osmValue = $data['osm_value'];
        }

        if (array_key_exists('osm_level', $data)) {
            $reg->osmLevel = $data['osm_level'];
        }

        if (array_key_exists('government_number', $data)) {
            $reg->govNumber = $data['government_number'];
        }

        if (array_key_exists('center_latitude', $data)) {
            $reg->centerLat = $data['center_latitude'];
        }

        if (array_key_exists('center_longitude', $data)) {
            $reg->centerLong = $data['center_longitude'];
        }

        if (array_key_exists('zoom', $data)) {
            $reg->zoom = $data['zoom'];
        }

        return $reg;
    }

    public function includeOrganizations(Region $region)
    {
        return $this->collection($region->organizations, new OrganizationTransformer());
    }


}