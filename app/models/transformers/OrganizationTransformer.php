<?php
/**
 * Created by PhpStorm.
 * User: tim
 * Date: 04.02.15
 * Time: 16:53
 */


use League\Fractal\TransformerAbstract;


class OrganizationTransformer extends TransformerAbstract
{

    protected $availableIncludes = [
        'regions', 'cats', 'requests'
    ];

    protected $defaultIncludes = [

    ];

    public function transform(Organization $org)
    {
        return [
            'id' => (int)$org->id,
            'name' => (string)$org->name,
            'email' => (string)$org->email,
            'letter_header' => (string)$org->letterHeader,
            'is_prefect' => (boolean)$org->isPrefect,
            'is_official' => (boolean)$org->isOfficial,
            'is_active' => (boolean)$org->isActive,
            'is_hidden' => (boolean)$org->isHidden,
            'is_region_default' => (boolean) $org->isRegionDefault,
            'description' => (string)$org->description,
            'address' => (string)$org->address,
            'phone' => (string)$org->phone,
            'is_deleted' => (boolean)(isset($org->deleted_at)),
            'created' => (string)($org->created_at),
            'changed' => (string)($org->updated_at)
        ];
    }

    public function includeRegions(Organization $org)
    {
        return $this->collection($org->regions, new RegionTransformer());
    }

    public function includeCats(Organization $org)
    {
        return $this->collection($org->categories, new CategoryTransformer());
    }

    public function includeRequests(Organization $org)
    {
        return $this->collection($org->letters, new LetterTransformer());
    }

    public static function transform_back(Organization $org, $data)
    {
        if (array_key_exists('name', $data)) {
            $org->name = $data['name'];
        }

        if (array_key_exists('email', $data)) {
            $org->email = $data['email'];
        }

        if (array_key_exists('letter_header', $data)) {
            $org->letterHeader = $data['letter_header'];
        }

        if (array_key_exists('is_prefect', $data)) {
            $org->isPrefect = $data['is_prefect'];
        }

        if (array_key_exists('is_official', $data)) {
            $org->isOfficial = $data['is_official'];
        }

        if (array_key_exists('is_active', $data)) {
            $org->isActive = $data['is_active'];
        }

        if (array_key_exists('is_hidden', $data)) {
            $org->isHidden = $data['is_hidden'];
        }

        if (array_key_exists('is_region_default', $data)) {
            $org->isRegionDefault = $data['is_region_default'];
        }

        if (array_key_exists('description', $data)) {
            $org->description = $data['description'];
        }

        if (array_key_exists('address', $data)) {
            $org->address = $data['address'];
        }

        if (array_key_exists('phone', $data)) {
            $org->phone = $data['phone'];
        }

        return $org;
    }


}