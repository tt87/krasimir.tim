<?php
/**
 * Created by PhpStorm.
 * User: tim
 * Date: 04.02.15
 * Time: 18:10
 */


use League\Fractal\TransformerAbstract;


class OptionTransformer extends TransformerAbstract
{

    protected $availableIncludes = [

    ];

    protected $defaultIncludes = [

    ];

    public function transform(Option $option)
    {
        return [
            'id' => (int)$option->id,
            'name' => (String)$option->name,
            'tag' => (String)$option->tag,
            'single_choice' => (Boolean)$option->isSingleChoice,
            'has_custom_option' => (Boolean)$option->hasCustomOption,
            'suffix' => (String)$option->suffix,
            'prefix' => (String)$option->prefix,
        ];
    }

}