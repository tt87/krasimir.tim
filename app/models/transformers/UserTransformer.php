<?php
/**
 * Created by PhpStorm.
 * User: tim
 * Date: 04.01.15
 * Time: 17:08
 */


use League\Fractal\TransformerAbstract;


class UserTransformer extends TransformerAbstract
{

    public function transform(User $user)
    {
        $arr = [
            'id' => (int)$user->id,
            'email' => (string)$user->email,
            'email_checked' => (boolean)$user->emailApproved,
            'screen_name' => (string)$user->nickname,
            'can_send' => (boolean)($user->isConsistent()),
            'first_name' => (string)$user->name,
            'family_name' => (string)$user->surname,
            'address' => (string)$user->address,
            'phone' => (string)$user->phone
        ];

        if ($user->hasPatronymic)
            $arr['patronymic'] = (string)$user->patronymic;
        else
            $arr['has_patronymic'] = (boolean)($user->hasPatronymic);

        //todo - add share additional fields
        return $arr;
    }

    static function transform_back(User $user, $arr)
    {

        if (array_key_exists('email', $arr)) {
            $user->email = $arr['email'];
            $user->emailApproved = false;
        }

        if (array_key_exists('screen_name', $arr)) {
            $user->nickname = $arr['screen_name'];
        }

        if (array_key_exists('first_name', $arr)) {
            $user->name = $arr['first_name'];
        }

        if (array_key_exists('family_name', $arr)) {
            $user->surname = $arr['family_name'];
        }

        if (array_key_exists('has_patronymic', $arr)) {
            $user->hasPatronymic = $arr['has_patronymic'];
        }

        if (array_key_exists('patronymic', $arr)) {
            $user->patronymic = $arr['patronymic'];
            $user->hasPatronymic = true;
        }

        if (array_key_exists('address', $arr)) {
            $user->address = $arr['address'];
        }

        if (array_key_exists('phone', $arr)) {
            $user->phone = $arr['phone'];
        }

        return $user;
    }


} 