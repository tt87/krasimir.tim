<?php
;

/**
 * Created by PhpStorm.
 * User: tim
 * Date: 04.01.15
 * Time: 14:49
 */

use Doctrine\DBAL\Types\DateTimeType;


class ProblemTransformer extends \League\Fractal\TransformerAbstract
{
    protected $availableIncludes = [
        'options', 'option_values', 'photos'
    ];

    protected $defaultIncludes = [
    ];


    public function transform(Problem $problem)
    {
        return [
            'id' => (int)$problem->id,
            'category' => (String)$problem->getCategory->name,
            'owner' => (String)$problem->owner->nickname,
            'owner_id' => (String)$problem->owner->id,
            'can_request' => (boolean)($problem->isReadyForRequest()),
            'latitude' => (Double)$problem->lat,
            'longitude' => (Double)$problem->long,
            'address' => (String)$problem->address,
            'status' => (String)$problem->status,
            'deadline' => (String)$problem->deadline,
            'remind_deadline' => (boolean)$problem->remindDeadline,
            'is_deleted' => (boolean)(isset($problem->deleted_at)),
            'created' => (String)($problem->created_at),
            'changed' => (String)($problem->statusChanged)
        ];
    }

    public function includeOptions(Problem $problem)
    {
        $opts = $problem->getCategory->options;
        return $this->collection($opts, new OptionTransformer);
    }

    public function includeOptionValues(Problem $problem)
    {
        return $this->item($problem, new ValuesTransformer);
    }

    public function includePhotos(Problem $problem)
    {
        return $this->collection($problem->photos, new PhotoTransformer());
    }

    static function transform_back(Problem $problem, $data)
    {

        if (array_key_exists('category', $data)) {
            $problem->category = $data['category'];
        }

        if (array_key_exists('latitude', $data)) {
            $problem->lat = $data['latitude'];
        }

        if (array_key_exists('longitude', $data)) {
            $problem->long = $data['longitude'];
        }

        if (array_key_exists('address', $data)) {
            $problem->address = $data['address'];
        }

        if (array_key_exists('status', $data)) {
            $problem->status = $data['status'];
        }

        if (array_key_exists('deadline', $data)) {
            $problem->deadline = $data['deadline'];
        }

        if (array_key_exists('remind_deadline', $data)) {
            $problem->remindDeadline = $data['remind_deadline'];
        }

        if (array_key_exists('changed', $data)) {
            $problem->statusChanged = $data['changed'];
        }

        if (array_key_exists('option_values', $data)) {
            $arr = array_merge($problem->getOptionValues(), $data['option_values']);
            $problem->setOptionValues($arr);
        }

        return $problem;
    }

    public static function validator($input){
        $validator = Validator::make($input, [
            'category' => 'required|exists:categories,id',
        ]);
        return $validator;
    }

}