<?php
/**
 * Created by PhpStorm.
 * User: tim
 * Date: 21.05.15
 * Time: 22:14
 */

use League\Fractal\TransformerAbstract;


class LetterTransformer extends TransformerAbstract{

    protected $availableIncludes = [
        'owner', 'organization', 'case'
    ];

    protected $defaultIncludes = [
        'owner',
    ];

    public function transform(Letter $req)
    {
        return [
            'id' => (int)$req->id,
            'status' => (String)($req->status),
            'text' => (string)$req->text,
            'header' => (string)$req->header,
            'footer' => (string)$req->footer,
            'remind_deadline' => (Boolean)($req->remindDeadline),
            'deadline' => (String)($req->answerDeadline),
            'is_deleted' => (Boolean)(isset($req->deleted_at)),
            'created' => (String)($req->created_at),
            'changed' => (String)($req->updated_at)
        ];
    }

    public function includeOwner(Letter $letter)
    {
        return $this->item($letter->owner, new PublicUserTransformer());
    }

    public function includeOrganization(Letter $letter)
    {
        return $this->item($letter->organization, new OrganizationTransformer());
    }

    public function includeCase(Letter $letter)
    {
        return $this->item($letter->problem, new ProblemTransformer());
    }

    public static function transform_back(Letter $reg, $data){
        if (array_key_exists('user', $data)) {
            $reg->user_id = $data['user'];
        }

        if (array_key_exists('problem', $data)) {
            $reg->problem_id = $data['problem'];
        }

        if (array_key_exists('organization', $data)) {
            $reg->organization_id = $data['organization'];
        }

        if (array_key_exists('text', $data)) {
            $reg->text = $data['text'];
        }

        if (array_key_exists('header', $data)) {
            $reg->header = $data['header'];
        }

        if (array_key_exists('footer', $data)) {
            $reg->footer = $data['footer'];
        }

        if (array_key_exists('status', $data)) {
            $reg->status = $data['status'];
        }

        if (array_key_exists('remind_deadline', $data)) {
            $reg->remindDeadline = $data['remind_deadline'];
        }
        if (array_key_exists('deadline', $data)) {
            $reg->answerDeadline = $data['deadline'];
        }

        return $reg;
    }



}