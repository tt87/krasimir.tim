<?php
/**
 * Created by PhpStorm.
 * User: tim
 * Date: 04.01.15
 * Time: 16:41
 */


use League\Fractal\TransformerAbstract;


class CategoryTransformer extends TransformerAbstract
{

    protected $availableIncludes = [
        'options', 'orgs'
    ];

    protected $defaultIncludes = [

    ];


    public function transform(Category $category)
    {
        return [
            'id' => (int)$category->id,
            'name' => (String)$category->name,
            'description' => (String)$category->description,
            'keywords' => (String)$category->keywords,
            'season' => (String)$category->season,
            'template' => (String)$category->textTemplate,
            'is_deleted' => (Boolean)isset($category->deleted_at),
        ];
    }

    public function includeOptions(Category $cat)
    {
        return $this->collection($cat->options, new OptionTransformer);
    }

    //todo make role based security first
//    public function includeOrgs(Category $category)
//    {
//        return $this->collection($category->organizations, new OrganizationTransformer());
//    }
} 