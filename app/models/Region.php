<?php

/**
 * Region
 *
 * @property integer $id
 * @property string $name
 * @property string $osmTag
 * @property string $osmValue
 * @property integer $osmLevel
 * @property integer $govNumber
 * @property float $centerLat
 * @property float $centerLong
 * @property integer $zoom
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @method static \Illuminate\Database\Query\Builder|\Region whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Region whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Region whereOsmTag($value)
 * @method static \Illuminate\Database\Query\Builder|\Region whereOsmValue($value)
 * @method static \Illuminate\Database\Query\Builder|\Region whereOsmLevel($value)
 * @method static \Illuminate\Database\Query\Builder|\Region whereGovNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\Region whereCenterLat($value)
 * @method static \Illuminate\Database\Query\Builder|\Region whereCenterLong($value)
 * @method static \Illuminate\Database\Query\Builder|\Region whereZoom($value)
 * @method static \Illuminate\Database\Query\Builder|\Region whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Region whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Region whereDeletedAt($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Domain[] $domains
 * @property-read \Illuminate\Database\Eloquent\Collection|\User[] $moderators
 * @property-read \Illuminate\Database\Eloquent\Collection|\Problem[] $problems
 * @property-read \Illuminate\Database\Eloquent\Collection|\Organization[] $organizations
 */
class Region extends \Eloquent
{
    protected $table = 'regions';
    protected $guarded = array('id', 'created_at', 'updated_at', 'deleted_at');

        public function domains()
    {
        return $this->hasMany('Domain', 'region');
    }

    public function moderators()
    {
        return $this->belongsToMany('User', 'moderators', 'region', 'user')->withTimestamps();
    }

    function problems()
    {
        return $this->belongsToMany('Problem', 'region_problem_links', 'region', 'problem')->withTimestamps();
    }

    function organizations()
    {
        return $this->belongsToMany('Organization', 'reg_org_links', 'region', 'organization')->withTimestamps();
    }

    public static function searchRegionsNominatim($lat, $long)
    {
        $location = file_get_contents('http://nominatim.openstreetmap.org/reverse?format=json&accept-language=en-US&lat=' . $lat . '&lon=' . $long);
        $reg = json_decode($location);
        $arr = get_object_vars($reg->address);
        $keys = array_keys($arr);
        $res = Region::latest();
        foreach($keys as $key) {
            $res->orWhere('osmTag',$key)->where('osmValue',$arr[$key]);
        }
        return $res;
    }


    public static function searchRegions($lat, $long)
    {
        $location = file_get_contents('https://api.opencagedata.com/geocode/v1/json?key=545169005bdae0358c1b7de042f8742b&q=' . $lat . ',' . $long . '&no_annotations=1');
        $reg = json_decode($location);
        $arr = get_object_vars($reg->results[0]->components);
        $keys = array_keys($arr);
        $res = Region::latest();
        foreach($keys as $key) {
            $res->orWhere('osmTag',$key)->where('osmValue',$arr[$key]);
        }
        return $res;
    }


}