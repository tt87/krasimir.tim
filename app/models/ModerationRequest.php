<?php

/**
 * ModerationRequest
 *
 * @property integer $id
 * @property integer $sender
 * @property integer $receiver
 * @property integer $problem
 * @property boolean $isActive
 * @property boolean $isProblemClosed
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\ModerationRequest whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\ModerationRequest whereSender($value)
 * @method static \Illuminate\Database\Query\Builder|\ModerationRequest whereReceiver($value)
 * @method static \Illuminate\Database\Query\Builder|\ModerationRequest whereProblem($value)
 * @method static \Illuminate\Database\Query\Builder|\ModerationRequest whereIsActive($value)
 * @method static \Illuminate\Database\Query\Builder|\ModerationRequest whereIsProblemClosed($value)
 * @method static \Illuminate\Database\Query\Builder|\ModerationRequest whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\ModerationRequest whereUpdatedAt($value)
 */
class ModerationRequest extends \Eloquent
{
    protected $guarded = array('id', 'created_at', 'updated_at');
}