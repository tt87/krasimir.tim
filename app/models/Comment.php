<?php

/**
 * Comment
 *
 * @property integer $id
 * @property integer $user
 * @property integer $problem
 * @property string $text
 * @property string $header
 * @property string $enclosureType
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @method static \Illuminate\Database\Query\Builder|\Comment whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Comment whereUser($value)
 * @method static \Illuminate\Database\Query\Builder|\Comment whereProblem($value)
 * @method static \Illuminate\Database\Query\Builder|\Comment whereText($value)
 * @method static \Illuminate\Database\Query\Builder|\Comment whereHeader($value)
 * @method static \Illuminate\Database\Query\Builder|\Comment whereEnclosureType($value)
 * @method static \Illuminate\Database\Query\Builder|\Comment whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Comment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Comment whereDeletedAt($value)
 */
class Comment extends \Eloquent
{
    protected $guarded = array('id', 'created_at', 'updated_at', 'deleted_at');
    protected $table = 'comments';
}