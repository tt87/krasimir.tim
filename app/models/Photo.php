<?php
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Photo
 *
 * @property integer $id
 * @property string $name
 * @property boolean $before
 * @property boolean $after
 * @property boolean $approved
 * @property integer $user_id
 * @property integer $problem_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \User $owner
 * @property-read \Problem $problem
 * @method static \Illuminate\Database\Query\Builder|\Photo whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Photo whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Photo whereBefore($value)
 * @method static \Illuminate\Database\Query\Builder|\Photo whereAfter($value)
 * @method static \Illuminate\Database\Query\Builder|\Photo whereApproved($value)
 * @method static \Illuminate\Database\Query\Builder|\Photo whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Photo whereProblemId($value)
 * @method static \Illuminate\Database\Query\Builder|\Photo whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Photo whereUpdatedAt($value)
 */
class Photo extends \Eloquent
{
    protected $guarded = array('id', 'created_at', 'updated_at');
    protected $table = 'photos';


    function owner()
    {
        return $this->belongsTo('User', 'user_id');
    }

    function problem()
    {
        return $this->belongsTo('Problem', 'problem_id');
    }

    public function getPath()
    {
        return floor($this->problem->id / 1000) . '/' . $this->name;
    }


    public static function Take(UploadedFile $file, Problem $case, User $user, $options)
    {
        $name = self::SaveToStorage($file, $case->id);
        $count = $case->photos()->count();
        $photo = new Photo();
        $photo->user_id = $user->id;
        $photo->problem_id = $case->id;
        $photo->name = $name;
        $photo->path = $photo->getPath();
        $photo->approved = $user->id == $case->owner->id;
        if ($count == 0) $photo->before = true;
        if (array_key_exists('before', $options))
        {
            $photo->before = $options['before'];
        }
        if (array_key_exists('after', $options))
        {
            $photo->after = $options['after'];
        }
        $photo->save();
        if ($photo->before)
            $case->photoBefore = $photo->id;
        if ($photo->after)
            $case->photoAfter = $photo->id;
        return $photo;
    }

    /**
     * @param UploadedFile $file
     * @param Problem $case
     * @return string
     */
    protected static function SaveToStorage(UploadedFile $file, $problemId)
    {
        $t = new DateTime();
        $dir = floor(((int)$problemId) / 1000) . '';
        $name = ((integer)$problemId) . '_' . $t->getTimestamp() . '.' . $file->getExtension();
        Storage::upload($file, $dir . '/' . $name);
        return $name;
    }

}