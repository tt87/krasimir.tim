<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;


/**
 * Category
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $keywords
 * @property string $season
 * @property string $textTemplate
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\CategoryGroup[] $groups
 * @property-read \Illuminate\Database\Eloquent\Collection|\Option[] $options
 * @property-read \Illuminate\Database\Eloquent\Collection|\Problem[] $problems
 * @method static \Illuminate\Database\Query\Builder|\Category whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Category whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Category whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\Category whereKeywords($value)
 * @method static \Illuminate\Database\Query\Builder|\Category whereSeason($value)
 * @method static \Illuminate\Database\Query\Builder|\Category whereTextTemplate($value)
 * @method static \Illuminate\Database\Query\Builder|\Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Category whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Category whereDeletedAt($value)
 */
class Category extends \Eloquent
{
    use SoftDeletingTrait;

    protected $table = "categories";
    protected $guarded = array('id', 'created_at', 'updated_at', 'deleted_at');
    protected $hidden = ["pivot", 'textTemplate', 'created_at', 'updated_at', 'deleted_at'];
    protected $softDelete = true;
    protected $dates = ['deleted_at', 'created_at', 'updated_at'];

    public function groups()
    {
        return $this->belongsToMany('CategoryGroup', 'category_group_links', 'category_id', 'category_group_id')->withTimestamps();
    }

    public function options()
    {
        return $this->belongsToMany('Option', 'category_option_links', 'category_id', 'option_id')->withTimestamps();
    }

    public function problems()
    {
        return $this->hasMany('Problem', 'category');
    }

    function organizations()
    {
        return $this->belongsToMany('Organization', 'cat_org_links', 'category', 'organization')->withTimestamps();
    }

}