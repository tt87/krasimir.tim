<?php


/**
 * CategoryGroup
 *
 * @property integer $id
 * @property string $name
 * @property string $iconUrl
 * @property string $description
 * @property string $season
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Category[] $categories
 * @method static \Illuminate\Database\Query\Builder|\CategoryGroup whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\CategoryGroup whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\CategoryGroup whereIconUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\CategoryGroup whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\CategoryGroup whereSeason($value)
 * @method static \Illuminate\Database\Query\Builder|\CategoryGroup whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\CategoryGroup whereUpdatedAt($value)
 */
class CategoryGroup extends Eloquent
{
    protected $guarded = array('id', 'created_at', 'updated_at');
    protected $table = 'category_groups';
//    protected $hidden = array('pivot');
    protected $dates = ['deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany('Category', 'category_group_links', 'category_group_id', 'category_id')->withTimestamps()->withTrashed();
    }

}