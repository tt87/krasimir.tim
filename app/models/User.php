<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

/**
 * User
 *
 * @property integer $id
 * @property string $email
 * @property string $password
 * @property string $nickname
 * @property string $name
 * @property string $surname
 * @property string $patronymic
 * @property integer $accessLevel
 * @property boolean $hasPatronymic
 * @property string $phone
 * @property string $address
 * @property boolean $sendCopyToEmail
 * @property boolean $onlyEmailAnswers
 * @property boolean $notifyEmailDeadline
 * @property boolean $emailApproved
 * @property float $homeLat
 * @property float $homeLon
 * @property string $remember_token
 * @property string $avatar
 * @property string $vkontakte
 * @property string $twitter
 * @property string $facebook
 * @property string $google
 * @property \Carbon\Carbon $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $region
 * @property-read \Illuminate\Database\Eloquent\Collection|\Region[] $moderationRegions
 * @property-read \Illuminate\Database\Eloquent\Collection|\Domain[] $adminDomains
 * @property-read \Illuminate\Database\Eloquent\Collection|\Problem[] $problems
 * @method static \Illuminate\Database\Query\Builder|\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\User wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereNickname($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereSurname($value)
 * @method static \Illuminate\Database\Query\Builder|\User wherePatronymic($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereHasPatronymic($value)
 * @method static \Illuminate\Database\Query\Builder|\User wherePhone($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereAddress($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereSendCopyToEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereOnlyEmailAnswers($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereNotifyEmailDeadline($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereEmailApproved($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereHomeLat($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereHomeLon($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereAvatar($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereVkontakte($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereTwitter($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereFacebook($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereGoogle($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereRegion($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Photo[] $photos
 * @property-read \Illuminate\Database\Eloquent\Collection|\Letter[] $letters
 * @method static \Illuminate\Database\Query\Builder|\User whereAccessLevel($value)
 */
class User extends Eloquent implements UserInterface, RemindableInterface
{
    use UserTrait, RemindableTrait, SoftDeletingTrait;

    const RootLevel = 10;
    const CiteModeratorLevel = 5;
    const CommonUserLevel = 0;

    protected $dates = ['deleted_at', 'created_at', 'updated_at'];
    protected $table = 'users';
    protected $hidden = array('password', 'remember_token');
    protected $guarded = array('remember_token', 'id', 'created_at', 'updated_at', 'deleted_at');

    public function moderationRegions()
    {
        return $this->belongsToMany('Region', 'moderators', 'user', 'region')->withTimestamps();
    }

    public function adminDomains()
    {
        return $this->belongsToMany('Domain', 'admins', 'user', 'domain')->withTimestamps();
    }

    public function problems()
    {
        return $this->hasMany('Problem', 'user');
    }

    public function photos()
    {
        return $this->hasMany('Photo', 'user_id');
    }

    function letters()
    {
        return $this->hasMany('Letter', 'user_id');
    }

    public function isConsistent()
    {
        $hasFio = User::checkName($this->name) && User::checkName($this->surname)
            && (!$this->hasPatronymic || User::checkName($this->patronymic));
        $hasEmail = isset($this->email) && User::checkEmail($this->email) && $this->emailApproved;
        $hasAddress = isset($this->address) && User::checkAddress($this->email);
        return $hasFio && ($hasEmail || ($hasAddress && !$this->onlyEmailAnswers));
    }

    static function findOrCreateSocialUser($socialService, $socialData)
    {
        $user = User::findUserBySocial($socialService, $socialData->id, isset($socialData->email) ? $socialData->email : null);
        if (!isset($user)) {
            $user = User::createUserFromSocial($socialService, $socialData);
        }
        return $user;
    }

    static function findUserBySocial($socialService, $socialId, $email = null)
    {
        $request = User::where('id'); //hack: it is search for id==null, consequently will find nobody unless next conditions
        if (isset($socialId) && isset($socialService))
            $request = $request->orWhere($socialService, '=', $socialId);
        if (isset($email))
            $request = $request->orWhere('email', '=', $email);
        $users = $request->get();

        // todo possible two users could be found
        //they need to be united, but links to cases and etc should be changed
        if ($users->count() > 0) {
            $user = $users->first();
            return $user;
        }
        return null;
    }

    static function createUserFromSocial($socialService, $socialData)
    {
        if (!isset($socialData->id) || !User::checkSocialService($socialService))
            throw new InvalidArgumentException('Social authorization failed');

        $user = new User;
        $user->$socialService = $socialData->id;
        if (isset($socialData->email) && User::checkEmail($socialData->email)) {
            $user->email = $socialData->email;
            $user->emailApproved = true;
        }

        if (isset($socialData->nickname))
            $user->nickname = trim($socialData->nickname);

        if (isset($socialData->name) && User::checkName($socialData->name))
            $user->name = trim($socialData->name);

        if (isset($socialData->surname) && User::checkName($socialData->surname))
            $user->surname = trim($socialData->surname);

        $user->save(); //todo check for completeness
        return $user;
    }

    static function checkName($namePart)
    {
        $pattern = '/^\p{Zs}*\p{Cyrillic}+\p{Zs}*$/u'; //cyrillic symbols with trailing and heading zeros
        return preg_match($pattern, $namePart) == 1;
    }

    static function checkEmail($email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    static function checkAddress($address)
    {
        return true;//filter_var($address, FILTER_DEFAULT); //todo make filter!
    }

    static function checkSocialService($service)
    {
        return $service == 'twitter' || $service == 'google' ||
        $service == 'facebook' || $service == 'vkontakte';
    }

}
