<?php

/**
 * Letter
 *
 * @property integer $id 
 * @property integer $user_id 
 * @property string $text 
 * @property string $header 
 * @property string $footer 
 * @property integer $problem_id 
 * @property integer $organization_id
 * @property string $answerDeadline 
 * @property boolean $remindDeadline 
 * @property string $status 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @property string $deleted_at 
 * @property-read \Problem $problem 
 * @property-read \User $owner 
 * @property-read \Organization $organization 
 * @method static \Illuminate\Database\Query\Builder|\Letter whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Letter whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Letter whereText($value)
 * @method static \Illuminate\Database\Query\Builder|\Letter whereHeader($value)
 * @method static \Illuminate\Database\Query\Builder|\Letter whereFooter($value)
 * @method static \Illuminate\Database\Query\Builder|\Letter whereProblemId($value)
 * @method static \Illuminate\Database\Query\Builder|\Letter whereOrganizationId($value)
  * @method static \Illuminate\Database\Query\Builder|\Letter whereAnswerDeadline($value)
 * @method static \Illuminate\Database\Query\Builder|\Letter whereRemindDeadline($value)
 * @method static \Illuminate\Database\Query\Builder|\Letter whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\Letter whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Letter whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Letter whereDeletedAt($value)
 */
class Letter extends \Eloquent
{
    protected $guarded = array('id', 'created_at', 'updated_at', 'deleted_at');
    protected $table = 'letters';

    const Draft = 'Draft';
    const Waiting = 'Waiting';
    const SendError = 'SendError';
    const Rejection = 'Rejection';
    const Promise = 'Promise';
    const Solution = 'Solution';
    const Kissoff = 'Kissoff';

    function problem()
    {
        return $this->belongsTo('Problem', 'problem_id');
    }

    function owner()
    {
        return $this->belongsTo('User', 'user_id');
    }

    function organization()
    {
        return $this->belongsTo('Organization', 'organization_id');
    }

    function saveNewData($data, $orgs)
    {
        LetterTransformer::transform_back($this, $data);
        $this->save();
    }

    static function sieve($collection){
        //  var_dump(Request::fullUrl());
        $collection = Letter::filter('updated_at', '>=', Input::get('from'), $collection);
        $collection = Letter::filter('updated_at', '<=', Input::get('to'), $collection);
        $collection = Letter::filterArray('status', Input::get('status'), $collection);
        $collection = Letter::filterArray('organization_id', Input::get('organization'), $collection);
        $collection = Letter::filterRegion(Input::get('region'), $collection);
        $collection = Letter::filterCategory(Input::get('category'), $collection);
        return $collection;
    }

    static function filter($field, $eq, $data, $collection){
        return isset($data) ? $collection->where($field, $eq, $data) : $collection;
    }

    static function filterArray($field, $values, $collection){
        if (!isset($values))
            return $collection;
        $arr = self::getArrFromString($values);
        return $collection->whereIn($field, $arr);
    }

    static function filterRegion($regions, $collection){
        if (!isset($regions))
            return $collection;
        $regions = self::getArrFromString($regions);
        $collection = $collection->join('region_problem_links', 'letters.problem_id', '=', 'region_problem_links.problem')
            ->whereIn('region_problem_links.region', $regions);
        return $collection;
    }

    static function filterCategory($cg, $collection){
        if (!isset($cg))
            return $collection;
        $cgs = self::getArrFromString($cg);
        $collection = $collection->join('problems', 'letters.problem_id', '=', 'problems.id')
            ->whereIn('problems.category', $cgs);
        return $collection;
    }

    protected static function getArrFromString($data)
    {
        $arr1 = explode(',', $data);
        return array_map(function ($x) {return trim($x);}, $arr1);
    }



}