<?php


use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Van
 *
 * @property integer $id
 * @property string $address
 * @property string $text
 * @property string $photoBefore
 * @property string $photoAfter
 * @property \User $user
 * @property \Problem $problem
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Van whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Van whereAddress($value)
 * @method static \Illuminate\Database\Query\Builder|\Van whereText($value)
 * @method static \Illuminate\Database\Query\Builder|\Van wherePhotoBefore($value)
 * @method static \Illuminate\Database\Query\Builder|\Van wherePhotoAfter($value)
 * @method static \Illuminate\Database\Query\Builder|\Van whereUser($value)
 * @method static \Illuminate\Database\Query\Builder|\Van whereProblem($value)
 * @method static \Illuminate\Database\Query\Builder|\Van whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Van whereUpdatedAt($value)
 */
class Van extends \Eloquent
{
    protected $guarded = array('id', 'created_at', 'updated_at', 'user', 'problem');
    protected $table = 'vans';

    const Dir = 'vans/';
    const DefaultCount = 15;



    function owner()
    {
        return $this->belongsTo('User', 'user');
    }

    static function createForProblem($problemId)
    {
        $problem = Problem::find($problemId);
        if ($problem->status != Problem::Success)
            return null;
        $van = new Van();
        $van->problem = $problem->id;
        $van->user = $problem->user;
        $van->address = $problem->address;
        $van->text = $problem->getCategory->description;
        $van->save();

        if (isset($problem->photoAfter)) {
            $van->photoAfter = Van::SaveCopyToStorage($problem->after->path, $van->id , "after");
        }
        if (isset($problem->photoBefore)) {
            $van->photoBefore = Van::SaveCopyToStorage($problem->before->path, $van->id , "before");
        }
        $van->save();
        return $van;
    }

    protected static function SaveCopyToStorage($from, $vanId, $str)
    {
        $arr = explode('.', $from);
        if (sizeof($arr) > 1) $ext = '.' . $arr[1];
        else $ext = "";
        $path = self::CreatePath($vanId, $str, $ext);
        Storage::Copy($from, $path);
        return $path;
    }

    protected static function SaveToStorage(UploadedFile $file, $vanId, $str)
    {
        $path = self::CreatePath($vanId, $str, $file->getExtension());
        Storage::upload($file, $path);
        return $path;
    }

    public static function CheckAndSave($vanId, $str)
    {
        $fileName = 'photo_' . $str;
        if (Input::hasFile($fileName)) {
            return  Van::SaveToStorage(Input::file($fileName), $vanId, $str);
        }
        return null;
    }

    public static function GetRandomCollection($count)
    {
        $count = isset($count)? $count : self::DefaultCount;
        return Van::orderBy(DB::raw('RANDOM()'))->take($count)->get();
    }


    protected static function CreatePath($vanId, $str, $ext)
    {
        $date = new DateTime();
        $path = Van::Dir . $vanId . '_' . $str . '_' . $date->getTimestamp() . $ext;
        return $path;
    }

}