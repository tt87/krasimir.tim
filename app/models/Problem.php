<?php


/**
 * Problem
 *
 * @property integer $id
 * @property integer $user
 * @property float $lat
 * @property float $long
 * @property string $address
 * @property string $optionValues
 * @property integer $category
 * @property string $status
 * @property string $deadline
 * @property string $statusChanged
 * @property boolean $remindDeadline
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \User $owner
 * @property-read \Illuminate\Database\Eloquent\Collection|\Region[] $regions
 * @method static \Illuminate\Database\Query\Builder|\Problem whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Problem whereUser($value)
 * @method static \Illuminate\Database\Query\Builder|\Problem whereLat($value)
 * @method static \Illuminate\Database\Query\Builder|\Problem whereLong($value)
 * @method static \Illuminate\Database\Query\Builder|\Problem whereAddress($value)
 * @method static \Illuminate\Database\Query\Builder|\Problem whereOptionValues($value)
 * @method static \Illuminate\Database\Query\Builder|\Problem whereCategory($value)
 * @method static \Illuminate\Database\Query\Builder|\Problem whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\Problem whereDeadline($value)
 * @method static \Illuminate\Database\Query\Builder|\Problem whereStatusChanged($value)
 * @method static \Illuminate\Database\Query\Builder|\Problem whereRemindDeadline($value)
 * @method static \Illuminate\Database\Query\Builder|\Problem whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Problem whereUpdatedAt($value)
 * @property integer $photoBefore
 * @property integer $photoAfter
 * @property-read \Illuminate\Database\Eloquent\Collection|\Photo[] $photos
 * @method static \Illuminate\Database\Query\Builder|\Problem wherePhotoBefore($value)
 * @method static \Illuminate\Database\Query\Builder|\Problem wherePhotoAfter($value)
 */
class Problem extends \Eloquent
{
    protected $guarded = array('id', 'created_at', 'updated_at', 'deleted_at');
    protected $table = 'problems';

    const Created = 'Created';
    const InProgress = 'InProgress';
    const NotRelevant = 'NotRelevant';
    const NeedCheck = 'NeedCheck';
    const Success = 'Success';
    const Closed = 'Closed';

    function getCategory()
    {
        return $this->belongsTo('Category', 'category');
    }

    function owner()
    {
        return $this->belongsTo('User', 'user');
    }

    function after()
    {
        return $this->belongsTo('Photo', 'photoAfter');
    }

    function before()
    {
        return $this->belongsTo('Photo', 'photoBefore');
    }

    function van()
    {
        return $this->hasOne('Van', 'problem');
    }

    function photos()
    {
        return $this->hasMany('Photo', 'problem_id');
    }

    function letters()
    {
        return $this->hasMany('Letter', 'problem_id');
    }

    function regions()
    {
        return $this->belongsToMany('Region', 'region_problem_links', 'problem', 'region')->withTimestamps();
    }

    function isReadyForRequest()
    {
        return (!$this->IsClosed()) && (isset($this->address)) && $this->checkOptions();
    }

    function checkOptions()
    {
        $opts = $this->getCategory->options->all();
        $tags = array_map(function ($x) {
            return $x->tag;
        }, $opts);
        $tags = array_combine($tags, array_pad([], count($tags), null));
        $values = array_merge($tags, $this->getOptionValues());

        foreach ($opts as $opt) {
            if (!$opt->checkValue($values[$opt->tag])) {
                return false;
            }
        }

        return true;
    }

    function IsClosed()
    {
        return $this->status == Problem::NotRelevant || $this->status == Problem::Success || $this->status == Problem::Closed;
    }

    function getOptionValues()
    {
        $arr = [];
        $opts = str_getcsv($this->optionValues, ';');
        foreach ($opts as $i => $str) {
            if (str_contains($str, '=')) {
                list ($key, $value) = explode('=', $str, 2);
                $arr[$key] = $value;
            }
        }
        return $arr;
    }

    function setOptionValues($arr)
    {
        $res = '';
        foreach ($arr as $key => $value) {
            $res .= $key . '=' . $value . ';';
        }
        $this->optionValues = $res;
        $this->save();
    }

    static function sieve($collection){
      //  var_dump(Request::fullUrl());
        $collection = Problem::filter('updated_at', '>=', Input::get('from'), $collection);
        $collection = Problem::filter('updated_at', '<=', Input::get('to'), $collection);
        $collection = Problem::filterArray('category', Input::get('category'), $collection);
        $collection = Problem::filterArray('status', Input::get('status'), $collection);
        $collection = Problem::filterRegion(Input::get('region'), $collection);
        $collection = Problem::filterCategoryGroup(Input::get('category_group'), $collection);
        return $collection;
    }

    static function filter($field, $eq, $data, $collection){
        return isset($data) ? $collection->where($field, $eq, $data) : $collection;
    }

    static function filterArray($field, $data, $collection){
        if (!isset($data))
            return $collection;
        $arr = self::getArrFromString($data);
        return $collection->whereIn($field, $arr);
    }

    static function filterRegion($regions, $collection){
        if (!isset($regions))
            return $collection;
        $regions = self::getArrFromString($regions);
        $collection = $collection->join('region_problem_links', 'problems.id', '=', 'region_problem_links.problem')
                                 ->whereIn('region_problem_links.region', $regions);
        return $collection;
    }

    static function filterCategoryGroup($cg, $collection){
        if (!isset($cg))
            return $collection;
        $cgs = self::getArrFromString($cg);
        $collection = $collection->join('category_group_links', 'problems.category', '=', 'category_group_links.category_id')
            ->whereIn('category_group_links.category_group_id', $cgs);
        return $collection;
    }


    protected static function getArrFromString($data)
    {
        $arr1 = explode(',', $data);
        return array_map(function ($x) {return trim($x);}, $arr1);
    }


    public static function trySave(Problem $problem, $data){
        $validator = ProblemTransformer::validator($data);
        if ($validator->fails())
            return false;
        ProblemTransformer::transform_back($problem, $data);
        $problem->save();
        return true;
    }

}