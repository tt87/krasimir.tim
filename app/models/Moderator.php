<?php

/**
 * Moderator
 *
 * @property integer $id
 * @property integer $user
 * @property integer $region
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Moderator whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Moderator whereUser($value)
 * @method static \Illuminate\Database\Query\Builder|\Moderator whereRegion($value)
 * @method static \Illuminate\Database\Query\Builder|\Moderator whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Moderator whereUpdatedAt($value)
 */
class Moderator extends \Eloquent
{
    protected $guarded = array('id', 'created_at', 'updated_at');
}