<?php

/**
 * Option
 *
 * @property integer $id
 * @property string $name
 * @property string $tag
 * @property boolean $isSingleChoice
 * @property boolean $hasCustomValue
 * @property string $prefix
 * @property string $suffix
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Option whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Option whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Option whereTag($value)
 * @method static \Illuminate\Database\Query\Builder|\Option whereIsSingleChoice($value)
 * @method static \Illuminate\Database\Query\Builder|\Option whereHasCustomValue($value)
 * @method static \Illuminate\Database\Query\Builder|\Option wherePrefix($value)
 * @method static \Illuminate\Database\Query\Builder|\Option whereSuffix($value)
 * @method static \Illuminate\Database\Query\Builder|\Option whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Option whereUpdatedAt($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Category[] $categories
 * @property boolean $canBeNull
 * @property-read \Illuminate\Database\Eloquent\Collection|\OptionValue[] $values
 * @method static \Illuminate\Database\Query\Builder|\Option whereCanBeNull($value)
 */
class Option extends \Eloquent
{
    protected $guarded = array('id', 'created_at', 'updated_at');
    protected $hidden = ["pivot", 'values', 'created_at', 'updated_at', 'prefix', 'suffix'];

    protected $table = "options";

    public function categories()
    {
        return $this->belongsToMany('Category', 'category_option_links', 'option_id', 'category_id');
    }

    public function values()
    {
        return $this->hasMany('OptionValue', 'option');
    }

    public function checkValue($value)
    {
        //  var_dump($value, $this->canBeNull);
        if ($value == null || trim($value) == '')
            return $this->canBeNull;
        $vals = explode('|', $value);
        $res = true;
        foreach ($vals as $val) {
            $res = $res && $this->CheckOneValue($val);
        }
        return $res;
    }

    /**
     * @param $value
     * @return bool
     */
    protected function CheckOneValue($value)
    {
        //var_dump($value);
        if ($value == null || trim($value) == '')
            return false;
        if ($this->hasCustomValue)
            return true;
        foreach ($this->values as $val) {
            if ($val->value == $value)
                return true;
        }
        return false;
    }

}