<ul class="alert alert-danger">
    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    @if (Session::get('errorMessage'))
    <li>{{ Session::get('errorMessage') }}</li>
    @endif
</ul>
<form name="auth" action="/auth/register" method="POST" role="form">
    <div class="form-group">
        <label for="email">E-mail</label>
        <input type="email" name="email" class="form-control" placeholder="Email" />
    </div>
    <div class="form-group">
        <label for="password">Password</label>
        <input type="password" name="password" class="form-control" />
    </div>

    <div class="form-group">
        <label for="password2">Repeat password</label>
        <input type="password" name="password2" class="form-control" />
    </div>

    <div class="form-group">
        <label for="nickname">Nickname</label>
        <input type="text" name="nickname" class="form-control" placeholder="" />
    </div>

    <div class="form-group">
        <label for="fioheader">Имя</label>
        <input type="text" name="fioheader" class="form-control" placeholder="Соловьева Станислава Николаевича" />
    </div>

    <div class="form-group">
        <label for="fiosign">Фамилия</label>
        <input type="text" name="fiosign" class="form-control" placeholder="Соловьев С.Н." />
    </div>

   <button type="submit" class="btn btn-default">Sign up</button>
</form>