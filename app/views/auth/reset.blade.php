<ul class="alert alert-danger">
    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    @if (Session::get('errorMessage'))
    <li>{{ Session::get('errorMessage') }}</li>
    @endif
</ul>
<form name="auth" action="/password/reset" method="POST" role="form">
    <div class="form-group">
        <label for="email">E-mail</label>
        <input type="email" name="email" class="form-control" placeholder="Email" />
    </div>
    <div class="form-group">
        <label for="password">Password</label>
        <input type="password" name="password" class="form-control" />
    </div>

    <div class="form-group">
        <label for="password_confirmation">Repeat password</label>
        <input type="password" name="password_confirmation" class="form-control" />
    </div>

    <input type="hidden" name="token" class="form-control" value="{{$token}}" />

    <button type="submit" class="btn btn-default">Sign up</button>
</form>