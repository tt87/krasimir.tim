@if (Session::get('errorMessage'))
<div class="alert alert-danger">{{ Session::get('errorMessage') }}</div>
@endif

<h2>Auth Index Page</h2>
<form name="auth" action="/auth/login" method="POST" role="form">
    <div class="form-group">
        <label for="email">E-mail</label>
        <input type="email" name="email" class="form-control" />
    </div>

    <div class="form-group">
        <label for="password">Password</label>
        <input type="password" name="password" class="form-control" />
    </div>

    <button type="submit" class="btn btn-default">Sign in</button>
</form>