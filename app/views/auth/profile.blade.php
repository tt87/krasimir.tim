@if (Session::get('message'))
<div class="alert alert-success">
    {{ Session::get('message') }}
</div>
@endif

<ul class="alert alert-danger">
    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    @if (Session::get('errorMessage'))
    <li>{{ Session::get('errorMessage') }}</li>
    @endif
</ul>

<h2>Profile page</h2>
<h2>User: <?php echo $user ?>  </h2>

<form name="auth" action="/auth/change" method="POST" role="form">
    <h2>Change password</h2>

    <div class="form-group">
        <label for="password">Current password</label>
        <input type="password" name="password" class="form-control" />
    </div>

    <div class="form-group">
        <label for="password">New password</label>
        <input type="password" name="newPassword" class="form-control" />
    </div>

    <div class="form-group">
        <label for="password2">Repeat new password</label>
        <input type="password" name="newPassword2" class="form-control" />
    </div>

    <button type="submit" class="btn btn-default">Change password</button>
</form>