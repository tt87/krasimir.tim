<ul class="alert alert-danger">
    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    @if (Session::get('errorMessage'))
    <li>{{ Session::get('errorMessage') }}</li>
    @endif
</ul>
<h2>Восстановление пароля</h2>
@if (! Session::has('status'))
<form name="auth" action="/password/remind" method="POST" role="form">
    <div class="form-group">
        <label for="email">E-mail</label>
        <input type="email" name="email" class="form-control" placeholder="Email" />
    </div>

    <button type="submit" class="btn btn-default">Sign up</button>
</form>
@else
<div>Ссылка для смены пароля выслана на ваш адрес</div>
@endif