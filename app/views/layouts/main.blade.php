<!DOCTYPE html>
<html lang="ru_RU">
	<head>
        <title>@yield('title')</title>
    	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
	    <link rel="stylesheet" type="text/css" href="/css/common.css" />
	    <link rel="stylesheet" type="text/css" href="/css/modals.css" />
	    <link rel="stylesheet" type="text/css" href="/css/sprites/footer.css" />
	    @yield('stylesheets')
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,700,600,300&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
	</head>
	<body>
		<div class="header-wrapper" id="header"></div>
		<div class="content-wrapper">
		    @yield('content')
		</div>
		<div class="footer-wrapper">
            <div class="footer-content">
                <div class="legal-info">
                    <a href="#" class="massmedia">Рассылка для СМИ</a>
                    <a href="mailto:info@krasimir.org" class="mailto">info@krasimir.org</a>
                    <span class="copyright">&copy; 2012-2015 Красивый Петербург</span>
                </div><!--
                --><div class="application-block">
                    <ul>
                        <li class="application iphone"><a href="https://itunes.apple.com/ru/app/krasivyj-mir/id857488685?l=ru&ls=1&mt=8" target="_blank">приложение для iPhone</a></li>
                        <li class="application android"><a href="https://play.google.com/store/apps/details?id=rpetrov.kraspb" target="_blank">приложение для Android</a></li>
                    </ul>
                </div><!--
                --><div class="social-block">
                    <ul><!--
                    --><li class="social-menu-item"><a href="https://vk.com/kp_app" target="_blank"><span class="icon sprite-social-vkontakte"></span></a></li><!--
                    --><li class="social-menu-item"><a href="https://www.facebook.com/SpbKrasiv?_rdr" target="_blank"><span class="icon sprite-social-facebook"></span></a></li><!--
                    --><li class="social-menu-item"><a href="https://twitter.com/peterburgkrasiv" target="_blank"><span class="icon sprite-social-twitter"></span></a></li><!--
                    --><li class="social-menu-item"><a href="http://spbkrasiv.livejournal.com/" target="_blank"><span class="icon sprite-social-livejournal"></span></a></li>
                    </ul>
                </div>
            </div>
		</div>
        <div id="overlay"></div>
        <div id="body-modal"></div>
        <!-- templates -->
        @include('partials/main/login')
        @include('partials/main/header')
        @yield('partials')
        <!-- libraries -->
        <script src="/js/libs/jquery.js"></script>
        <script src="/js/libs/underscore.js"></script>
        <script src="/js/libs/backbone.js"></script>
        <script src="/js/libs/backbone.marionette.js"></script>
        <script src="/js/libs/handlebars.js"></script>
        <!-- application -->
        <script src="/js/app/common/Authorized.js"></script>
        <script src="/js/app/common/Krasimir.js"></script>
        <script src="/js/app/common/Krasimir.Modal.js"></script>
        <script src="/js/app/common/Krasimir.Overlay.js"></script>
        <script src="/js/app/common/Krasimir.Login.js"></script>
        <script src="/js/app/common/Krasimir.Header.js"></script>
        <script src="/js/app/common/Krasimir.Layout.js"></script>
        <script src="/js/app/common/Krasimir.User.js"></script>
        @yield('scripts')
	</body>
</html>