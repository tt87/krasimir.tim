@extends('layouts.main')

@section('title')
Красивый Петербург
@stop

@section('stylesheets')
<link rel="stylesheet" type="text/css" href="/css/index.css" />
<link rel="stylesheet" type="text/css" href="/css/sprites/social.css" />
<link rel="stylesheet" type="text/css" href="/css/create-case.css" />
@stop

@section('content')
<div class="progress-container">
    <div class="progress-shadow"></div>

    <div class="progress step-1-now">
        <div class="line">
            <div class="line-grayed">
                <div class="line-part line-part-1"></div>
                <div class="line-part line-part-2"></div>
            </div>
        </div>
        <div class="step-point step-1-point">
            <div class="circle">1</div>
            <div class="step-title">выберите тип&nbsp;проблемы</div>
        </div>
        <div class="step-point step-2-point">
            <div class="circle">2</div>
            <div class="step-title">прикрепите фото и укажите адрес</div>
        </div>
        <div class="step-point step-3-point">
            <div class="circle">3</div>
            <div class="step-title">проверьте обращение</div>
        </div>
    </div>
</div>
<div class="step-1 case-type shown"></div>
<div class="step-2 case-details type-map">
    <div class="photos">
        <div class="add-photo"></div>
        <div class="add-photo"></div>
        <div class="add-photo"></div>
        <div class="add-photo"></div>
        <div class="add-photo"></div>
    </div>
    <h3>Укажите проблемы <span class="input-type-map">прямо на карте</span> или <span class="input-type-address">введите адрес</span></h3>
    <div class="address-wrapper">
        <div class="map" id="address_map"></div>
        <div class="overlay"></div>
        <div class="address-form">
            <input class="textfield input-city" type="text" placeholder="Город" />
            <input class="textfield input-street" type="text" placeholder="Улица" />
            <input class="textfield input-house" type="text" placeholder="Дом" />
            <input class="textfield input-corpus" type="text" placeholder="Корпус" />
            <textarea class="textareafield input-specified" placeholder="Уточните адрес, если нужно"></textarea>
            <div class="hint">
                Например:<br/>
                Напротив 1-го подъезда<br/>
                Между д. 2 и д. 4 по Невскому пр.
            </div>
        </div>
    </div>
    <h3>Отметьте необходимые пункты</h3>
    <div class="additional-info">
        <div class="info-row">
            <input type="checkbox"  />
            <span class="checkbox"></span>
            <label>Сломанные скамейки</label>
        </div>
        <div class="info-row">
            <input type="checkbox" />
            <span class="checkbox"></span>
            <label>Возможность незаконного заезда автомобилей на территорию</label>
        </div>
        <div class="info-row">
            <input type="checkbox" />
            <span class="checkbox"></span>
            <label>Повреждённые пешеходные дорожки</label>
        </div>
        <div class="info-row">
            <input type="checkbox" />
            <span class="checkbox"></span>
            <label>Отсутствие скамеек</label>
        </div>
        <div class="info-row">
            <input type="checkbox" />
            <span class="checkbox"></span>
            <label>Сломанные детские площадки</label>
        </div>
        <div class="info-row">
            <input type="checkbox" />
            <span class="checkbox"></span>
            <label>Отсутствие урн</label>
        </div>
        <div class="info-row">
            <input type="checkbox" />
            <span class="checkbox"></span>
            <label>Неубирающийся и невывозящийся в массовом порядке мусор</label>
        </div>
        <div class="info-row">
            <input type="checkbox" />
            <span class="checkbox"></span>
            <label>Сломанный забор</label>
        </div>
        <div class="info-row">
            <input type="checkbox" />
            <span class="checkbox"></span>
            <label>Загрязнённый пруд</label>
        </div>
    </div>
    <button class="blue-button next">Далее</button>
</div>
<div class="step-3 case-letter">
    <div class="letter">
        <div class="recipient">Губернатору Санкт-Петербурга</div>
        <div class="sender">от гражданина Абдурахмангаджи Абдурахмана Александровича.</div>
        <div class="sender-phone">контактный телефон: 89051234567</div>
        <div class="sender-email">адрес электронной почты: 89051234567@yandex.ru</div>
        <div class="title">Обращение</div>
        <div class="number">№ 44516 от 30.06.2014</div>
        <textarea class="extra">Прошу привлечь нарушителя к ответственности.</textarea>
        <div class="signature">С уважением, Абдурахмангаджи А.А.</div>
        <div class="photos"></div>
    </div>
    <button class="blue-button send">Отправить обращение</button>
</div>
@stop

@section('partials')
    @include('partials/create-case')
@stop

@section('scripts')
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <script src="/js/app/create-case/Krasimir.Categories.js"></script>
    <script src="/js/app/create-case/Krasimir.StickyHeader.js"></script>
    <script src="/js/app/create-case/Krasimir.AddressMap.js"></script>
    <script src="/js/app/create-case/Krasimir.Details.js"></script>
    <script src="/js/app/create-case/Krasimir.CreateCase.js"></script>
@stop