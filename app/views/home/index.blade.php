@extends('layouts.main')

@section('title')
Красивый Петербург
@stop

@section('stylesheets')
<link rel="stylesheet" type="text/css" href="/css/index.css" />
<link rel="stylesheet" type="text/css" href="/css/sprites/social.css" />
<link rel="stylesheet" type="text/css" href="/css/sprites/achievements.css" />
@stop

@section('content')
<h1>О движении Красивый Петербург</h1>
<div class="description">
    <div class="side-block half">
        <p>Красивый Петербург – это движение инициативных граждан за улучшение качества городской среды. Мы стремимся сделать родной город удобным и приятным для жизни людей, и для этого делимся простыми и эффективными способами влияния на качество городской среды.</p>
        <div class="resources">
            <div class="annotation">Присоединяйтесь к движению в социальных сетях:</div>
            {{-- TODO: unhardcode links, move them to some common place --}}
            <div class="social-menu"><!--
                --><div class="social-menu-item"><a href="https://vk.com/kp_app" target="_blank"><span class="icon sprite-social-vkontakte"></span></a></div><!--
                --><div class="social-menu-item"><a href="https://www.facebook.com/SpbKrasiv?_rdr" target="_blank"><span class="icon sprite-social-facebook"></span></a></div><!--
                --><div class="social-menu-item"><a href="https://twitter.com/peterburgkrasiv" target="_blank"><span class="icon sprite-social-twitter"></span></a></div><!--
                --><div class="social-menu-item"><a href="http://spbkrasiv.livejournal.com/" target="_blank"><span class="icon sprite-social-livejournal"></span></a></div>
            </div>
        </div>
    </div><!--
--><div class="side-block half">
        <p>Сайт КрасивыйПетербург.рф и мобильные приложения «Красивый Мир» созданы для горожан с целью исправления нарушений благоустройства. Всего за 20 секунд Вы можете отправить обращение по любой из множества городских проблем, тем самым дав старт её решению.</p>
        <div class="resources mobile">
            <div class="annotation">Скачать мобильное приложение можно по ссылкам:</div>
            <div class="application-menu">
                <a href="https://play.google.com/store/apps/details?id=rpetrov.kraspb" class="application-menu-item" target="_blank"><img src="https://developer.android.com/images/brand/ru_generic_rgb_wo_45.png" height="40" alt="Загрузите на Google Play" /></a>
                <a href="https://itunes.apple.com/ru/app/krasivyj-mir/id857488685?l=ru&ls=1&mt=8" class="application-menu-item" target="_blank"><img src="/images/index/appstore.png" width="135" height="40" alt="Загрузите на App Store" /></a>
            </div>
        </div>
    </div>
</div>
<div class="steps-wrapper">
    <div class="step">
        <div class="step-meta">
            <div class="step-number">1</div>
            <div class="step-name">Фотографируем</div>
        </div>
        <div class="description">Испорченный газон, разбитую дорогу, давнюю лужу, горы мусора и любую другую проблему.</div>
    </div><!--
--><div class="arrow"></div><!--
--><div class="step">
        <div class="step-meta">
            <div class="step-number">2</div>
            <div class="step-name">Отправляем</div>
        </div>
        <div class="description">Выберите тип проблемы, её адрес, прикрепите сделанный снимок и отправьте обращение.</div>
    </div><!--
--><div class="arrow"></div><!--
--><div class="step">
        <div class="step-meta">
            <div class="step-number">3</div>
            <div class="step-name">Контролируем</div>
        </div>
        <div class="description">Теперь у чиновников есть 30 дней, чтобы отчитаться перед вами о ходе решения проблемы.</div>
    </div>
</div>
<a href="/create-case" class="blue-button create-case">Отправить обращение по проблеме</a>
<div class="achievements-list">
    <div class="achievement members">
        <div class="icon sprite-achievements-members"></div>
        <div class="number">29200</div>
        <div class="description">Участников</div>
    </div><!--
    --><div class="achievement letters">
        <div class="icon sprite-achievements-letters"></div>
        <div class="number">43699</div>
        <div class="description">Обращений</div>
    </div><!--
    --><div class="achievement approves">
        <div class="icon sprite-achievements-approves"></div>
        <div class="number">6548</div>
        <div class="description">Подтверждений</div>
    </div><!--
    --><div class="achievement solutions">
        <div class="icon sprite-achievements-solutions"></div>
        <div class="number">14548</div>
        <div class="description">Решенных проблем</div>
    </div>
</div>
<div class="solved-issues-container">
    <div class="solved-issues">
        <div class="issues-container">
            <div class="issue-item">
                <div class="description">
                    <div class="title">Отремонтирована ограда</div>
                    <div class="text">По адресу Лермонтовский Проспект, д. 52 заменены сломанные элементы ограды.</div>
                </div>
                <div class="images">
                    <div class="image before" style="background-image: url(http://krasimir.org/img/photo/prew/img_12160.JPG)"></div>
                    <div class="arrow"></div>
                    <div class="image after" style="background-image: url(http://krasimir.org/img/ready/prew/img_12160.JPG)"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<h2>Новости</h2>
<div class="news">
    <div class="news-content"></div>
    <div class="images-content"></div>
</div>
<a href="http://vk.com/wall-38228859" class="all-news">Все новости &#8594;</a>
<h2>Проекты</h2>
<div class="projects">
    <div class="shadow-wrapper">
        <div class="shadow"></div>
        <div class="shadow"></div>
    </div>
    <div class="projects-wrapper">
        <div class="side-block half">
            <div class="project-description">
                <div class="annotation-image" style="background-image: url(https://pp.vk.me/c622118/v622118543/1ba90/FAWgHIW965w.jpg)"></div>
                <a class="project-title title" href="https://vk.com/savesennaya">Реконструкция Сенной площади</a>
                <div class="annotation">Сенная площадь &mdash; это площадь в центре города, на которую покушались Полтавченко и его приспешники с целью сделать эту и так не самую туристически привлекательную площадь города ещё менее привлекательной. Но благодаря слаженной и героической работы волонтёров Красивого Петербурга во главе со Стивом Каддинсом, мы смогли отстоять эту площадь и сделать её одной из самых красивых и людных площадей города, достойной европейского уровня.</div>
            </div>
        </div><!--
    --><div class="side-block half">
            <div class="project-description">
                <div class="annotation-image" style="background-image: url(https://pp.vk.me/c421423/v421423377/6b7/bT8QmidgisM.jpg)"></div>
                <a class="project-title title" href="https://vk.com/udpark">Защитим Удельный парк!</a>
                <div class="annotation">Группа вредителей во главе с Георгием Полтавченко хотели на месте этого парка провести автомагистраль, тем самым увеличив и так значительные пробки в городе. Красивый Петербург убедил власти провести референдум о статусе Удельного парка, в результате чего там не только не построили магистраль, но и провели обширную работу по его облагораживанию. На текущий момент Удельный парк является самым благоустроенным лесопарком города, в котором люди всех возрастов и статуса могут найти, чем заняться.</div>
            </div>
        </div>
    </div>
</div>
<a href="#" class="all-projects">Все проекты &#8594;</a>
@stop

@section('partials')
    @include('partials/index')
@stop

@section('scripts')
    <script src="/js/app/index/Krasimir.News.js"></script>
    <script src="/js/app/index/Krasimir.Index.js"></script>
@stop