<script type="text/html" id="template-login-form">
    <input type="text" class="login-input email-input" name="email" placeholder="Почта" value="tchurovtim@gmail.com" />
    <input type="password" class="login-input psw-input" name="password" placeholder="Пароль" value="12345" />
    <button class="blue-button login-button">Войти</button>
    <div class="login-options">
        <span class="remember-me">
            <input type="checkbox" name="remember" />
            <label for="remember">Запомнить меня</label>
        </span>
        <span class="recover-password">Забыли пароль?</span>
    </div>
    <div class="login-social">
        <div class="login-social-header">Или войдите с помощью:</div>
        <div class="login-social-buttons">
            <span class="login-social-button vk">
                <span class="login-social-sprite sprite-social-vkontakte"></span>
                <span class="login-social-label">ВКонтакте</span>
            </span>
            <span class="login-social-button fb">
                <span class="login-social-sprite sprite-social-facebook"></span>
                <span class="login-social-label">Фэйсбук</span>
            </span>
            <span class="login-social-button tw">
                <span class="login-social-sprite sprite-social-twitter"></span>
                <span class="login-social-label">Твиттер</span>
            </span>
        </div>
    </div>
</script>