<script type="text/html" id="template-header">
    <div class="header-content">
        <div class="logo-wrapper">
            <img src="/images/layout/logo.png" alt="Логотип Красивого мира" class="logo" />
            <div class="logo-side-data">
                <div class="site-name">Красивый Мир</div>
                <a href="#" class="city-name">Петропавловск-Камчатский</a>
            </div>
        </div><!--
        --><ul class="header-menu">
            <li class="item"><a href="#">Отправить обращение</a></li>
            <li class="item"><a href="#">Карта</a></li>
            <li class="item"><a href="#">Новости</a></li>
            <li class="item"><a href="#">Кабинет
                {{# if data.count }}
                <span class="count">{{ data.count }}</span>
                {{/ if }}
            </a></li>
        </ul><!--
        --><div class="user-data">
            {{# if data }}
            <ul class="user-area">
                <li>{{ data.screen_name }}</li>
                <li class="other">Личный кабинет</li>
                <li class="other">Настройки</li>
                <li class="other logout">Выйти</li>
            </ul>
            {{ else }}
            <a class="login-button">Войти</a>
            {{/ if }}
        </div>
    </div>
</script>