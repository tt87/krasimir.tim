<script type="text/html" id="template-dropdown-item-view">{{ name }}</script>

<script type="text/html" id="template-dropdown-view">
    <div class="details-wrapper"></div>
</script>

<script type="text/html" id="template-categories-view">
    <div class="categories-block"></div>
    <div class="categories-dropdown"></div>
</script>

<script type="text/html" id="template-categories-item-view">
    <img src="{{ icon_url }}" alt="{{ name }}" />
    <span class="category-title">{{ name }}</span>
</script>
