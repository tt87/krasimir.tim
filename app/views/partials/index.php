<script type="text/html" id="template-news-block">
    <div class="news-date">{{ date }}</div>
    <a href="http://vk.com/wall-38228859_{{ id }}" class="annotation">{{ title }}</a>
</script>

<script type="text/html" id="template-news-image">
    <div class="image" style="background-image: url({{ firstImage }});"></div>
</script>