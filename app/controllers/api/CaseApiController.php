<?php
/**
 * Created by PhpStorm.
 * User: tim
 * Date: 16.07.15
 * Time: 0:28
 */

use Chrisbjr\ApiGuard\ApiKey;

class CaseApiController extends BasicApiController
{

    protected $apiMethods = [
        'getAllCases' => ['keyAuthentication' => false],
        'getCase' => ['keyAuthentication' => false],
        'getOwnerCases' => ['keyAuthentication' => true,
            'level' => User::CommonUserLevel],
        'getUserCases' => ['keyAuthentication' => true,
            'level' => User::CommonUserLevel],
        'postCase' => ['keyAuthentication' => true,
            'level' => User::CommonUserLevel],
        'putCase' => ['keyAuthentication' => true,
            'level' => User::CommonUserLevel],
        'deleteCase' => ['keyAuthentication' => true,
            'level' => User::CommonUserLevel],
    ];

    public function getAllCases()
    {
        $collection = Problem::sieve(Problem::latest());
        return $this->getCollectionWithCursor($collection, new ProblemTransformer());
    }

    public function getOwnerCases()
    {
        $collection = Problem::sieve($this->apiKey->user->problems()->latest());
        return $this->getCollectionWithCursor($collection, new ProblemTransformer());
    }

    public function getUserCases($userId)
    {
        $user = User::find($userId);
        if ($this->checkAccess($user, User::CiteModeratorLevel)) {
            $collection = Problem::sieve($user->problems()->latest());
            return $this->getCollectionWithCursor($collection, new ProblemTransformer());
        }
        return $this->response->errorForbidden();
    }

    public function getCase($id)
    {
        $case = Problem::find($id);
        return $this->returnOrShowError($case, new ProblemTransformer());
    }

    public function postCase()
    {
        $case = new Problem(['user'=>$this->apiKey->user->id, 'status' => Problem::Created]); //todo it's not good: wee declare default value
        if (Problem::trySave($case, Input::json()->all())) {
            return $this->response->withItem($case, new ProblemTransformer());
        }
        return $this->response->errorWrongArgs();
    }

    public function putCase($id)
    {
        $case = Problem::find($id);
        if (!isset($case))
            return $this->response->errorNotFound();
        if (!$this->checkAccess($case->owner, User::CiteModeratorLevel))
            return $this->response->errorForbidden();
        if (!Problem::trySave($case, Input::json()->all()))
            return $this->response->errorWrongArgs();
        return $this->response->withItem($case, new ProblemTransformer());
    }

    public function deleteCase($id)
    {
        $case = Problem::find($id);
        if (isset($case) && $this->checkAccess($case->owner, User::CiteModeratorLevel)) {
            Problem::destroy([$id]);
            return $this->returnOk();
        }
        return $this->response->errorForbidden();
    }


}