<?php

use Chrisbjr\ApiGuard\ApiGuardController;
use Chrisbjr\ApiGuard\ApiKey;
use Chrisbjr\ApiGuard\Transformers\ApiKeyTransformer;
use OAuth\Common\Storage\Session;
use OAuth\Common\Consumer\Credentials;

class ApiAuthController extends ApiGuardController
{

    protected $apiMethods = [
        'registerVk' => ['keyAuthentication' => false],
        'registerFb' => ['keyAuthentication' => false],
        'registerTw' => ['keyAuthentication' => false],
        'registerGoogle' => ['keyAuthentication' => false],
        'register' => ['keyAuthentication' => false],
        'login' => ['keyAuthentication' => false],
        'logout' => ['keyAuthentication' => true],
//        'authBySocial' => ['keyAuthentication' => false],

    ];

    private $rules = array(
        'email' => 'required|email|unique:users,email',
        'password' => 'required|alpha_num|between:5,255',
    );


    function __construct()
    {
        parent::__construct();
        $uriFactory = new \OAuth\Common\Http\Uri\UriFactory();
        $this->currentUri = $uriFactory->createFromSuperGlobalArray($_SERVER);
        $this->currentUri->setQuery(''); //force clean possible rest of query
        $this->serviceFactory = new \OAuth\ServiceFactory();
    }


    //todo this is a start for optimization
//    public function authBySocial($serviceName)
//    {
//        $service = $this->getOauthService($serviceName);
//
//        if (empty($_GET['code'])) {
//            $url = $service->getAuthorizationUri();
//            return Redirect::to( (string) $url );
//        }
//
////        if (empty($_GET['oauth_token'])) {
////            $token = $service->requestRequestToken();
////            $url = $service->getAuthorizationUri(array('oauth_token' => $token->getRequestToken()));
////            return Redirect::to( (string)$url );
////        }
//    }


    public function registerVk()
    {
        $serviceName = 'vkontakte';
        $vkService = $this->getOauthService($serviceName);

        //redirect to OAuth Endpoint if request didn't come from there
        if (empty($_GET['code'])) {
            $url = $vkService->getAuthorizationUri();
            return Redirect::to((string)$url);
        }

        $token = $vkService->requestAccessToken($_GET['code']);
        $userData = new stdClass();
        $userData->id = $token->getExtraParams()['user_id'];
        $userData->email = $token->getExtraParams()['email'];

        $user = User::findUserBySocial($serviceName, $userData->id, $userData->email);

        if (!isset($user)) {
            $result = json_decode($vkService->request('/users.get?user_ids=' . $userData->id . '&fields=city,country,screen_name'));
            $userData->nickname = $result->response[0]->screen_name;
            $userData->name = $result->response[0]->first_name;
            $userData->surname = $result->response[0]->last_name;
            $userData->accessLevel = User::CommonUserLevel;
            $user = User::createUserFromSocial($serviceName, $userData);
        }

        return $this->FindAndWrapApiKey($user);
    }


    public function registerFb()
    {
        $serviceName = 'facebook';
        $oauthService = $this->getOauthService($serviceName);

        //redirect to OAuth Endpoint if request didn't come from there
        if (empty($_GET['code'])) {
            $url = $oauthService->getAuthorizationUri();
            return Redirect::to((string)$url);
        }

        $oauthService->requestAccessToken($_GET['code']);
        $result = json_decode($oauthService->request('v2.2/me'));

        $userData = new stdClass();
        $userData->id = $result->id;
        $userData->email = $result->email;
        $userData->nickname = $result->name;
        $userData->name = $result->first_name;
        $userData->surname = $result->last_name;
        $userData->accessLevel = User::CommonUserLevel;

        $user = User::findOrCreateSocialUser($serviceName, $userData);
        return $this->FindAndWrapApiKey($user);
    }

    public function registerGoogle()
    {
        $serviceName = 'google';
        $oauthService = $this->getOauthService($serviceName, ['email', 'profile', 'https://www.googleapis.com/auth/userinfo.email', 'https://www.googleapis.com/auth/userinfo.profile', 'https://www.googleapis.com/auth/plus.login', 'https://www.googleapis.com/auth/plus.login']);

        //redirect to OAuth Endpoint if request didn't come from there
        if (empty($_GET['code'])) {
            $url = $oauthService->getAuthorizationUri();
            return Redirect::to((string)$url);
        }

        $token = $oauthService->requestAccessToken($_GET['code']);
        $result = json_decode($oauthService->request('https://www.googleapis.com/oauth2/v1/userinfo'));
        //todo migrate to new version of API, this is deprecated
        //    $token->getExtraParams()['token_type'] . ' ' . $token->getAccessToken();
        //$result = json_decode($oauthService->request('/plus/v1/people/me?key=' . $this->servicesCredentials['google']['key']));

        $userData = new stdClass();
        $userData->id = $result->id;
        $userData->email = $result->email;
        $userData->nickname = $result->name;
        $userData->name = $result->given_name;
        $userData->surname = $result->family_name;
        $userData->accessLevel = User::CommonUserLevel;

        $user = User::findOrCreateSocialUser($serviceName, $userData);
        return $this->FindAndWrapApiKey($user);
    }

    public function registerTw()
    {
        $serviceName = 'twitter';
        $oauthService = $this->getOauthService($serviceName, null); //Twitter is Oauth v1.0 service and doesn't provide email
        $storage = $oauthService->getStorage();

        //redirect to OAuth Endpoint if request didn't come from there
        if (empty($_GET['oauth_token'])) {
            $token = $oauthService->requestRequestToken();
            $url = $oauthService->getAuthorizationUri(array('oauth_token' => $token->getRequestToken()));
            return Redirect::to((string)$url);
        }

        $token = $storage->retrieveAccessToken('Twitter'); //todo ??
        $oauthService->requestAccessToken(
            $_GET['oauth_token'],
            $_GET['oauth_verifier'],
            $token->getRequestTokenSecret()
        );

        $result = json_decode($oauthService->request('account/verify_credentials.json'));

        $userData = new stdClass();
        $userData->id = $result->id;
        $userData->email = null;
        $userData->nickname = $result->screen_name;
        $userData->accessLevel = User::CommonUserLevel;


        $names = explode(' ', $result->name);
        if (count($names) > 0)
            $userData->name = $names[0];
        if (count($names) > 1)
            $userData->surname = $names[1];

        $user = User::findOrCreateSocialUser($serviceName, $userData);

        return $this->FindAndWrapApiKey($user);
    }

    public function register()
    {
        $input = json_decode(Input::getContent());
        $validator = Validator::make((array)$input, $this->rules);

        if ($validator->fails()) {
            return $this->response->errorWrongArgsValidator($validator);
        }

        $user = new User;
        $user->email = $input->email;
        $user->password = Hash::make($input->password);
        $user->emailApproved = false;
        $user->accessLevel = User::CommonUserLevel;

        if (!$user->save())
            return $this->response->errorInternalError("Failed save user. Please try again.");

        return $this->FindAndWrapApiKey($user);
    }

    public function login()
    {
        $credentials['email'] = Input::json('email');
        $credentials['password'] = Input::json('password');

        if (!Auth::validate($credentials)) {
            return $this->response->errorUnauthorized("Your email or password is incorrect");
        }

        $user = User::whereEmail($credentials['email'])->first();
        return $this->FindAndWrapApiKey($user);
    }


    public function logout()
    {
        if (empty($this->apiKey)) {
            return $this->response->errorUnauthorized("There is no such user to deauthenticate.");
        }
        $this->apiKey->delete();
        return $this->response->withArray([
            'ok' => [
                'code' => 'SUCCESSFUL',
                'http_code' => 200,
                'message' => 'User was successfully deauthenticated'
            ]
        ]);
    }

    /**
     * @param $serviceName
     * @return \OAuth\Common\Service\ServiceInterface
     */
    private function getOauthService($serviceName, $scope = ['email'])
    {
        $credentials = $this->getCredentials($serviceName);
        $storage = new Session();
        return $this->serviceFactory->createService($serviceName, $credentials, $storage, $scope);
    }

    /**
     * @param $serviceName
     * @return Credentials
     */
    private function getCredentials($serviceName)
    {
        $credentials = new Credentials(
            $_ENV['SOCIAL_CREDENTIALS.' . $serviceName . '.key'],
            $_ENV['SOCIAL_CREDENTIALS.' . $serviceName . '.secret'],
            $this->currentUri->getAbsoluteUri()
        );
        return $credentials;
    }

    /**
     * @param $user
     * @return ApiKey|\Illuminate\Database\Eloquent\Model|null|static
     */
    static function findOrCreateApiKey($user)
    {
        $apiKey = ApiKey::where('user_id', '=', $user->id)->first();

        if (!isset($apiKey)) {
            $apiKey = new ApiKey();
            $apiKey->user_id = $user->id;
            $apiKey->level = $user->accessLevel;
            $apiKey->ignore_limits = $user->accessLevel >= User::CiteModeratorLevel;
            $apiKey->key = $apiKey->generateKey();
            $apiKey->save();
        }
        return $apiKey;
    }

    /**
     * @param $user
     * @return mixed
     */
    private function FindAndWrapApiKey($user)
    {
        $apiKey = $this->findOrCreateApiKey($user); //ApiKey::findOrCreate($user); - macro doesn't work

        if (!isset($apiKey)) {
            return $this->response->errorInternalError("Failed to find or create an API key. Please try again.");
        }

        return $this->response->withItem($apiKey, new ApiKeyTransformer);
    }

}