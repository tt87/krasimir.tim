<?php

/**
 * Created by PhpStorm.
 * User: tim
 * Date: 10.07.15
 * Time: 23:52
 */
class OrganizationController extends BasicApiController
{

    protected $apiMethods = [
        'getOrganizations' => [
            'keyAuthentication' => true,
            'level' => User::CommonUserLevel
        ],
        'getOrganization' => [
            'keyAuthentication' => true,
            'level' => User::CommonUserLevel
        ],
        'getCaseOrganizations' => [
            'keyAuthentication' => true,
            'level' => User::CommonUserLevel
        ],
        'getRegionOrganizations' => [
            'keyAuthentication' => true,
            'level' => User::CommonUserLevel
        ],
        'getCategoryOrganizations' => [
            'keyAuthentication' => true,
            'level' => User::CommonUserLevel
        ],
        'postOrganization' => [
            'keyAuthentication' => true,
            'level' => User::CiteModeratorLevel
        ],
        'putOrganization' => [
            'keyAuthentication' => true,
            'level' => User::CiteModeratorLevel
        ],
        'deleteOrganization' => [
            'keyAuthentication' => true,
            'level' => User::CiteModeratorLevel
        ],
        'postSingleOrganization' => [
            'keyAuthentication' => true,
            'level' => User::CiteModeratorLevel
        ],
    ];


    public function getOrganizations()
    {
        return $this->getCollectionWithCursor(Organization::orderBy('id', 'desc'), new OrganizationTransformer());
    }

    public function getOrganization($id)
    {
        $org = Organization::find($id);
        return $this->returnOrShowError($org, new OrganizationTransformer());
    }

    public function getRegionOrganizations($id)
    {
        $reg = Region::find($id);
        if (isset($reg)) {
            return $this->getCollectionWithCursor($reg->organizations()->orderBy('id', 'desc'), new OrganizationTransformer());
        } else {
            return $this->response->errorNotFound();
        }
    }

    public function getCaseOrganizations($id)
    {
        $case = Problem::find($id);
        if (isset($case)) {
            return $this->getCollectionWithCursor(Organization::searchOrganizationWithCat($case->lat, $case->long, $case->getCategory)->orderBy('id', 'desc'), new OrganizationTransformer());
        } else {
            return $this->response->errorNotFound();
        }
    }

    public function getCategoryOrganizations($id)
    {
        $cat = Category::find($id);
        if (isset($cat)) {
            return $this->getCollectionWithCursor($cat->organizations()->orderBy('id', 'desc'), new OrganizationTransformer());
        } else {
            return $this->response->errorNotFound();
        }
    }

    public function deleteOrganization($id)
    {
        Organization::destroy([$id]);
        return json_encode(["result" => "ok"]);
    }

    public function postOrganization()
    {
        $org = new Organization();
        $this->Fill($org, 'syncLinks');
        return $this->response->withItem($org, new OrganizationTransformer());
    }

    public function putOrganization($id)
    {
        $org = Organization::find($id);
        if (!isset($org)) {
            return $this->response->errorForbidden();
        }
        $this->Fill($org, 'syncLinks');
        return $this->response->withItem($org, new OrganizationTransformer());
    }

    public function postSingleOrganization($id)
    {
        $org = Organization::find($id);
        if (!isset($org)) {
            return $this->response->errorForbidden();
        }
        $this->Fill($org, 'attachLinks');
        return $this->response->withItem($org, new OrganizationTransformer());
    }

    protected function Fill(Organization $org, $func)
    {
        $data = Input::json()->all();
        OrganizationTransformer::transform_back($org, $data);
        $org->save();
        $regs = explode(',', Input::get('regions', ''));
        $cats = explode(',', Input::get('cats', ''));
        $org->$func($regs, $cats);
    }

}