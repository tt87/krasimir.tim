<?php
/**
 * Created by PhpStorm.
 * User: tim
 * Date: 16.07.15
 * Time: 0:28
 */

use Chrisbjr\ApiGuard\ApiKey;

class RequestApiController extends BasicApiController
{
    protected $apiMethods = [
        'getRequest' => ['keyAuthentication' => true,
            'level' => User::CommonUserLevel],
        'getRequests' => ['keyAuthentication' => true,
            'level' => User::CommonUserLevel],
        'getCaseRequests' => ['keyAuthentication' => true,
            'level' => User::CommonUserLevel],
        'getUserRequests' => ['keyAuthentication' => true,
            'level' => User::CommonUserLevel],
        'postRequest' => ['keyAuthentication' => true,
            'level' => User::CommonUserLevel],
        'putRequest' => ['keyAuthentication' => true,
            'level' => User::CommonUserLevel],
        'deleteRequest' => ['keyAuthentication' => true,
            'level' => User::CommonUserLevel]
    ];

    //todo need to think: select private|public transformer on depend of user's identity

    public function getRequest($id)
    {
        $req = Letter::find($id);
        return $this->returnOrShowError($req, new PublicLetterTransformer());
    }

    public function getRequests()
    {
        $collection = Letter::sieve(Letter::latest());
        return $this->getCollectionWithCursor($collection, new PublicLetterTransformer());
    }

    public function getCaseRequests($id)
    {
        $problem = Problem::find($id);
        if (!isset($problem))
            return $this->response->errorNotFound();
        if (!$this->checkAccess($problem->owner, User::CiteModeratorLevel))
            return $this->response->errorForbidden();
        $collection = Letter::sieve($problem->letters()->latest());
        return $this->getCollectionWithCursor($collection, new LetterTransformer());
    }

    public function getUserRequests($id)
    {
        $user = User::find($id);
        if (!isset($user))
            return $this->response->errorNotFound();
        if ($this->checkAccess($user, User::CiteModeratorLevel)) {
            $collection = Letter::sieve($user->letters()->latest());
            return $this->getCollectionWithCursor($collection, new LetterTransformer());
        }
        return $this->response->errorForbidden();
    }

    public function postRequest()
    {
        $orgs = explode(',', Input::get('organizations', ''));
        $data = Input::json()->all();
        $data['user'] = $this->apiKey->user->id;
        $requests = [];
        //todo need prepare common header
        foreach ($orgs as $org) {
            $data['organization'] = $org;
            $data['header'] = Organization::find($org)->letterHeader;
            $data['footer'] = $this->getFooter();
            $data['organization'] = $org;
            $req = new Letter();
            $req->saveNewData($data, $orgs);
            $requests[] = $req;
        } //todo send emails here
        return $this->response->withCollection($requests, new LetterTransformer());
    }

    public function putRequest($id)
    {
        $req = Letter::find($id);
        if (!isset($req))
            return $this->response->errorNotFound();
        if ($this->checkAccess($req->owner, User::CiteModeratorLevel)) {
            $data = Input::json()->all();
            $req->saveNewData($data, null);
            return $this->returnOrShowError($req, new LetterTransformer());
        }
        return $this->response->errorForbidden();
    }

    public function deleteRequest($id)
    {
        $letter = Letter::find($id);
        if (isset($letter) && $this->checkAccess($letter->owner, User::CiteModeratorLevel)) {
            Letter::destroy([$id]);
            return $this->returnOk();
        }
        return $this->response->errorForbidden();
    }

    protected function getFooter()
    {
        $template = Config::get('templates.footer','');
        $fio  = $this->apiKey->user->surname .' '. $this->apiKey->user->name . ' ' . $this->apiKey->user->patronymic;
        $result = str_replace('%fio%', $fio, $template);
        return $result;
    }

}