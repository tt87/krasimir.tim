<?php

class TestApiController extends BaseController
{
    public function test($requestString)
    {
        if ($requestString == '' || is_null($requestString)) {
            return Response::json([
                'error' => true,
                'message' => 'Please specify request string!',
            ],
                400);
        }

        return Response::json([
            'error' => false,
            'requestString' => $requestString,
            'date' => date_create(),
        ],
            200
        );
    }

    public function testenv()
    {
        return $_ENV['TEST_STRIPE_KEY'];
    }

    public function refresh()
    {
        Artisan::call('migrate:refresh', ['--seed' => null]);
        $user2 = User::find(3);
        return $user2;
    }

    public function migrate()
    {
        Artisan::call('migrate:install');
        return 'ok';
    }

    public function file()
    {
        $file = Input::file('hello');
        $file->move('./app/tests/outdata', 'www.txt');
        //return Response::o
    }

    public function getfile()
    {
        return Response::download(public_path() . "/../app/tests/outdata/www.txt");
    }

    public function postform()
    {
        $file = Input::file('file');
        $r = $file->move('./app/tests/outdata', 'file');
        //return Response::o
    }

    public function getform()
    {
        return Response::view('testform');

    }

    public function email()
    {
        Mail::send('testemail', [], function ($message) {
            $message->to('azabolots@gmail.com', 'Tim')->subject('Yo!');
        });
        return "ok!";
    }


}

