<?php

use Chrisbjr\ApiGuard\ApiKey;
use \League\Fractal\Pagination\Cursor;

/**
 * Created by PhpStorm.
 * User: tim
 * Date: 21.05.15
 * Time: 21:51
 */
class BasicApiController extends \Chrisbjr\ApiGuard\ApiGuardController
{
    const DefaultCount = 15;
    const MaxCount = 100;

    protected function getCollectionWithCursor($collection, $transformer)
    {
        if (!isset($collection))
            return $this->response->errorNotFound();

        list($count, $current) = $this->GetCursorAndCount();

        $res = $collection->skip($current)->take($count)->get();
        $next = $res->count() != $count ? null : $current + $count;
        $cursor = new Cursor($current, null, $next, $res->count());

        return $this->response->withCollection($res, $transformer, null, $cursor);
    }

    protected function returnOrShowError($item, $transformer)
    {
        if (isset($item))
            return $this->response->withItem($item, $transformer);
        else
            return $this->response->errorNotFound();
    }


    /**
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    protected function GetApiKey()
    {
        $key = Input::Header(Config::get('api-guard::keyName'));
        if (empty($key)) {
            $key = Input::get(Config::get('api-guard::keyName'));
        }
        $apiKey = ApiKey::where('key', '=', $key)->first();
        return $apiKey;
    }

    /**
     * @param $case
     * @return bool
     */
    protected function  checkAccess(User $user, $level)
    {
        if (!isset($user))
            return false;
        return $user->id == $this->apiKey->user->id || (isset($this->apiKey) && $this->apiKey->level >= $level);
    }

    protected function CheckUserLevel($level)
    {
        return isset($this->apiKey) && $this->apiKey->level >= $level;
    }


    /**
     * @return string
     */
    protected function returnOk()
    {
        return json_encode(["result" => "ok"]);
    }

    /**
     * @return array
     */
    protected function GetCursorAndCount()
    {
        $count = Input::get('count', ApiController::DefaultCount);
        $count = $count >= ApiController::MaxCount ? ApiController::MaxCount : $count;
        $current = Input::get('cursor', 0);
        return array($count, $current);
    }

}