<?php

/**
 * Created by PhpStorm.
 * User: tim
 * Date: 10.07.15
 * Time: 0:09
 */
class RegionApiController extends BasicApiController
{
    protected $apiMethods = [
        'getRegions' => [
            'keyAuthentication' => true,
            'level' => User::CommonUserLevel
        ],
        'getCaseRegions' => [
            'keyAuthentication' => true,
            'level' => User::CommonUserLevel
        ],
        'getRegion' => [
            'keyAuthentication' => true,
            'level' => User::CommonUserLevel
        ],
        'postRegion' => [
            'keyAuthentication' => true,
            'level' => User::RootLevel
        ],
        'putRegion' => [
            'keyAuthentication' => true,
            'level' => User::RootLevel
        ],
        'deleteRegion' => [
            'keyAuthentication' => true,
            'level' => User::RootLevel
        ],
    ];

    public function getRegions()
    {
        return $this->getCollectionWithCursor(Region::orderBy('id', 'desc'), new RegionTransformer());
    }

    public function getCaseRegions($id)
    {
        $case = Problem::find($id);
        if (isset($case)) {
            return $this->getCollectionWithCursor(Region::searchRegions($case->lat, $case->long)->orderBy('id', 'desc'), new RegionTransformer());
        } else {
            return $this->response->errorNotFound();
        }
    }

    public function getRegion($id)
    {
        $region = Region::find($id);
        return $this->returnOrShowError($region, new RegionTransformer());
    }

    public function postRegion()
    {
        $data = Input::json()->all();
        $region = new Region();
        RegionTransformer::transform_back($region, $data);
        $region->save();
        return $this->response->withItem($region, new RegionTransformer());
    }

    public function putRegion($id)
    {
        $data = Input::json()->all();
        $region = Region::find($id);
        if (!isset($region)) {
            return $this->response->errorForbidden();
        }
        RegionTransformer::transform_back($region, $data);
        $region->save();
        return $this->response->withItem($region, new RegionTransformer());
    }

    public function deleteRegion($id)
    {
        Region::destroy([$id]);
        return $this->returnOk();
    }

}