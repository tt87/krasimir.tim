<?php

use Chrisbjr\ApiGuard\ApiGuardController;
use Chrisbjr\ApiGuard\ApiKey;
use Dmyers\Storage\Storage;
use League\Fractal\Pagination\Cursor;
use League\Fractal\Resource\Collection;
use Symfony\Component\HttpFoundation\File\UploadedFile;


class ApiController extends BasicApiController
{
    protected $apiMethods = [
        'getUserInfo' => ['keyAuthentication' => false],
        'postUserInfo' => ['keyAuthentication' => true],
        'postPhoto' => ['keyAuthentication' => true],
        'getProblemPhotos' => ['keyAuthentication' => true],
        'getPhoto' => ['keyAuthentication' => true],
        'getPhotos' => ['keyAuthentication' => true],
    ];

    public function getUserInfo()
    {
        $apiKey = $this->GetApiKey();
        if (isset($apiKey)) {
            $user = $apiKey->user;
            return $this->response->withItem($user, new UserTransformer());
        }
        return $this->response->withArray(['error' => true]);
    }

    public function postUserInfo()
    {
        $user2 = UserTransformer::transform_back($this->apiKey->user, Input::json()->all());
        if (!$user2->save()) {
            return $this->response->errorInternalError("Failed to save changes. Please try again");
        }
        return $this->response->withItem($user2, new UserTransformer());
    }

    public function postPhoto($id)
    {
        $data = Input::json()->all();
        $problem = Problem::find($id);
        $photo = Photo::Take(Input::file('photo'), $problem, $this->apiKey->user, $data);
        return $this->response->withItem($photo, new PhotoTransformer());
    }

    public function getProblemPhotos($problemId)
    {
        $problem = Problem::find($problemId);
        return $this->response->withCollection($problem->photos, new PhotoTransformer());
    }

    public function getPhoto($photoId)
    {
        $photo = Photo::find($photoId);
        return $this->response->withItem($photo, new PhotoTransformer());
    }

    public function getPhotos()
    {
        return $this->getCollectionWithCursor(Photo::orderBy('id', 'desc'), new PhotoTransformer());
    }



}
