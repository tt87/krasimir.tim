<?php

/**
 * Created by PhpStorm.
 * User: tim
 * Date: 21.05.15
 * Time: 21:40
 */
class PublicApiController extends BasicApiController
{
    protected $apiMethods = [
        'getStat' => ['keyAuthentication' => false],
        'getCategory' => ['keyAuthentication' => false],
        'getCategories' => ['keyAuthentication' => false],
        'getNews' => ['keyAuthentication' => false],
        'getAddress' => ['keyAuthentication' => false],
        'getAddressRegions' => ['keyAuthentication' => false],
        'missing' => ['keyAuthentication' => false],
    ];

    public function getStat()
    {
        $data = [];
        $data['cases_total'] = Problem::count();
        $data['users_total'] = User::count();
        $data['cases_solved'] = Problem::where('status', '=', Problem::Success)->count();
        return json_encode($data);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getCategories()
    {
        $categoryGroups = CategoryGroup::all();
        return $this->response->withCollection($categoryGroups, new CategoryGroupTransformer());
    }

    /**
     * @param $id
     * @param $format
     * @return \Illuminate\Support\Collection|static[]
     */
    public function getCategory($id)
    {
        $cat = Category::withTrashed()->where('id', '=', $id)->first();
        return $this->response->withItem($cat, new CategoryTransformer());
    }

    public function getNews()
    {
        $data = file_get_contents('http://api.vk.com/method/wall.get?domain=peterburg_krasiv&count=4');
        $response = Response::make($data, 200);
        $response->header('Content-Type', 'application/json');
        return $response;
    }

    //for nominatim service
    public function getAddressNominatim()
    {
        if (Input::has('lon') && Input::has('lat')) {
            $data = file_get_contents('http://nominatim.openstreetmap.org/reverse?format=json&accept-language=en-US&lat=' . Input::get('lat') . '&lon=' . Input::get('lon'));
            var_dump($data);
            $response = Response::make($data, 200);
            $response->header('Content-Type', 'application/json');
            return $response;
        } else
            return $this->response->withError("Not found arguments", 408);
    }

    public function getAddress()
    {
        if (Input::has('lon') && Input::has('lat')) {
            $data = file_get_contents('https://api.opencagedata.com/geocode/v1/json?key=545169005bdae0358c1b7de042f8742b&q=' . Input::get('lat') . ',' . Input::get('lon') . '&no_annotations=1');
            var_dump($data);
            $response = Response::make($data, 200);
            $response->header('Content-Type', 'application/json');
            return $response;
        } else
            return $this->response->withError("Not found arguments", 408);
    }

    public function getAddressRegions()
    {
        if (Input::has('lon') && Input::has('lat')) {
            return $this->getCollectionWithCursor(Region::searchRegions(Input::get('lat'),Input::get('lon'))->orderBy('id', 'desc'), new RegionTransformer());
        }
        else
            return $this->response->withError("Not found", 408);

    }

    public function getAddressOrganizations()
    {
        if (Input::has('lon') && Input::has('lat')) {
            return $this->getCollectionWithCursor(Organization::searchOrganization(Input::get('lat'),Input::get('lon'))->orderBy('id', 'desc'), new OrganizationTransformer());
        }
        else
            return $this->response->withError("Not found", 408);

    }

    public function missing(){
        return $this->response->errorNotFound();
    }
}