<?php

/**
 * Created by PhpStorm.
 * User: tim
 * Date: 21.05.15
 * Time: 21:49
 */
class VanApiController extends BasicApiController
{
    protected $apiMethods = [
        'postVanFromCase' => [
            'keyAuthentication' => true,
            'level' => User::CiteModeratorLevel
        ],
        'putVan' => [
            'keyAuthentication' => true,
            'level' => User::CiteModeratorLevel
        ],
        'postVan' => [
            'keyAuthentication' => true,
            'level' => User::CiteModeratorLevel
        ],
        'deleteVan' => [
            'keyAuthentication' => true,
            'level' => User::CiteModeratorLevel
        ],
        'getVan' => ['keyAuthentication' => false],
        'getVans' => ['keyAuthentication' => false],
    ];

    public function postVanFromCase($problemId)
    {
        $van = Van::createForProblem($problemId);
        if (isset($van))
            return $this->response->withItem($van, new VanTransformer());
        else
            return $this->response->withError("Case not finished successfully", "400");
    }

    public function putVan($vanId)
    {
        $van = Van::find($vanId);
        $data = Input::json()->all();
        if (!isset($van)) {
            return $this->response->errorForbidden();
        }
        $van->photoBefore = Van::CheckAndSave($vanId, 'before');
        $van->photoAfter = Van::CheckAndSave($vanId, 'after');
        VanTransformer::transform_back($van, $data);
        $van->save();
        return $this->response->withItem($van, new VanTransformer());
    }

    public function postVan()
    {
        $van = new Van();
        $data = Input::json()->all();
        VanTransformer::transform_back($van, $data);
        $van->user = $this->apiKey->user->id;
        $van->save();
        $van->photoBefore = Van::CheckAndSave($van->id, 'before');
        $van->photoAfter = Van::CheckAndSave($van->id, 'after');
        $van->save();
        return $this->response->withItem($van, new VanTransformer());
    }

    public function getVan($id)
    {
        $van = Van::find($id);
        return $this->returnOrShowError($van, new VanTransformer());
    }

    public function getVans()
    {
        if (!Input::has('random'))
            return $this->getCollectionWithCursor(Van::orderBy('id', 'desc'), new VanTransformer());
        else {
            $count = Input::get('random', self::DefaultCount);
            return $this->response->withCollection(Van::GetRandomCollection($count), new VanTransformer());
        }
    }

    public function deleteVan($vanId)
    {
        Van::destroy($vanId);
        return $this->returnOk();
    }


}