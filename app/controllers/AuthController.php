<?php

class AuthController extends \BaseController
{
    public function __construct()
    {
        $this->beforeFilter('auth', ['only' => ['getProfile', 'postChange', 'getLogout']]);
        $this->beforeFilter('guest', ['only' => ['getIndex','getRegister', 'postRegister', 'postLogin']]);
    }

	/**
	 * Display authentication form.
	 * GET /auth/index
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		return View::make('auth.index');
	}


    /**
     * Display profile page.
     * GET /auth/profile
     *
     * @TODO: move to some ProfileController once there are more user pages
     *
     * @return Response
     */
    public function getProfile()
    {
        return View::make('auth.profile', ["user"=>Auth::user()->nickname]);
    }

    /**
     * Display registration form.
     * GET /auth/register
     *
     * @return Response
     */
    public function getRegister()
    {
        return View::make('auth.register');
    }

    /**
     * Register new user.
     * POST /auth/store
     *
     * @return Response
     */
    public function postRegister()
    {
        // @TODO: find better validation tool in laravel, this one is awful

        $rules = array(
            'email' 	=> 'required|email|unique:users,email',
            'password' 	=> 'required|alpha_num|between:4,50',
            'nickname' 	=> 'required|alpha_num|unique:users,nickname',
        );

        $validator = Validator::make(Input::all(), $rules);

        if($validator->fails())
        {
            return Redirect::back()->withInput()->withErrors($validator);
        }

        if (Input::get('password') != Input::get('password2'))
        {
            // @TODO: l10n
            return Redirect::back()->withInput()->with('errorMessage', 'Passwords don\'t match!');
        }

       // $user = new User([Input::all()]);

        $user = new User;
        $user->email = Input::get('email');
        $user->password = Hash::make(Input::get('password'));
        $user->nickname = Input::get('nickname');
        $user->save();


        Auth::loginUsingId($user->id);

        return Redirect::action('AuthController@getProfile');
    }

    /**
     * Register new user.
     * POST /auth/change
     *
     * @return Response
     */
    public function postChange()
    {
        $user = Auth::user();
        if (!Auth::attempt(['email' => $user->email, 'password' => Input::get('password')]))
        {
            // @TODO: l10n
            return Redirect::back()->with('errorMessage', 'You typed wrong current password!');
        }

        // @TODO: see previous todo

        $rules = array(
            'newPassword' 	=> 'required|alpha_num|between:4,50'
        );

        $validator = Validator::make(Input::all(), $rules);

        if($validator->fails())
        {
            return Redirect::back()->withInput()->withErrors($validator);
        }

        if (Input::get('newPassword') != Input::get('newPassword2'))
        {
            // @TODO: l10n
            return Redirect::back()->withInput()->with('errorMessage', 'Passwords don\'t match!');
        }

        $user->password = Hash::make(Input::get('password'));
        $user->save();

        // @TODO: l10n
        return Redirect::action('AuthController@getProfile')->with('message', 'Password successfully changed!');
    }

	/**
	 * Authenticate user.
	 * POST /auth/login
     *
     * @return Response
	 */
	public function postLogin()
	{
		if (Auth::attempt(['email' => Input::get('email'), 'password' => Input::get('password')], Input::get('remember_me')))
        {
            return Redirect::back();
        }

        // @TODO: l10n
        return Redirect::back()->with('errorMessage', 'Incorrect email or password!');
	}

    /**
     * Unauthenticate user.
     * POST /auth/logout
     *
     * @return Response
     */
    public function getLogout()
    {
        Auth::logout();
        return Redirect::action('AuthController@getIndex');
    }

    public function getSuperProfile()
    {
//        return View
    }

    // GET /auth/superProfile
    // GET /auth/super_profile
}