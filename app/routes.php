<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


Route::get('/', ['as' => 'home', 'uses'=>'HomeController@showWelcome']);
Route::get('/create-case', ['as' => 'home', 'uses'=>'HomeController@showCreateCase']);

Route::get('/profile', ['as' => 'profile', "uses"=>'AuthController@getProfile']);

Route::get('/login', ['as' => 'login', "uses"=>'AuthController@getIndex']);

Route::controller('/auth', 'AuthController');
Route::controller('/password', 'RemindersController');



//---------------------- Public API -------------------------

Route::get('/api/stat', ['as'=>'api.stat', 'uses' =>'PublicApiController@getStat']);
Route::get('/api/cats', ['as'=>'api.cats', 'uses' =>'PublicApiController@getCategories']);
Route::get('/api/cats/{id}', ['as'=>'api.getCat', 'uses' =>'PublicApiController@getCategory']);

Route::get('/api/address', ['as'=>'api.address.get', 'uses' =>'PublicApiController@getAddress']);
Route::get('/api/address/regions', ['as'=>'api.address.get', 'uses' =>'PublicApiController@getAddressRegions']);
Route::get('/api/address/orgs', ['as'=>'api.address.get', 'uses' =>'PublicApiController@getAddressOrganizations']);



//---------------------- Cases API -------------------------
Route::get('/api/cases', ['as'=>'api.cases', 'uses' =>'CaseApiController@getAllCases']);
Route::get('/api/user/cases', ['as'=>'api.owner.cases', 'uses' =>'CaseApiController@getOwnerCases']);
Route::get('/api/users/{id}/cases', ['as'=>'api.user.cases', 'uses' =>'CaseApiController@getUserCases']);
Route::get('/api/cases/{id}', ['as'=>'api.cases.get', 'uses' =>'CaseApiController@getCase']);
Route::post('/api/cases', ['as'=>'api.cases.get', 'uses' =>'CaseApiController@postCase']);
Route::put('/api/cases/{id}', ['as'=>'api.cases.put', 'uses' =>'CaseApiController@putCase']);
Route::delete('/api/cases/{id}', ['as'=>'api.cases.delete', 'uses' =>'CaseApiController@deleteCase']);

//---------------------- Photos API -------------------------
Route::get('/api/cases/{id}/photos', ['as'=>'api.cases.getProblemPhotos', 'uses' =>'ApiController@getProblemPhotos']);
Route::get('/api/photos/{id}', ['as'=>'api.photos.getPhoto', 'uses' =>'ApiController@getPhoto']);
Route::get('/api/photos', ['as'=>'api.photos.getPhotos', 'uses' =>'ApiController@getPhotos']);
Route::post('/api/cases/{id}/photo', ['as'=>'api.cases.addPhoto', 'uses' =>'ApiController@postPhoto']);

//---------------------- Users API -------------------------
Route::get('/api/user/info', ['as'=>'api.user.info.get', 'uses' =>'ApiController@getUserInfo']);
Route::post('/api/user/info', ['as'=>'api.user.info.post', 'uses' =>'ApiController@postUserInfo']);

//-----------------------Regions -----------------------
Route::get('/api/regions', ['as'=>'api.regions.getRegions', 'uses' =>'RegionApiController@getRegions']);
Route::get('/api/regions/{id}', ['as'=>'api.regions.getRegion', 'uses' =>'RegionApiController@getRegion']);
Route::get('/api/cases/{id}/regions', ['as'=>'api.regions.getCaseRegions', 'uses' =>'RegionApiController@getCaseRegions']);
Route::post('/api/regions', ['as'=>'api.regions.postRegions', 'uses' =>'RegionApiController@postRegion']);
Route::put('/api/regions/{id}', ['as'=>'api.regions.putRegion', 'uses' =>'RegionApiController@putRegion']);
Route::delete('/api/regions/{id}', ['as'=>'api.regions.deleteRegion', 'uses' =>'RegionApiController@deleteRegion']);

//-----------------------Organization -----------------------
Route::get('/api/orgs', ['as'=>'api.organizations.getOrgs', 'uses' =>'OrganizationController@getOrganizations']);
Route::get('/api/orgs/{id}', ['as'=>'api.organizations.getOrg', 'uses' =>'OrganizationController@getOrganization']);
Route::get('/api/regions/{id}/orgs', ['as'=>'api.organizations.getRegionOrgs', 'uses' =>'OrganizationController@getRegionOrganizations']);
Route::get('/api/cases/{id}/orgs', ['as'=>'api.organizations.getCaseOrgs', 'uses' =>'OrganizationController@getCaseOrganizations']);
Route::get('/api/cats/{id}/orgs', ['as'=>'api.organizations.getRegionOrgs', 'uses' =>'OrganizationController@getCategoryOrganizations']);
Route::post('/api/orgs', ['as'=>'api.organizations.postOrgs', 'uses' =>'OrganizationController@postOrganization']);
Route::put('/api/orgs/{id}', ['as'=>'api.organizations.putOrg', 'uses' =>'OrganizationController@putOrganization']);
Route::post('/api/orgs/{id}', ['as'=>'api.organizations.putOrg', 'uses' =>'OrganizationController@postSingleOrganization']);
Route::delete('/api/orgs/{id}', ['as'=>'api.regions.deleteOrg', 'uses' =>'OrganizationController@deleteOrganization']);

//-----------------------Requests API -----------------------
Route::get('/api/requests', ['as'=>'api.requests.getRequests', 'uses' =>'RequestApiController@getRequests']);
Route::get('/api/requests/{id}', ['as'=>'api.requests.getRequest', 'uses' =>'RequestApiController@getRequest']);
Route::get('/api/cases/{id}/requests', ['as'=>'api.requests.getCaseRequests', 'uses' =>'RequestApiController@getCaseRequests']);
Route::get('/api/user/{id}/requests', ['as'=>'api.requests.getUserRequests', 'uses' =>'RequestApiController@getUserRequests']);
Route::post('/api/requests', ['as'=>'api.requests.postRequest', 'uses' =>'RequestApiController@postRequest']);
Route::put('/api/requests/{id}', ['as'=>'api.requests.putRequest', 'uses' =>'RequestApiController@putRequest']);
Route::delete('/api/requests/{id}', ['as'=>'api.requests.putRequest', 'uses' =>'RequestApiController@deleteRequest']);


//-----------------------Round-Robbin API -----------------------
Route::post('/api/cases/{id}/van', ['as'=>'api.cases.postVanFromCase', 'uses' =>'VanApiController@postVanFromCase']);
Route::put('/api/vans/{id}', ['as'=>'api.vans.putVan', 'uses' =>'VanApiController@putVan']);
Route::post('/api/vans', ['as'=>'api.vans.postVan', 'uses' =>'VanApiController@postVan']);
Route::get('/api/vans/{id}', ['as'=>'api.vans.getVan', 'uses' =>'VanApiController@getVan']);
Route::get('/api/vans', ['as'=>'api.vans.getVans', 'uses' =>'VanApiController@getVans']);
Route::delete('/api/vans/{id}', ['as'=>'api.vans.deleteVan', 'uses' =>'VanApiController@deleteVan']);

//---------------------- Auth API -------------------------

Route::get('/api/register/vkontakte', ['as'=>'api.register.vk', 'uses' =>'ApiAuthController@registerVk']);
Route::get('/api/register/facebook', ['as'=>'api.register.fb', 'uses' =>'ApiAuthController@registerFb']);
Route::get('/api/register/twitter', ['as'=>'api.register.tw', 'uses' =>'ApiAuthController@registerTw']);
Route::get('/api/register/google', ['as'=>'api.register.google', 'uses' =>'ApiAuthController@registerGoogle']);
Route::post('/api/register', ['as'=>'api.register', 'uses' =>'ApiAuthController@register']);
Route::post('/api/logout', ['as'=>'api.logout', 'uses' =>'ApiAuthController@logout']);
Route::post('/api/login', ['as'=>'api.login', 'uses' =>'ApiAuthController@login']);
//todo - this is start for new optimization
//Route::get('/api/auth/{serviceName}', ['as'=>'api.auth', 'uses' =>'ApiAuthController@authBySocial']);

//--------------------- News API -------------------------
Route::get('/api/news', ['as'=>'api.news', 'uses' =>'PublicApiController@getNews']);


//---------------------- Test API -------------------------

Route::get('/testApi/string/{requestString}', 'TestApiController@test');
Route::get('/testApi/env', 'TestApiController@testEnv');
Route::get('/testApi/category_group/{category_group}', function($cg){
    /**
     * @var $cat CategoryGroup
     */
    $cat = CategoryGroup::find($cg);
    return  $cat->categories;
});
Route::get('/testApi/refresh', 'TestApiController@refresh');
Route::get('/testApi/migrate', 'TestApiController@migrate');
Route::put('/testApi/file', 'TestApiController@file');
Route::get('/testApi/file', 'TestApiController@getfile');
Route::get('/testApi/email', 'TestApiController@email');


Route::get('/testApi/form', 'TestApiController@getform');
Route::any('/testApi/postform', 'TestApiController@postform');


