<?php
/**
 * Created by PhpStorm.
 * User: tim
 * Date: 05.12.14
 * Time: 17:35
 */

use \Chrisbjr\ApiGuard\ApiKey as ApiKey;

Response::macro('xml', function($vars, $status = 200, array $header = array(), $rootElement = 'root', $xml = null)
{

    if (is_object($vars) && $vars instanceof Illuminate\Support\Contracts\ArrayableInterface) {
        $vars = $vars->toArray();
    }

    if (is_null($xml)) {
        $xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><' . $rootElement . '/>');
    }
    foreach ($vars as $key => $value) {
        if (is_array($value)) {
            if (is_numeric($key)) {
                Response::xml($value, $status, $header, $rootElement, $xml->addChild(str_singular($xml->getName())));
            } else {
                Response::xml($value, $status, $header, $rootElement, $xml->addChild($key));
            }
        } else {
            $xml->addChild($key, $value);
        }
    }
    if (empty($header)) {
        $header['Content-Type'] = 'application/xml';
    }
    return Response::make($xml->asXML(), $status, $header);
});

//can't autoload unfortunately
ApiKey::macro( 'findOrCreate', function($user)
{
    $apiKey = ApiKey::where('user_id', '=', $user->id)->first();
    if (!isset($apiKey)) {
        $apiKey = new ApiKey;
        $apiKey->user_id = $user->id;
        $apiKey->level = 5;
        $apiKey->ignore_limits = 0;
    }
    $apiKey->key = $apiKey->generateKey();

    if (!$apiKey->save())
        $apiKey = null;

    return $apiKey;
});


//Route::get('api.{ext}', function()
//{
//    $data = ['status' => 'OK'];
//    $ext = File::extension(Request::url());
//    return Response::$ext($data);
//})->where('ext', 'xml|json');
