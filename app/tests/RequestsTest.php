<?php

use League\FactoryMuffin\Facade as FactoryMuffin;

class RequestsTest extends TestModelsCase
{

    protected $key = null;

    public function setUp()
    {
        parent::setUp();
        $_SESSION = array();
        $_SERVER['REQUEST_URI'] = 'http://krasimir.su/';
        $this->key = ApiAuthController::findOrCreateApiKey($this->user)->key;
    }

    public function testGetRequests()
    {
        $res = $this->call('GET', '/api/requests/', ['include' => 'owner'], [], [$this->AuthorizationHeader => $this->rootKey]);
        $requests = json_decode($res->getContent());
        $this->assertNotNull($requests);
        $this->assertCount(Letter::count(), $requests->data);
        $this->assertNotNull($requests->data[1]->owner);
    }

    public function testGetCaseRequestsFail()
    {
        $res = $this->call('GET', '/api/cases/'. 666 . '/requests/', ['include' => 'owner'], [], [$this->AuthorizationHeader => $this->key]);
        $requests = json_decode($res->getContent());
        $this->assertEquals(404, $requests->error->http_code);
    }

    public function testGetCaseRequestsOk()
    {
        $res = $this->call('GET', '/api/cases/'. $this->problem->id . '/requests/', ['include' => 'owner'], [], [$this->AuthorizationHeader => $this->key]);
        $requests = json_decode($res->getContent());
        $this->assertNotNull($requests->data);
    }

    public function testGetCaseRequestsFailsWrongUser()
    {
        $res = $this->call('GET', '/api/cases/'. $this->problem->id . '/requests/', ['include' => 'owner'], [], [$this->AuthorizationHeader => $this->key2]);
        $requests = json_decode($res->getContent());
        $this->assertEquals(403, $requests->error->http_code);
    }

    public function testGetRequest()
    {
        $res = $this->call('GET', '/api/requests/' . $this->request->id, ['include' => 'owner'], [], [$this->AuthorizationHeader => $this->key]);
        $request = json_decode($res->getContent());
        $this->assertNotNull($request);
        $this->assertEquals($this->request->text, $request->data->text);
        $this->assertNotNull($request->data->owner);
    }

    public function testGetUserRequests()
    {
        $res = $this->call('GET', '/api/user/' . $this->user->id . '/requests', ['include' => 'owner'], [], [$this->AuthorizationHeader => $this->key]);
        $request = json_decode($res->getContent());
        $this->assertNotNull($request);
        $this->assertEquals($this->user->id, $request->data[0]->owner->data->id);
    }

    public function testGetUserRequestsFail()
    {
        $res = $this->call('GET', '/api/user/' . 666 . '/requests', ['include' => 'owner'], [], [$this->AuthorizationHeader => $this->key]);
        $request = json_decode($res->getContent());
        $this->assertEquals(404, $request->error->http_code);
    }

    public function testGetUserRequestsFailWrongUser()
    {
        $res = $this->call('GET', '/api/user/' . $this->user->id  . '/requests', ['include' => 'owner'], [], [$this->AuthorizationHeader => $this->key2]);
        $request = json_decode($res->getContent());
        $this->assertEquals(403, $request->error->http_code);
    }

    public function testPostRequest()
    {
        $text = 'asdfawser awer waqer awer wqer waerwq e';
        $data = ['problem' => $this->problem->id, 'text' => $text];
        $res = $this->call('POST', '/api/requests/', ['organizations' => $this->organization->id], [], [$this->AuthorizationHeader => $this->key], json_encode($data));
        $req = json_decode($res->getContent());

        $this->assertEquals($this->user->id, $req->data[0]->owner->data->id );
        $this->assertEquals($text, $req->data[0]->text);
        $this->assertContains('уважением', $req->data[0]->footer);
    }

    public function testPutRequest()
    {
        $data = ['status' => Letter::Rejection];
        $res = $this->call('Put', '/api/requests/' . $this->request->id, [], [], [$this->AuthorizationHeader => $this->key], json_encode($data));
        $req = json_decode($res->getContent());
        $this->assertEquals($this->user->id, $req->data->owner->data->id );
        $this->assertEquals(Letter::Rejection, $req->data->status);
    }

    public function testPutRequestFail()
    {
        $data = ['status' => Letter::Rejection];
        $res = $this->call('Put', '/api/requests/' . 666, [], [], [$this->AuthorizationHeader => $this->key], json_encode($data));
        $req = json_decode($res->getContent());
        $this->assertEquals(404, $req->error->http_code);
    }

    public function testPutRequestFailWrongUser()
    {
        $data = ['status' => Letter::Rejection];
        $res = $this->call('Put', '/api/requests/' . $this->request->id, [], [], [$this->AuthorizationHeader => $this->key2], json_encode($data));
        $req = json_decode($res->getContent());
        $this->assertEquals(403, $req->error->http_code);
    }

    public function testPutRequestRoot()
    {
        $data = ['status' => Letter::Rejection];
        $res = $this->call('Put', '/api/requests/' . $this->request->id, [], [], [$this->AuthorizationHeader => $this->rootKey], json_encode($data));
        $req = json_decode($res->getContent());
        $this->assertEquals(Letter::Rejection, $req->data->status);
    }

    public function testDeleteRequest()
    {
        $response = $this->call('DELETE', '/api/requests/' . $this->request->id, [], [], [$this->AuthorizationHeader => $this->key]);
        $req = json_decode($response->getContent());
        $this->assertEquals('ok', $req->result);
        $response = $this->call('GET', '/api/requests/' . $this->request->id, [], [], [$this->AuthorizationHeader => $this->key]);
        $req = json_decode($response->getContent());
        $this->assertEquals(404, $req->error->http_code);
    }

    public function testDeleteRequestByRoot()
    {
        $response = $this->call('DELETE', '/api/requests/' . $this->request->id, [], [], [$this->AuthorizationHeader => $this->rootKey]);
        $req = json_decode($response->getContent());
        $this->assertEquals('ok', $req->result);
    }

    public function testDeleteRequestFailWrongUser()
    {
        $response = $this->call('DELETE', '/api/requests/' . $this->request->id, [], [], [$this->AuthorizationHeader => $this->key2]);
        $req = json_decode($response->getContent());
        $this->assertEquals(403, $req->error->http_code);
    }

    public function testDeleteRequestFailWrongId()
    {
        $response = $this->call('DELETE', '/api/requests/' . 666, [], [], [$this->AuthorizationHeader => $this->key]);
        $req = json_decode($response->getContent());
        $this->assertEquals(403, $req->error->http_code);
    }

    public function testGetAllLettersWithMultiStatus()
    {
        FactoryMuffin::seed(10, 'Letter', ['status' => 'Rejection']);
        FactoryMuffin::seed(10, 'Letter', ['status' => 'Waiting']);

        $res = $this->call('GET', '/api/requests', ['count'=>15,'status' => 'Rejection, Waiting'], [], [$this->AuthorizationHeader => $this->rootKey]);
        $cases = json_decode($res->getContent());

        $this->assertCount(15, $cases->data);
    }

    public function testGetAllLettersWithOrganization()
    {
        FactoryMuffin::seed(10, 'Letter', ['organization_id' => '2']);
        FactoryMuffin::seed(10, 'Letter', ['organization_id' => '3']);

        $res = $this->call('GET', '/api/requests', ['count'=>15,'organization' => '2,3'], [], [$this->AuthorizationHeader => $this->rootKey]);
        $cases = json_decode($res->getContent());
        $this->assertCount(15, $cases->data);
    }


    public function testGetAllLettersWithRegion()
    {
        $org1 = FactoryMuffin::create('Problem');
        Region::find(2)->problems()->saveMany([$org1]);
        $org2 = FactoryMuffin::create('Problem');
        Region::find(3)->problems()->saveMany([$org2]);

        FactoryMuffin::seed(5, 'Letter', ['problem_id' => $org1->id]);
        FactoryMuffin::seed(5, 'Letter', ['problem_id' => $org2->id]);

        $res = $this->call('GET', '/api/requests', ['count'=>15,'region' => '2,3'], [], [$this->AuthorizationHeader => $this->rootKey]);
        $cases = json_decode($res->getContent());

        $this->assertCount(10, $cases->data);
    }

    public function testGetAllLettersWithCategory()
    {
        $org1 = FactoryMuffin::create('Problem');
        Category::find(1)->problems()->saveMany([$org1]);
        $org2 = FactoryMuffin::create('Problem');
        Category::find(3)->problems()->saveMany([$org2]);

        FactoryMuffin::seed(5, 'Letter', ['problem_id' => $org1->id]);
        FactoryMuffin::seed(5, 'Letter', ['problem_id' => $org2->id]);

        $res = $this->call('GET', '/api/requests', ['count'=>15,'category' => '1,3'], [], [$this->AuthorizationHeader => $this->rootKey]);
        $cases = json_decode($res->getContent());

        $this->assertCount(10, $cases->data);
    }



    public function testGetAddressByCoords()
    {
        $response = $this->call('GET', '/api/address?lat=59.970339&lon=30.364988', [], [], []);
        $req = json_decode($response->getContent());
        var_dump($req);
        $this->assertNotNull($req);
        $this->assertEquals($req->results[0]->components->state, "Saint Petersburg");
    }


}