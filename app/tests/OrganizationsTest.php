<?php
/**
 * Created by PhpStorm.
 * User: tim
 * Date: 05.12.14
 * Time: 16:28
 */

use League\FactoryMuffin\Facade as FactoryMuffin;
use Guzzle\Service\Command\LocationVisitor\Request as GzRequest;


class OrganizationsTest extends TestModelsCase
{

    public function setUp()
    {
        parent::setUp();
        $_SESSION = array();
        $_SERVER['REQUEST_URI'] = 'http://krasimir.su/';
    }

    public function testGetOrgs()
    {
        FactoryMuffin::seed(15, 'Organization');
        $response = $this->call('GET', '/api/orgs', ['count' => 15], [], [$this->AuthorizationHeader => $this->key]);
        $orgs = json_decode($response->getContent());
        $this->assertEquals(0, $orgs->meta->cursor->current);
        $this->assertEquals(15, $orgs->meta->cursor->count);
        $this->assertCount(15, $orgs->data);
    }

    public function testGetOrg()
    {
        $response = $this->call('GET', '/api/orgs/' . $this->organization->id, [], [], [$this->AuthorizationHeader => $this->key]);
        $org = json_decode($response->getContent());
        $this->assertNotNull($org);
        $this->assertEquals($this->organization->id, $org->data->id);
        $this->assertEquals($this->organization->name, $org->data->name);
    }

    public function testGetOrgWithMetaInformation()
    {
        $response = $this->call('GET', '/api/orgs/' . $this->organization->id, ['include'=>'cats,regions,requests'], [], [$this->AuthorizationHeader => $this->key]);
        $org = json_decode($response->getContent());
        $this->assertNotNull($org->data->cats);
        $this->assertNotNull($org->data->regions);
        $this->assertNotNull($org->data->requests);
    }

    public function testPostOrganizations()
    {
        $data = ['name' => 'Saint Petersburg governor', 'email' => 'gov@spb.ru', 'phone' => '+79134685232'];
        $response = $this->call('POST', '/api/orgs/', [], [], [$this->AuthorizationHeader => $this->rootKey], json_encode($data));
        $org = json_decode($response->getContent());
        $this->assertNotNull($org);
        $this->assertEquals($data['email'], $org->data->email);
    }

    public function testPutOrganization()
    {
        $data = ['name' => 'Saint Petersburg governor', 'email' => 'gov@spb.ru', 'phone' => '+79134685232'];
        $response = $this->call('PUT', '/api/orgs/' . $this->organization->id, [], [], [$this->AuthorizationHeader => $this->rootKey], json_encode($data));
        $org = json_decode($response->getContent());
        $this->assertNotNull($org);
        $this->assertEquals($data['email'], $org->data->email);
        $this->assertNotEquals($this->organization->name, $org->data->name);
        $this->assertEquals($this->organization->id, $org->data->id);
    }


    public function testPutOrganizationFailIfNotExist()
    {
        $data = ['name' => 'Saint Petersburg governor', 'email' => 'gov@spb.ru', 'phone' => '+79134685232'];
        $response = $this->call('PUT', '/api/orgs/' . 666, [], [], [$this->AuthorizationHeader => $this->rootKey], json_encode($data));
        $org = json_decode($response->getContent());
        $this->assertNotNull($org->error);
    }

    public function testPostSingleOrganizationFailIfNotExist()
    {
        $data = ['name' => 'Saint Petersburg governor', 'email' => 'gov@spb.ru', 'phone' => '+79134685232'];
        $response = $this->call('POST', '/api/orgs/' . 666, [], [], [$this->AuthorizationHeader => $this->rootKey], json_encode($data));
        $org = json_decode($response->getContent());
        $this->assertNotNull($org->error);
    }

    public function testDeleteOrganization()
    {
        $this->call('DELETE', '/api/orgs/' . $this->organization->id, [], [], [$this->AuthorizationHeader => $this->rootKey]);
        $response = $this->call('GET', '/api/orgs/' . $this->organization->id, [], [], [$this->AuthorizationHeader => $this->key]);
        $org = json_decode($response->getContent());
        $this->assertNotNull($org);
        $this->assertNotNull($org->error);
    }

    public function testGetOrgWithCategoryAndRegions()
    {
        $response = $this->call('GET', '/api/orgs/' . $this->organization->id, ['include' => 'regions,cats'], [], [$this->AuthorizationHeader => $this->key]);
        $org = json_decode($response->getContent());
        $this->assertNotNull($org->data->cats);
        $this->assertNotNull($org->data->regions);
        $this->assertCount($this->organization->categories->count(), $org->data->cats->data);
        $this->assertCount($this->organization->regions->count(), $org->data->regions->data);
    }

    public function testPostOrganizationsWithRegionsAndCats()
    {
        $reg = FactoryMuffin::create('Region');
        $cat = FactoryMuffin::create('Category');
        $data = ['name' => 'Saint Petersburg governor', 'email' => 'gov@spb.ru', 'phone' => '+79134685232', 'is_prefect' => false];
        $response = $this->call('POST', '/api/orgs/', ['regions' => $reg->id, 'cats' => $cat->id . ',' . $this->category->id, 'include' => 'regions,cats'], [], [$this->AuthorizationHeader => $this->rootKey], json_encode($data));
        $org = json_decode($response->getContent());
        $this->assertCount(1, $org->data->regions->data);
        $this->assertEquals($reg->name, $org->data->regions->data[0]->name);
        $this->assertCount(2, $org->data->cats->data);
        $this->assertEquals($cat->name, $org->data->cats->data[0]->name); //could be excluded
    }


    public function testPutOrganizationWithRegionsAndCats()
    {
        $reg = FactoryMuffin::create('Region');
        $cat = FactoryMuffin::create('Category');
        $data = ['name' => 'Saint Petersburg governor', 'email' => 'gov@spb.ru', 'phone' => '+79134685232'];
        $response = $this->call('PUT', '/api/orgs/' . $this->organization->id, ['regions' => $reg->id, 'cats' => $cat->id . ',' . $this->category->id, 'include' => 'regions,cats'], [], [$this->AuthorizationHeader => $this->rootKey], json_encode($data));
        $org = json_decode($response->getContent());
        $this->assertNotNull($org);
        $this->assertEquals($data['email'], $org->data->email);
        $this->assertNotEquals($this->organization->name, $org->data->name);
        $this->assertEquals($this->organization->id, $org->data->id);
        $this->assertCount(1, $org->data->regions->data); // old regions erased
        $this->assertEquals($reg->name, $org->data->regions->data[0]->name);
        $this->assertCount(2, $org->data->cats->data);
        $this->assertEquals($cat->name, $org->data->cats->data[1]->name); //could be excluded
    }

    public function testPostSingleOrganizationWithCatsAndRegions()
    {
        $reg = FactoryMuffin::create('Region');
        $cat = FactoryMuffin::create('Category');
        $response = $this->call('POST', '/api/orgs/' . $this->organization->id, ['regions' => $reg->id, 'cats' => $cat->id . ',' . $this->category->id, 'include' => 'regions,cats'], [], [$this->AuthorizationHeader => $this->rootKey]);
        $org = json_decode($response->getContent());
        $this->assertCount(2, $org->data->regions->data);
        $this->assertCount(3, $org->data->cats->data);
        $this->assertEquals($reg->organizations[0]->id, $org->data->id);
    }

    public function testGetRegionOrganizations()
    {
        $response = $this->call('GET', '/api/regions/' . $this->region->id . '/orgs', [], [], [$this->AuthorizationHeader => $this->key]);
        $orgs = json_decode($response->getContent());
        $this->assertNotNull($orgs);
        $this->assertCount(3, $orgs->data);
    }

    public function testGetRegionOrganizationsFails()
    {
        $response = $this->call('GET', '/api/regions/' . 666 . '/orgs', [], [], [$this->AuthorizationHeader => $this->key]);
        $orgs = json_decode($response->getContent());
        $this->assertNotNull($orgs->error);
    }

    public function testGetRegionOrganizationsSQLInj()
    {
        $response = $this->call('GET', '/api/regions/' . 'DELETE * from Users' . '/orgs', [], [], [$this->AuthorizationHeader => $this->key]);
        $orgs = json_decode($response->getContent());
        $this->assertNotNull($orgs->error);
    }

    public function testGetCaseOrganizations()
    {
        $region = new Region(['osmTag' => 'state', 'osmValue' => 'Saint Petersburg']);
        $region->save();

        $region2 = new Region(['osmTag' => 'state_district', 'osmValue' => 'Калининский район']);
        $region2->save();

        $organization = new Organization(['name' => 'УОЖ', 'email'=>'uozh@gov.spb.ru', 'isRegionDefault' => true]);
        $organization->save();
        $organization->regions()->save($region);

        $organization = new Organization(['name' => 'УОЖ2', 'email'=>'uozh2@gov.spb.ru', 'isRegionDefault' => true]);
        $organization->save();
        $organization->regions()->save($region2);

        $problem = Problem::create(['category' => $this->category->id, 'lat' => 59.9705253, 'long' => 30.3648796, 'user' => $this->root]);
        $problem->save();

        $response = $this->call('GET', '/api/cases/' . $problem->id . '/orgs', [], [], [$this->AuthorizationHeader => $this->key]);
        $orgs = json_decode($response->getContent());
        //var_dump($orgs);
        $this->assertCount(2, $orgs->data);
    }

    public function testRegionSearchWithOrgs()
    {
        $region = new Region(['osmTag' => 'state', 'osmValue' => 'Saint Petersburg']);
        $region->save();

        $organization = new Organization(['name' => 'УОЖ', 'email'=>'uozh@gov.spb.ru']);
        $organization->save();
        $organization->regions()->save($region);

        $orgs = Organization::searchOrganization(59.9705253, 30.3648796)->get(); //it's point in Vyborsky district in Saint Petersburg
        $this->assertCount(1, $orgs);
        $this->assertEquals($organization->id, $orgs[0]->id);
    }

    public function testRegionSearchWithOrgs2()
    {
        $region = new Region(['osmTag' => 'state', 'osmValue' => 'Saint Petersburg']);
        $region->save();

        $region2 = new Region(['osmTag' => 'state_district', 'osmValue' => 'Калининский район']);
        $region2->save();

        $organization = new Organization(['name' => 'УОЖ', 'email'=>'uozh@gov.spb.ru']);
        $organization->save();
        $organization->regions()->save($region);

        $organization = new Organization(['name' => 'УОЖ2', 'email'=>'uozh2@gov.spb.ru']);
        $organization->save();
        $organization->regions()->save($region2);

        $orgs = Organization::searchOrganization(59.9705253, 30.3648796)->get(); //it's point in Vyborsky district in Saint Petersburg
        $this->assertCount(2, $orgs);
    }


    public function testRegionSearchWithOrgsWithCat()
    {
        $region = new Region(['osmTag' => 'state', 'osmValue' => 'Saint Petersburg']);
        $region->save();

        $organization = new Organization(['name' => 'УОЖ2', 'email'=>'uozh2@gov.spb.ru']);
        $organization->save();
        $organization->regions()->save($region);

        $organization = new Organization(['name' => 'УОЖ', 'email'=>'uozh@gov.spb.ru']);
        $organization->save();
        $organization->regions()->save($region);
        $organization->categories()->save($this->category);


        $orgs = Organization::searchOrganizationWithCat(59.9705253, 30.3648796, $this->category); //it's point in Vyborsky district in Saint Petersburg
        $this->assertCount(1, $orgs->get());

    }

    public function testRegionSearchWithDefaultOrgs()
    {
        $region = new Region(['osmTag' => 'state', 'osmValue' => 'Saint Petersburg']);
        $region->save();

        $organization = new Organization(['name' => 'УОЖ2', 'email'=>'uozh2@gov.spb.ru']);
        $organization->save();
        $organization->regions()->save($region);

        $organization = new Organization(['name' => 'УОЖ', 'email'=>'uozh@gov.spb.ru', 'isRegionDefault' => true]);
        $organization->save();
        $organization->regions()->save($region);


        $orgs = Organization::searchOrganizationWithCat(59.9705253, 30.3648796, $this->category); //it's point in Vyborsky district in Saint Petersburg
        $this->assertCount(1, $orgs->get());

    }

    public function testRegionSearchWithDefaultOrgs2()
    {
        $region = new Region(['osmTag' => 'state', 'osmValue' => 'Saint Petersburg']);
        $region->save();

        $organization = new Organization(['name' => 'УОЖ2', 'email'=>'uozh2@gov.spb.ru']);
        $organization->save();
        $organization->regions()->save($region);
        $organization->categories()->save($this->category);

        $organization = new Organization(['name' => 'УОЖ', 'email'=>'uozh@gov.spb.ru', 'isRegionDefault' => true ]);
        $organization->save();
        //$organization->categories()->save($this->category);

        $orgs = Organization::searchOrganizationWithCat(59.9705253, 30.3648796, $this->category); //it's point in Vyborsky district in Saint Petersburg
        $this->assertCount(1, $orgs->get());

    }

}