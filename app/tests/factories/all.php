<?php
use League\FactoryMuffin\Facade as FactoryMuffin;

//Example
//FactoryMuffin::define('Message', array(
//    'user_id'      => 'factory|User',
//    'subject'      => 'sentence',
//    'message'      => 'text',
//    'phone_number' => 'randomNumber|8',
//    'created'      => 'date|Ymd h:s',
//    'slug'         => 'call|makeSlug|word',
//), function ($object, $saved) {
//    // we're taking advantage of the callback functionality here
//    $object->message .= '!';
//});

FactoryMuffin::define('User', array(
    'nickname' => 'unique:firstNameMale',
    'email' => 'unique:email',
    'name' => 'name',
    'surname' => 'lastName',
    'patronymic' => 'name',
    'password' => 'password',
    'accessLevel' => 0,
));

FactoryMuffin::define('Domain', array(
    'name' => 'word',
    'url' => 'unique:url',
    'region' => 'factory|Region',
));


FactoryMuffin::define('CategoryGroup', array(
        'name' => 'word|30',
        'description' => 'optional:sentence|10',
        'iconUrl' => 'optional:url',)
);

FactoryMuffin::define('Category', array(
        'name' => 'sentence|5)',
        'description' => 'optional:sentence|5',
        'textTemplate' => 'sentence|5',
        'keywords' => 'optional:sentence|10',)
);

FactoryMuffin::define('Option', array(
        'name' => 'word|30',
        'tag' => 'word|5',)
);

FactoryMuffin::define('OptionValue', array(
        'value' => 'word|30',
    )
);

FactoryMuffin::define('Region', array(
    'name' => 'word',
    'osmTag' => 'word',
    'osmValue' => 'word',
    'osmLevel' => 4,
    'centerLong' => 'longitude',
    'centerLat' => 'latitude'));

FactoryMuffin::define('Organization', array(
    'name' => 'word',
    'email' => 'email',
    'address' => 'address',
    'letterHeader' => 'word|60'
));

FactoryMuffin::define('Problem', array(
    'user' => 'factory|User',
    'lat' => 'latitude',
    'long' => 'longitude',
    'address' => 'address',
    'category' => 'factory|Category',
    'status' => 'Created'
));

FactoryMuffin::define('Photo', array(
    'user_id' => 'factory|User',
    'problem_id' => 'factory|Problem',
    'name' => '123_cercawml.jpg',
    'path' => '0/123_cercawml.jpg',
    'approved' => true,
    'before' => true,
));

FactoryMuffin::define('Van', array(
    'user' => 'factory|User',
    'problem' => 'factory|Problem',
    'address' => 'address',
    'text' => 'text',
));

FactoryMuffin::define('Letter', array(
    'user_id' => 'factory|User',
    'problem_id' => 'factory|Problem',
    'organization_id' => 'factory|Organization',
    'header' => 'text',
    'footer' => 'text',
    'text' => 'text',
));