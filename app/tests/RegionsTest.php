<?php
/**
 * Created by PhpStorm.
 * User: tim
 * Date: 05.12.14
 * Time: 16:28
 */

use Dmyers\Storage\Storage;
use League\FactoryMuffin\Facade as FactoryMuffin;
use Guzzle\Service\Command\LocationVisitor\Request as GzRequest;
use Symfony\Component\HttpFoundation\File\UploadedFile;


class RegionsTest extends TestModelsCase
{

    public function setUp()
    {
        parent::setUp();
        $_SESSION = array();
        $_SERVER['REQUEST_URI'] = 'http://krasimir.su/';
    }

    public function testGetRegions()
    {
        FactoryMuffin::seed(15, 'Region'); // create 15 + 3 regions to test cursor
        $response = $this->call('GET', '/api/regions', ['count' => 20], [], [$this->AuthorizationHeader => $this->key]);
        $regs = json_decode($response->getContent());
        $this->assertEquals(0, $regs->meta->cursor->current);
        $this->assertEquals(18, $regs->meta->cursor->count);
        $this->assertCount(18, $regs->data);
    }

    public function testGetRegion()
    {
        $response = $this->call('GET', '/api/regions/' . $this->region->id, [], [], [$this->AuthorizationHeader => $this->key]);
        $reg = json_decode($response->getContent());
        $this->assertNotNull($reg);
        $this->assertEquals($this->region->id, $reg->data->id);
    }

    public function testGetRegionWithOrgs()
    {
        $response = $this->call('GET', '/api/regions/' . $this->region->id, ['include' => 'organizations'], [], [$this->AuthorizationHeader => $this->key]);
        $reg = json_decode($response->getContent());
        $this->assertNotNull($reg->data->organizations);
        $this->assertCount($this->region->organizations->count(), $reg->data->organizations->data);
    }

    public function testPostRegion()
    {
        $data = ['name' => 'Saint Petersburg', 'center_latitude' => 67.456, 'center_longitude' => 67.456, 'osm_tag' => 'state', 'osm_value' => 'Санкт-Петербург'];
        $response = $this->call('POST', '/api/regions/', [], [], [$this->AuthorizationHeader => $this->rootKey], json_encode($data));
        $reg = json_decode($response->getContent());
        $this->assertNotNull($reg);
        $this->assertEquals($data['name'], $reg->data->name);
    }

    public function testMinimumPostData()
    {
        $data = []; //nothing!
        $response = $this->call('POST', '/api/regions/', [], [], [$this->AuthorizationHeader => $this->rootKey], json_encode($data));
        $reg = json_decode($response->getContent());
        $this->assertNotNull($reg->data->id);
    }

    public function testPutRegion()
    {
        $data = ['name' => 'New Name'];
        $response = $this->call('PUT', '/api/regions/' . $this->region->id, [], [], [$this->AuthorizationHeader => $this->rootKey], json_encode($data));
        $reg = json_decode($response->getContent());
        $this->assertNotNull($reg);
        $this->assertEquals($data['name'], $reg->data->name);
        $this->assertEquals($this->region->id, $reg->data->id);
    }


    public function testPutRegionFailsIfNotExist()
    {
        $data = ['name' => 'New Name'];
        $response = $this->call('PUT', '/api/regions/' . 666 , [], [], [$this->AuthorizationHeader => $this->rootKey], json_encode($data));
        $reg = json_decode($response->getContent());
        $this->assertNotNull($reg->error);
    }

    public function testDeleteRegion()
    {
        $resp = $this->call('DELETE', '/api/regions/' . $this->region->id, [], [], [$this->AuthorizationHeader => $this->rootKey]);
        $reg = json_decode($resp->getContent());
        $this->assertEquals('ok',$reg->result);
        $response = $this->call('GET', '/api/regions/' . $this->region->id, [], [], [$this->AuthorizationHeader => $this->rootKey]);
        $reg = json_decode($response->getContent());
        $this->assertNotNull($reg->error);
    }

    public function testRegionSearch()
    {
        $region = new Region(['osmTag' => 'state', 'osmValue' => 'Saint Petersburg']);
        $region->save();
        $regs = Region::searchRegions(59.9705253, 30.3648796)->get();
        $this->assertCount(1, $regs);
        $this->assertEquals($region->id, $regs[0]->id);
    }

    public function testRegion2Search()
    {
        $region = new Region(['osmTag' => 'state', 'osmValue' => 'Saint Petersburg']);
        $region->save();

        $region2 = new Region(['osmTag' => 'state_district', 'osmValue' => 'Калининский район']);
        $region2->save();

        $region3 = new Region(['osmTag' => 'state_district', 'osmValue' => 'Петроградский район']);
        $region3->save();

        $regs = Region::searchRegions(59.9705253, 30.3648796)->get(); //it's point in Vyborsky district in Saint Petersburg
        $this->assertCount(2, $regs);

    }

    public function testGetCaseRegions()
    {
        $region = new Region(['osmTag' => 'state', 'osmValue' => 'Saint Petersburg']);
        $region->save();

        $region2 = new Region(['osmTag' => 'state_district', 'osmValue' => 'Калининский район']);
        $region2->save();

        $region2 = new Region(['osmTag' => 'state_district', 'osmValue' => 'Выборгский район']);
        $region2->save();

        $problem = Problem::create(['category' => $this->category->id, 'lat' => 59.9705253, 'long' => 30.3648796, 'user' => $this->root]);
        $problem->save();

        $response = $this->call('GET', '/api/cases/' . $problem->id . '/regions', [], [], [$this->AuthorizationHeader => $this->key]);
        $regs = json_decode($response->getContent());

        $this->assertCount(2, $regs->data);
    }



}