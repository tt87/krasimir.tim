<?php
/**
 * Created by PhpStorm.
 * User: tim
 * Date: 06.02.15
 * Time: 16:16
 */

use League\FactoryMuffin\Facade as FactoryMuffin;

class TestModelsCase extends TestCase
{
    const AUTHORIZATION_HEADER = 'HTTP_Authorization';

    protected $category;
    protected $categoryGroup;
    protected $option;
    protected $value;
    protected $region;
    protected $domain;
    protected $user;
    protected $user2;
    protected $root;
    protected $key;
    protected $key2;
    protected $rootKey;
    protected $problem;
    protected $photo;
    protected $organization;
    protected $request;


    public function setUp()
    {
        parent::setUp();
        $this->categoryGroup = FactoryMuffin::seed(3, 'CategoryGroup')[1];

        $this->category = FactoryMuffin::seed(3, 'Category')[1];
        $this->categoryGroup->categories()->save($this->category);

        $opts = FactoryMuffin::seed(3, 'Option');
        $this->option = $opts[1];
        $this->category->options()->saveMany($opts);

        $vals = [FactoryMuffin::create('OptionValue', ['option' => $this->option->id, 'value' => 'one']),
            FactoryMuffin::create('OptionValue', ['option' => $this->option->id, 'value' => 'two']),
            FactoryMuffin::create('OptionValue', ['option' => $this->option->id, 'value' => 'three'])];
        $this->value = $vals[1];


        $this->root = FactoryMuffin::create('User');
        $this->root->accessLevel = User::RootLevel;
        $this->rootKey = ApiAuthController::findOrCreateApiKey($this->root)->key;

        $this->user = FactoryMuffin::create('User');
        $this->user->accessLevel = User::CommonUserLevel;
        $this->key = ApiAuthController::findOrCreateApiKey($this->user)->key;

        $this->user2 = FactoryMuffin::create('User');
        $this->user2->accessLevel = User::CommonUserLevel;
        $this->key2 = ApiAuthController::findOrCreateApiKey($this->user2)->key;

        $this->CreateRegions();


        $this->domain = FactoryMuffin::create('Domain', ['region' => $this->region->id]);
        $this->domain->admins()->save($this->user);

        $problems = FactoryMuffin::seed(3, 'Problem', ['category' => $this->category->id, 'user' => $this->user->id]);
        $this->problem = $problems[1];
        $this->region->problems()->saveMany($problems);

        $photos = FactoryMuffin::seed(3, "Photo");
        $this->photo = $photos[1];
        $this->problem->photoAfter = $photos[1]->id;
        $this->problem->photos()->saveMany($photos);

        $orgs = FactoryMuffin::seed(3, "Organization");
        $this->organization = $orgs[1];
        $this->region->organizations()->saveMany($orgs);

        $letters = FactoryMuffin::seed(3, "Letter");
        $this->request = $letters[1];
        $this->problem->letters()->saveMany($letters);
        $this->user->letters()->saveMany($letters);


        $this->category->organizations()->save($this->organization);
    }

    protected function CreateRegions()
    {
        $this->region = FactoryMuffin::seed(3, 'Region')[0];
//
//        $arr = ['name' => 'Санкт-Петербург', 'osmTag' => 'state', 'osmValue'=> 'Saint Petersburg', 'osmLevel' => 3, 'moderators' => [$this->user]];
//        $this->region = Region::create($arr);
//        var_dump($this->region);

        $this->region->moderators()->save($this->user);


    }


}