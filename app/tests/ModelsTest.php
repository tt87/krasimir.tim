<?php

use League\FactoryMuffin\Facade as FactoryMuffin;


class ModelTests extends TestModelsCase
{

    public function testRegionAndDomainLink()
    {
        $re = $this->region;
        $dom = $this->domain;
        $this->assertEquals($dom->id, $re->domains[0]->id);
        $this->assertEquals($dom->getRegion->id, $re->id);
    }

    public function testCategoryAndCategoryGroupLink()
    {
        $c = $this->categoryGroup->categories->first();
        $this->assertEquals($this->categoryGroup->name, $c->groups->first()->name);
    }

    public function testOptionAndCategoryLink()
    {
        $this->assertEquals($this->category->name, $this->category->options->first()->categories->first()->name);
    }

    public function testOptionValuesAndOptionLink()
    {
        $opt = $this->option;
        $val = $this->value;
        $this->assertEquals($val->value, $opt->values()->where('optionValues.id', '=', $val->id)->first()->value);
        $this->assertEquals($val->getOption->id, $opt->id);
    }

    public function testModeratorAndRegionLink()
    {
        $reg = $this->user->moderationRegions->first();
        $this->assertEquals($this->region->name, $reg->name);
        $this->assertEquals($this->user->id, $this->region->moderators()->first()->id);
    }

    public function testAdminAndDomainLink()
    {
        $dom = $this->user->adminDomains->first();
        $this->assertEquals($this->domain->name, $dom->name);
        $this->assertEquals($this->user->id, $this->domain->admins()->first()->id);
    }

    public function testProblemModel()
    {
        $problem = $this->problem;
        $reg = $this->problem->regions->first();
        $user = $this->problem->owner;

        $this->assertEquals($this->category->id, $problem->getCategory->id);
        $this->assertNotNull($this->category->problems()->where('problems.id', '=', $problem->id)->first());
        $this->assertEquals($this->region->name, $reg->name);
        $this->assertNotNull($this->region->problems()->where('problems.id', '=', $problem->id)->first());
        $this->assertEquals($this->user->name, $user->name);
        $this->assertNotNull($this->user->problems()->where('problems.id', '=', $problem->id)->first());
    }

    public function testUserCompleteness()
    {
        $user1 = FactoryMuffin::create('User', ['password' => '123', 'nickname' => 'az']);
        $user2 = FactoryMuffin::create('User', ['password' => '123', 'name' => 'Елена', 'patronymic' => 'Сергеевна', 'surname' => 'Макарова', 'email' => 'wer@ert.com', 'emailApproved' => true]);
        $user4 = FactoryMuffin::create('User', ['name' => 'Елена', 'patronymic' => 'Сергеевна', 'surname' => 'Макарова', 'address' => "Polikarpov's ally", 'onlyEmailAnswers' => true]);
        $user5 = FactoryMuffin::create('User', ['name' => 'Елена', 'patronymic' => 'Сергеевна', 'surname' => 'Макарова', 'email' => 'wer@qwe.com', 'address' => "Аллея Котельникова 13",]);

        $this->assertFalse($user1->isConsistent());
        $this->assertTrue($user2->isConsistent());
        $this->assertFalse($user4->isConsistent());
        $this->assertTrue($user5->isConsistent());
    }


    public function testUsersSearch()
    {
        $twuser0 = FactoryMuffin::create('User', ['google' => '09876']);
        $twuser = FactoryMuffin::create('User', ['twitter' => '12345']);
        $twuser1 = FactoryMuffin::create('User', ['twitter' => '54321', 'email' => 'qwe@qwe.qwe']);
        $twuser2 = FactoryMuffin::create('User', ['email' => 'asd@asd.asd']);

        $this->assertEquals($twuser->id, User::findUserBySocial('twitter', '12345')->id);
        $this->assertEquals($twuser2->id, User::findUserBySocial('twitter', 'wrong', 'asd@asd.asd')->id);
        $this->assertEquals($twuser2->id, User::findUserBySocial('twitter', null, 'asd@asd.asd')->id);
        $this->assertEquals($twuser2->id, User::findUserBySocial(null, null, 'asd@asd.asd')->id);
        $this->assertNull(User::findUserBySocial(null, null));
        $this->assertNull(User::findUserBySocial("tw", null));
        $this->assertNull(User::findUserBySocial("facebook", null));
        $this->assertEquals($twuser1->id, User::findUserBySocial(null, null, 'qwe@qwe.qwe')->id);
        $this->assertEquals($twuser1->id, User::findUserBySocial('twitter', '54321', null)->id);
    }

    public function testUserFindAndCreate()
    {
        $twuser0 = FactoryMuffin::create('User', ['google' => '09876']); // need for mass
        $twuser1 = FactoryMuffin::create('User', ['twitter' => '12345', 'email' => 'qwe@qwe.qwe']);
        $count = User::count();

        $this->assertEquals($twuser1->id, User::findOrCreateSocialUser('twitter', (object)['id' => '12345'])->id);
        $this->assertEquals($twuser1->id, User::findOrCreateSocialUser('twitter', (object)['id' => 'wrong', 'email' => 'qwe@qwe.qwe'])->id);
        $this->assertEquals($twuser1->id, User::findOrCreateSocialUser('twitter', (object)['id' => '12345', 'email' => 'qwe@qwe.qwe'])->id);
        $this->assertEquals($twuser1->id, User::findOrCreateSocialUser('google', (object)['id' => '12345', 'email' => 'qwe@qwe.qwe'])->id);

        User::findOrCreateSocialUser('google', (object)['id' => '56734', 'email' => 'asd@asd.asd']);
        $this->assertEquals($count + 1, User::count());
    }

    public function testUserCreate()
    {
        $userData = new stdClass();
        $userData->name = 'Тим';
        $userData->id = '234234';
        $user = User::createUserFromSocial('twitter', $userData);

        $this->assertEquals($userData->id, $user->twitter);
        $this->assertEquals($userData->name, $user->name);
    }

    public function testWrongSocialServiceName()
    {
        $this->assertException(function () {
            $userData = new stdClass();
            $userData->name = 'Тим';
            $userData->id = '234234';
            User::createUserFromSocial('qwerty', $userData);
        });
    }

    public function testLatinLettersInNameSavesAsNull()
    {
        $userData = new stdClass();
        $userData->surname = 'Nchuriv'; // Latin is not appropriate for Russian system
        $userData->id = '234234';
        $user = User::createUserFromSocial('facebook', $userData);
        $this->assertNull($user->surname);
    }

    public function testHasPatronymic()
    {
        $user6 = FactoryMuffin::create('User', ['name' => 'Елена', 'surname' => 'Макарова', 'hasPatronymic' => false, 'email' => '6@qwe.com', 'emailApproved' => true]);
        $this->assertTrue($user6->isConsistent());
    }

    public function testPatronymicAndAddress()
    {
        $user3 = FactoryMuffin::create('User', ['name' => 'Елена', 'surname' => 'Макарова', 'hasPatronymic' => false, 'address' => "Аллея Котельникова 13", 'onlyEmailAnswers' => false]);
        $this->assertTrue($user3->isConsistent());
    }

    public function testProblemIsReadyForRequest()
    {
        $opt = $this->category->options->first();
        $opt->canBeNull = false;
        $opt->save();
        $this->problem->setOptionValues([$opt->tag => null]);
        $this->assertFalse($this->problem->isReadyForRequest());
    }

    public function testProblemIsReadyForRequest2()
    {
        $arr = [];
        foreach ($this->category->options as $opt) {
            // $arr[$opt->tag] = ''; // its almost null ;)
            $opt->canBeNull = true;
            $opt->save();
        }
        $this->assertTrue($this->problem->isReadyForRequest());
    }

    public function testProblemOptionValues()
    {
        $this->problem->optionValues = 'cover=asphalt;number=3;'; //
        $this->assertEquals('asphalt', $this->problem->getOptionValues()['cover']);
    }

    public function testProblemOptionValues2()
    {
        $this->problem->optionValues = 'cover=asphalt;number=3;';
        $this->problem->setOptionValues(['number' => '4']);
        $this->assertNotContains('cover', $this->problem->getOptionValues());
        $this->assertEquals('4', $this->problem->getOptionValues()['number']);
    }

    public function testProblemOptionValues3()
    {
        $this->problem->setOptionValues(['number' => null]);
        $this->assertEquals(null, $this->problem->getOptionValues()['number']);
    }

    public function testOptionCheckWithoutCustomValues()
    {
        $this->option->isSingleChoice = true;
        $this->option->hasCustomValue = false;
        $this->option->canBeNull = false;
        $this->option->save();
        $this->assertFalse($this->option->checkValue('cover'));
        $this->assertFalse($this->option->checkValue(null));
        $this->assertFalse($this->option->checkValue(""));
        $this->assertTrue($this->option->checkValue($this->value->value));
    }

    public function testOptionCheckWithCustomValues()
    {
        $this->option->isSingleChoice = true;
        $this->option->hasCustomValue = true;
        $this->option->canBeNull = true;
        $this->option->save();
        $this->assertTrue($this->option->checkValue('cover'));
        $this->assertTrue($this->option->checkValue(null));
        $this->assertTrue($this->option->checkValue(""));
        $this->assertTrue($this->option->checkValue($this->value->value));
    }

    public function testOptionCheck()
    {
        $this->option->isSingleChoice = true;
        $this->option->hasCustomValue = false;
        $this->option->canBeNull = true;
        $this->option->save();
        $this->assertTrue($this->option->checkValue(null));
        $this->assertTrue($this->option->checkValue(""));

    }

    public function testOptionCheckMultiple()
    {
        $this->option->isSingleChoice = false;
        $this->option->hasCustomValue = false;
        $this->option->canBeNull = true;
        $this->option->save();
        $this->assertTrue($this->option->checkValue(null));
        $this->assertTrue($this->option->checkValue(""));
        $this->assertTrue($this->option->checkValue("one"));
        $this->assertTrue($this->option->checkValue("one|two"));
        $this->assertFalse($this->option->checkValue("one|"));
        $this->assertFalse($this->option->checkValue("one|custom"));
    }

    public function testOptionCheckMultiple2()
    {
        $this->option->isSingleChoice = false;
        $this->option->hasCustomValue = true;
        $this->option->canBeNull = false;
        $this->option->save();
        $this->assertFalse($this->option->checkValue(null));
        $this->assertFalse($this->option->checkValue(""));
        $this->assertTrue($this->option->checkValue("one"));
        $this->assertTrue($this->option->checkValue("custom"));
        $this->assertTrue($this->option->checkValue("one|two"));
        $this->assertTrue($this->option->checkValue("one|custom"));
        $this->assertFalse($this->option->checkValue("one|"));
    }


}
