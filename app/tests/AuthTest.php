<?php
/**
 * Created by PhpStorm.
 * User: tim
 * Date: 05.12.14
 * Time: 16:28
 */

use League\FactoryMuffin\Facade as FactoryMuffin;
use Guzzle\Service\Command\LocationVisitor\Request as GzRequest;


class ApiAuthTest extends TestCase
{

//    protected $AUTHORIZATION_HEADER = 'HTTP_X-Auth-Token';
//    protected $AUTHORIZATION_PARAMETER = 'X-Auth-Token';



    public function testLogin()
    {
        $_SESSION = array();
        $_SERVER['REQUEST_URI'] = '/api/login';
        $cred1 = ['email' => 't1@t.t', 'password' => '12345'];
        $maker = Hash::make('12345');
        $cred = ['email' => 't1@t.t', 'password' => $maker];
        $user = User::create($cred);
        $user->save();
        $user = User::find($user->id);
   //     var_dump($user);

        $res1 = $this->call('POST', '/api/login', [], [], [], json_encode($cred1));
        $key = json_decode($res1->getContent());
     //   var_dump($key);

        $this->assertEquals(200, $res1->getStatusCode());
        $this->assertEquals(40, strlen($key->data->key));
    }

    public function testWrongPassword()
    {
        $_SESSION = array();
        $_SERVER['REQUEST_URI'] = '/api/login';

        $cred1 = ['email' => 't2@t.t', 'password' => 'wrong']; //wrong password
        $cred = ['email' => 't2@t.t', 'password' => Hash::make('12345')];

        $u = User::create($cred);
        $u->save();

        $res1 = $this->call('POST', '/api/login', [], [], [], json_encode($cred1));
        $this->assertEquals(401, $res1->getStatusCode());
    }

    public function testWrongEmail()
    {
        $_SESSION = array();
        $_SERVER['REQUEST_URI'] = '/api/login';
        $cred1 = ['email' => 'wrong@t.t', 'password' => '12345']; //wrong password
        $cred = ['email' => 't2@t.t', 'password' => Hash::make('12345')];
        FactoryMuffin::create('User', $cred);

        $res1 = $this->call('POST', '/api/login', [], [], [], json_encode($cred1));

        $this->assertEquals(401, $res1->getStatusCode());
    }

    public function testAuthWorks()
    {
        $_SESSION = array();
        $_SERVER['REQUEST_URI'] = 'http://krasimir.su/api/logout';
        $this->PrepareUser();
        //method has authorizarionKey = true, but we don't send it, that should return error
        $res2 = $this->call('POST', '/api/logout', [], []);

        $this->assertEquals(401, $res2->getStatusCode());
    }


    public function testLogoutWithHeader()
    {
        $_SESSION = array();
        $_SERVER['REQUEST_URI'] = 'http://krasimir.su/api/logout';
        $user = $this->PrepareUser();

        $res2 = $this->call('POST', '/api/logout', [], [], [$this->AuthorizationHeader => $user->key]);

        $this->assertEquals(200, $res2->getStatusCode());
    }


    public function testLogoutWithParameter()
    {
        $_SESSION = array();
        $_SERVER['REQUEST_URI'] = 'http://krasimir.su/api/logout';
        $user = $this->PrepareUser();

        $res2 = $this->call('POST', '/api/logout', ['format' => 'json', $this->AuthorizationParameter => $user->key], [], []);

        $this->assertEquals(200, $res2->getStatusCode());
    }


    private function PrepareUser()
    {
        $cred = ['email' => 't@t.t', 'password' => Hash::make('12345')];
        $cred1 = ['email' => 't@t.t', 'password' => '12345'];
        $user = FactoryMuffin::create('User', $cred);
        $res1 = $this->call('POST', '/api/login', array('format' => 'json'), [], [], json_encode($cred1));
        $this->assertEquals(200, $res1->getStatusCode());
        $key = json_decode($res1->getContent());
        $user->key = $key->data->key;
        return $user;
    }

    public function testRegister()
    {
        $_SESSION = array();
        $_SERVER['REQUEST_URI'] = 'http://krasimir.su/api/register';

        $content = json_encode(['email' => 'ertxfghrt@ertz.we', 'password' => '12345']);

        $res = $this->call('POST', '/api/register', array(), [], [], $content);
        $key = json_decode($res->getContent());

        $this->assertEquals('200', $res->getStatusCode());
        $this->assertNotNull($key->data->key);
    }

    public function testRegisterWrongEmail()
    {
        $_SESSION = array();
        $_SERVER['REQUEST_URI'] = 'http://krasimir.su/api/register';

        $content = json_encode(['email' => 'wrong', 'password' => '12345']);

        $res = $this->call('POST', '/api/register', array(), [], [], $content);

        $this->assertEquals('400', $res->getStatusCode());
    }

    public function testRegisterWrongPass()
    {
        $_SESSION = array();
        $_SERVER['REQUEST_URI'] = 'http://krasimir.su/api/register';

        $content = json_encode(['email' => 'qwe@qwe.erf', 'password' => '123']); // too small, limits: 5-255

        $res = $this->call('POST', '/api/register', array(), [], [], $content);

        $this->assertEquals('400', $res->getStatusCode());
    }

    //todo this is start for future optimization
//    public function testAuthBySocial(){
//        $_SESSION = array();
//        $_SERVER['REQUEST_URI'] = 'http://krasimir.su/api/auth/vkontakte';
//
//        $res = $this->call('GET', '/api/auth/vkontakte', array(), [], []);
//
//        $this->assertEquals('302', $res->getStatusCode());
//        $this->assertContains('oauth.vk.com',$res->headers->get('Location'));
//    }

    public function testRegisterVk()
    {
        $_SESSION = array();
        $_SERVER['REQUEST_URI'] = 'http://krasimir.su/api/register/vkontakte';
        var_dump("This test needs handy check by link " . $_SERVER['REQUEST_URI'] . "\n");

        $res = $this->call('GET', '/api/register/vkontakte', array(), [], []);

        $this->assertEquals('302', $res->getStatusCode());
        $this->assertContains('oauth.vk.com', $res->headers->get('Location'));
    }

    public function testRegisterFb()
    {
        $_SESSION = array();
        $_SERVER['REQUEST_URI'] = 'http://krasimir.su/api/register/facebook';
        var_dump("This test needs handy check by link " . $_SERVER['REQUEST_URI'] . "\n");

        $res = $this->call('GET', '/api/register/facebook', array(), [], []);

        $this->assertEquals('302', $res->getStatusCode());
        $this->assertContains('facebook.com', $res->headers->get('Location'));

//        $client = new Guzzle\Http\Client();
//        $get =   $client->get($res->headers->get('Location'));
//        $payload = $get->send();
    }

    public function testRegisterGoogle()
    {
        $_SESSION = array();
        $_SERVER['REQUEST_URI'] = 'http://krasimir.su/api/register/google';
        var_dump("This test needs handy check by link " . $_SERVER['REQUEST_URI'] . "\n");

        $res = $this->call('GET', '/api/register/google', array(), [], []);

        $this->assertEquals('302', $res->getStatusCode());
        $this->assertContains('google.com', $res->headers->get('Location'));

        $client = new Guzzle\Http\Client();
        $get = $client->get($res->headers->get('Location'));
        $payload = $get->send();
        $this->assertEquals(200, $payload->getStatusCode());
    }


    public function testRegisterTwitter()
    {
        $_SESSION = array();
        $_SERVER['REQUEST_URI'] = 'http://krasimir.su/api/register/twitter';
        var_dump("This test needs handy check by link " . $_SERVER['REQUEST_URI'] . "\n");

        $res = $this->call('GET', '/api/register/twitter', array(), [], []);

        $this->assertEquals('302', $res->getStatusCode());
        $this->assertContains('twitter.com', $res->headers->get('Location'));

        $client = new Guzzle\Http\Client();
        $get = $client->get($res->headers->get('Location'));
        $payload = $get->send();
        $this->assertEquals(200, $payload->getStatusCode());
    }

}