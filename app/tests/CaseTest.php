<?php
/**
 * Created by PhpStorm.
 * User: tim
 * Date: 05.12.14
 * Time: 16:28
 */

use League\FactoryMuffin\Facade as FactoryMuffin;
use Guzzle\Service\Command\LocationVisitor\Request as GzRequest;
use Symfony\Component\HttpFoundation\File\UploadedFile;


class CaseTest extends TestModelsCase
{

    public function setUp()
    {
        parent::setUp();
        $_SESSION = array();
        $_SERVER['REQUEST_URI'] = 'http://krasimir.su/';
    }


    public function testHowWorksCursorAndMetadata()
    {
        FactoryMuffin::seed(40, 'Problem');

        $res = $this->call('GET', '/api/cases', ['count' => 5], [], []);
        $cases = json_decode($res->getContent());

        $this->assertCount(5, $cases->data);
        $this->assertEquals(5, $cases->meta->cursor->count);
        $this->assertEquals(0, $cases->meta->cursor->current);
        $this->assertEquals(5, $cases->meta->cursor->next);

        $res2 = $this->call('GET', '/api/cases', ['cursor' => $cases->meta->cursor->next], [], []);
        $cases2 = json_decode($res2->getContent());

        $this->assertCount(15, $cases2->data);
        $this->assertEquals(15, $cases2->meta->cursor->count);
        $this->assertEquals(5, $cases2->meta->cursor->current);
        $this->assertEquals(20, $cases2->meta->cursor->next);
    }

    public function testGetAllCasesWithCategory()
    {
        FactoryMuffin::seed(10, 'Problem', ['category' => '3']);
        FactoryMuffin::seed(15, 'Problem', ['category' => '2']);

        $res = $this->call('GET', '/api/cases', ['count'=>15,'category' => '3'], [], []);
        $cases = json_decode($res->getContent());

        $this->assertCount(10, $cases->data);
    }

    public function testGetAllCasesWithCategoryGroup()
    {
        CategoryGroup::find(1)->categories()->saveMany([Category::find(3)]);
        FactoryMuffin::seed(10, 'Problem', ['category' => '3']);
        FactoryMuffin::seed(15, 'Problem', ['category' => '2']);

        $res = $this->call('GET', '/api/cases', ['count'=>15,'category_group' => '1'], [], []);
        $cases = json_decode($res->getContent());

        $this->assertCount(10, $cases->data);
    }

    public function testGetAllCasesWithRegion()
    {
        $problems = FactoryMuffin::seed(10, 'Problem', []);
        $problems2 = FactoryMuffin::seed(15, 'Problem', []);

        Region::find(3)->problems()->saveMany($problems);
        Region::find(2)->problems()->saveMany($problems2);


        $res = $this->call('GET', '/api/cases', ['count'=>30, 'region' => '2,3'], [], []);
        $cases = json_decode($res->getContent());

        $this->assertCount(25, $cases->data);
    }

    public function testGetAllCasesWithStatus()
    {
        FactoryMuffin::seed(10, 'Problem', ['status' => 'Closed']);
        FactoryMuffin::seed(5, 'Problem', ['status' => 'NotRelevant']);

        $res = $this->call('GET', '/api/cases', ['count'=>10,'status' => 'Closed'], [], []);
        $cases = json_decode($res->getContent());

        $this->assertCount(10, $cases->data);

        $res = $this->call('GET', '/api/cases', ['status' => 'NotRelevant'], [], []);
        $cases = json_decode($res->getContent());

        $this->assertCount(5, $cases->data);
    }

    public function testGetAllCasesWithMultiStatus()
    {
        FactoryMuffin::seed(10, 'Problem', ['status' => 'Closed']);
        FactoryMuffin::seed(5, 'Problem', ['status' => 'NotRelevant']);

        $res = $this->call('GET', '/api/cases', ['count'=>15,'status' => 'Closed, NotRelevant'], [], []);
        $cases = json_decode($res->getContent());

        $this->assertCount(15, $cases->data);
    }

    public function testGetAllCasesWithData()
    {
        FactoryMuffin::seed(40, 'Problem');

        $res = $this->call('GET', '/api/cases', ['from' => new DateTime('19.07.2015 13:42')], [], []);
        $cases = json_decode($res->getContent());
        $this->assertCount(15, $cases->data);

        $res = $this->call('GET', '/api/cases', ['from' => new DateTime('19.07.2020 13:42')], [], []);
        $cases = json_decode($res->getContent());
        $this->assertCount(0, $cases->data);

    }

    public function testGetOwnerCases()
    {
        $res = $this->call('GET', '/api/user/cases', [], [], [$this->AuthorizationHeader => $this->key]);
        $cases = json_decode($res->getContent());

        $this->assertCount($this->user->problems()->count(), $cases->data);
        $this->assertEquals($this->user->nickname, $cases->data[0]->owner);
    }


    public function testGetUserCasesByModerator()
    {
        $res = $this->call('GET', '/api/users/'. $this->user->id .'/cases', [], [], [$this->AuthorizationHeader => $this->rootKey]);
        $cases = json_decode($res->getContent());
        $this->assertCount($this->user->problems()->count(), $cases->data);
    }

    public function testGetUserCasesByCommonUserFails()
    {
        $res = $this->call('GET', '/api/users/'. $this->user->id .'/cases', [], [], [$this->AuthorizationHeader => $this->key2]);
        $cases = json_decode($res->getContent());
        $this->assertNotNull($cases->error);
    }

    public function testGetUserCasesByUser()
    {
        $res = $this->call('GET', '/api/users/'. $this->user->id .'/cases', [], [], [$this->AuthorizationHeader => $this->key]);
        $cases = json_decode($res->getContent());
        $this->assertCount($this->user->problems()->count(), $cases->data);
    }


    public function testGetCase()
    {
        $problem = $this->problem;
        $problem->setOptionValues(['cover' => 'asphalt']);
        $res = $this->call('GET', '/api/cases/' . $problem->id, ['include'=>'options,option_values,photos'], []);
        $case = json_decode($res->getContent());
        $this->assertEquals($problem->id, $case->data->id);
        $this->assertNotNull($case->data->options);
        $this->assertNotNull($case->data->option_values);
        $this->assertNotNull($case->data->photos);
        $this->assertEquals('asphalt', $case->data->option_values->data->cover);
    }

    public function testGetCaseFail()
    {
        $res = $this->call('GET', '/api/cases/' . 666 , [], []);
        $case = json_decode($res->getContent());
        $this->assertNotNull($case->error);
    }


    public function testMinimumPostCaseArgs()
    {
        $data = ['category' => $this->category->id]; //category enough
        $res = $this->call('POST', '/api/cases/', [], [], [$this->AuthorizationHeader => $this->key], json_encode($data));
        $case = json_decode($res->getContent());
        $this->assertEquals($this->user->nickname, $case->data->owner);
        $this->assertFalse($case->data->can_request);
    }

    public function testPutCaseAndOption()
    {
        $data = ['category' => $this->category->id, 'latitude' => 67.456, 'option_values' => ['cover' => 'asphalt']];
        $this->problem->optionValues = 'number=12321'; //getValue move to model???
        $this->problem->save();

        $res = $this->call('PUT', '/api/cases/' . $this->problem->id, ['include'=>'option_values'], [], [$this->AuthorizationHeader => $this->key], json_encode($data));
        $case = json_decode($res->getContent());

        $this->assertEquals('asphalt', $case->data->option_values->data->cover);
        $this->assertEquals('12321', $case->data->option_values->data->number);
    }

    public function testPutCaseFailIfNotExist()
    {
        $data = ['category' => $this->category->id];
        $res = $this->call('PUT', '/api/cases/' . 666, [], [], [$this->AuthorizationHeader => $this->key], json_encode($data));
        $case = json_decode($res->getContent());
        $this->assertEquals(404, $case->error->http_code);
    }


    public function testPutCaseFailIfWrongData()
    {
        $data = ['category' => 666];
        $res = $this->call('PUT', '/api/cases/' . $this->category->id, [], [], [$this->AuthorizationHeader => $this->key], json_encode($data));
        $case = json_decode($res->getContent());
        $this->assertNotNull($case->error);
    }

    public function testPutNotOwnCase()
    {
        $newProblem = FactoryMuffin::create('Problem'); // this is problem for different user
        $data = ['category' => $this->category->id, 'latitude' => 67.456, "longitude" => 76.5464];
        $res = $this->call('PUT', '/api/cases/' . $newProblem->id, [], [], [$this->AuthorizationHeader => $this->key], json_encode($data));
        $case = json_decode($res->getContent());
        $this->assertNotNull($case->error);
    }


    public function testDeleteCaseByUser()
    {
        $res = $this->call('DELETE', '/api/cases/' . $this->problem->id, [], [], [$this->AuthorizationHeader => $this->key]);
        $case = json_decode($res->getContent());
        $this->assertEquals('ok', $case->result);
    }

    public function testDeleteCase()
    {
        $res = $this->call('DELETE', '/api/cases/' . $this->problem->id, [], [], [$this->AuthorizationHeader => $this->key]);
        $case = json_decode($res->getContent());
        $this->assertEquals('ok', $case->result);
    }

    public function testDeleteCaseByAdmin()
    {
        $res = $this->call('DELETE', '/api/cases/' . $this->problem->id, [], [], [$this->AuthorizationHeader => $this->rootKey]);
        $case = json_decode($res->getContent());
        $this->assertEquals('ok', $case->result);
    }

    public function testDeleteCaseFailIfWrongUser()
    {
        $problem = $this->problem;
        $res = $this->call('DELETE', '/api/cases/' . $problem->id, [], [], [$this->AuthorizationHeader => $this->key2]);
        $case = json_decode($res->getContent());
        $this->assertEquals(403, $case->error->http_code);
    }


    public function testDeleteCaseFailIfWrongId()
    {
        $res = $this->call('DELETE', '/api/cases/' . 666 , [], [], [$this->AuthorizationHeader => $this->key]);
        $case = json_decode($res->getContent());
        $this->assertEquals(403, $case->error->http_code);
    }

    public function testPostWithZeroDataReturnsError()
    {
        $res = $this->call('POST', '/api/cases/', [], [], [$this->AuthorizationHeader => $this->key], json_encode(''));
        $case = json_decode($res->getContent());
        $this->assertEquals(400, $case->error->http_code);
    }

}