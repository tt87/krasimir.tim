<?php
/**
 * Created by PhpStorm.
 * User: tim
 * Date: 04.02.15
 * Time: 14:45
 */

use Dmyers\Storage\Storage;
use \Symfony\Component\HttpFoundation\File\UploadedFile;

class ExploringTest extends TestCase
{

    public function testRedirection()
    {
        $fakeUrl = 'http://krasimir.org/fake_url';
        $response = $this->action('POST', 'AuthController@postRegister', ['email' => 'tsd@tsdf.tsdf', 'nickname' => 't', 'password' => '12345', 'password2' => '12345', 'fiosign' => '12345', 'fioheader' => '12345',], [], [], ['HTTP_REFERER' => $fakeUrl]);

        $this->assertRedirectedTo($fakeUrl);
    }

    public function testGetRegisterContent()
    {
        $response = $this->call('GET', 'auth/register');

        $this->assertContains('Nickname', $response->getContent());
    }

    public function testPreg()
    {
        $pattern = '/^\p{Zs}*\p{Cyrillic}+\p{Zs}*$/u';
        $this->assertEquals(1, preg_match($pattern, 'Тимофей'));
        $this->assertEquals(0, preg_match($pattern, 'TimТим'));
        $this->assertEquals(0, preg_match($pattern, 'ТимTim'));
        $this->assertEquals(0, preg_match($pattern, 'Tim'));

        $this->assertEquals(1, preg_match($pattern, 'Тимофей  '));
        $this->assertEquals(1, preg_match($pattern, '  Тимофей'));
        $this->assertEquals(1, preg_match($pattern, '  Тимофей   '));
    }

    public function testFile()
    {

        exec("echo 123 >./app/tests/indata/qqq.txt"); //garanties that file exist
        $f = new UploadedFile('./app/tests/indata/qqq.txt', 'hello2', null, null, null, false);

        $this->assertNotNull($f);

        $res = $this->call('PUT', '/testApi/file', [], ['hello' => $f]);
        $this->assertNotNull($res);

        $res2 = $this->call('GET', '/testApi/file', [], []);
        $file = $res2->getContent();
        $this->assertNotNull($file);
    }


    public function testStorage()
    {
        $t = Storage::put('test/testfile.txt', "123");
        $t = Storage::exists('test/testfile.txt');
        $this->assertTrue($t);
    }

    public function testTime()
    {
        $t = new DateTime();
        //var_dump($t->getTimestamp());
    }

    public function testStorageUrl()
    {
        $t = Storage::url('avatar');
        $this->assertEquals("http://krasimir.su/photos/avatar", $t);
    }


}