<?php
/**
 * Created by PhpStorm.
 * User: tim
 * Date: 05.12.14
 * Time: 16:28
 */

use League\FactoryMuffin\Facade as FactoryMuffin;
use Guzzle\Service\Command\LocationVisitor\Request as GzRequest;
use Symfony\Component\HttpFoundation\File\UploadedFile;


class ApiTest extends TestModelsCase
{

    public function setUp()
    {
        parent::setUp();
        $_SESSION = array();
        $_SERVER['REQUEST_URI'] = 'http://krasimir.su/';

    }

    /**
     * @expectedException \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function testPutOnUnknownId1()
    {
        $this->call('PUT', '/api/notpath/', ['include'=>'option_values'], [], [$this->AuthorizationHeader => $this->key]);
    }

    /**
     * @expectedException \Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException
     */
    public function testPutOnUnknownId()
    {
        $data = ['category' => $this->category->id, 'latitude' => 67.456, 'option_values' => ['cover' => 'asphalt']];
        $this->call('PUT', '/api/cases/', ['include'=>'option_values'], [], [$this->AuthorizationHeader => $this->key], json_encode($data));
    }

    public function testGetUserCasesByUser()
    {
        $apiKey = ApiAuthController::findOrCreateApiKey($this->user);
        $this->assertEquals($this->user->id, $apiKey->user_id);
        $this->assertEquals($this->key, $apiKey->key);
        $this->assertNotEquals($this->key, $apiKey->rootKey);
    }



    public function testGetStat()
    { //todo append needed fields
        $response = $this->call('GET', '/api/stat', []);
        $obj = json_decode($response->getContent());
        $this->assertNotNull($obj->users_total);
        $this->assertNotNull($obj->cases_total);
        $this->assertNotNull($obj->cases_solved);
    }

    public function testGetCategories()
    {
        $response = $this->action('GET', 'PublicApiController@getCategories', [], []);
        $obj = json_decode($response->getContent());
        $this->assertCount(CategoryGroup::count(), $obj->data);
    }

    public function testGetCategory()
    {
        $response = $this->call('GET', '/api/cats/' . $this->category->id . '?include=options', []);
        $cat = json_decode($response->getContent());
        $this->assertEquals($this->category->id, $cat->data->id);
        $this->assertNotNull($cat->data->options);
        $this->assertCount(count($this->category->options), $cat->data->options->data);
    }

    //todo make role-based security first
//    public function testGetCategoryWithOrgs(){
//        $response = $this->call('GET', '/api/cats/' . $this->category->id, ['include'=>'orgs'],[],[$this->AuthorizationHeader=>$this->key]);
//        $cat = json_decode($response->getContent());
//        $this->assertNotNull($cat->data->orgs);
//        $this->assertCount($this->category->organizations->count(),$cat->data->orgs->data);
//    }

    public function testGetCategoryWithSafeDelete()
    {
        $cat = $this->category;
        $cat->delete();

        $response2 = $this->call('GET', '/api/cats/' . $cat->id, []);
        $res = json_decode($response2->getContent());

        $cat->restore(); // hack for factoryMuffin delete procedure

        $this->assertTrue($res->data->is_deleted);
    }

    public function testCategoryAndUserIncludes()
    {
        $res = $this->call('GET', '/api/cases', [], [], []);
        $cases = json_decode($res->getContent());

        $this->assertCount(Problem::count(), $cases->data);
        $this->assertFalse(isset($cases->data[0]->user));
    }

    public function testGetCategoryOrganizations()
    {
        $response = $this->call('GET', '/api/cats/' . $this->category->id . '/orgs', [], [], [$this->AuthorizationHeader => $this->key]);
        $orgs = json_decode($response->getContent());
        $this->assertNotNull($orgs);
        $this->assertCount(1, $orgs->data);
    }

    public function testGetUserData()
    {
        $res = $this->call('GET', '/api/user/info', [], [], [$this->AuthorizationHeader => $this->key]);
        $user = json_decode($res->getContent());

        $this->assertNotNull($user);
        $this->assertEquals($this->user->surname, $user->data->family_name);
        $this->assertFalse($user->data->can_send);
    }

    public function testPostUserData()
    {
        $data2 = ['family_name' => 'Копелиович', 'email' => 'r@r.r'];
        $res = $this->call('POST', '/api/user/info', [], [], [$this->AuthorizationHeader => $this->key], json_encode($data2));
        $newUser = json_decode($res->getContent());
        $user2 = User::find($this->user->id);

        $this->assertNotNull($newUser);
        $this->assertEquals($user2->surname, $newUser->data->family_name);
        $this->assertEquals($user2->surname, $data2['family_name']);

        $this->assertNotEquals($this->user->surname, $user2->surname); //surname changed
        $this->assertFalse($newUser->data->can_send);
        $this->assertFalse((boolean)$user2->emailApproved);// SQLite save bool as bits and converts them to string
    }

    public function testPhotoUpload()
    {
        $data = ['before' => true, 'after' => false];
        $problem = $this->problem;
        $f = new UploadedFile(public_path() . '/../app/tests/indata/www.jpg', 'photo2', null, null, null, false);
        $this->assertNotNull($f);
        $res = $this->call('POST', '/api/cases/' . $problem->id . '/photo', [], ['photo' => $f], [$this->AuthorizationHeader => $this->key], json_encode($data));
        $photo = json_decode($res->getContent());
        $this->assertNotNull($photo);
        $this->assertStringStartsWith((string)floor($this->problem->id / 1000), $photo->data->path);
        $this->assertEquals(Photo::find($photo->data->id)->name, $photo->data->name);
    }

    public function testGetProblemPhotos()
    {
        $problem = $this->problem;
        $res = $this->call('GET', '/api/cases/' . $problem->id . '/photos', [], [], [$this->AuthorizationHeader => $this->key]);
        $photos = json_decode($res->getContent());
        $this->assertNotNull($photos);
        $this->assertEquals(Photo::find($photos->data[1]->id)->name, $this->photo->name);
    }

    public function testGetPhoto()
    {
        $res = $this->call('GET', '/api/photos/' . $this->photo->id, [], [], [$this->AuthorizationHeader => $this->key]);
        $photo = json_decode($res->getContent());
        $this->assertNotNull($photo);
        $this->assertEquals(Photo::find($photo->data->id)->name, $this->photo->name);
    }

    public function testGetPhotos()
    {
        $res = $this->call('GET', '/api/photos/', array('format' => 'json', "count" => "2"), [], [$this->AuthorizationHeader => $this->key]);
        $photos = json_decode($res->getContent());
        $this->assertNotNull($photos);
        $this->assertCount(2, $photos->data);  //we assumed that there aer just 3 photo in storage

        $cursor = $photos->meta->cursor->next;
        $res = $this->call('GET', '/api/photos/', array('format' => 'json', "cursor" => $cursor, "count" => "2"), [], [$this->AuthorizationHeader => $this->key]);
        $photos2 = json_decode($res->getContent());
        $this->assertNotNull($photos2);
        $this->assertCount(1, $photos2->data);  //we assumed that there aer just 3 photo in storage
    }

    public function testGetAddressRegions()
    {
        $region = new Region(['osmTag' => 'state', 'osmValue' => 'Saint Petersburg']);
        $region->save();

        $region2 = new Region(['osmTag' => 'state_district', 'osmValue' => 'Калининский район']);
        $region2->save();

        $region3 = new Region(['osmTag' => 'state_district', 'osmValue' => 'Выборгский район']);
        $region3->save();

        $response = $this->call('GET', '/api/address/regions', ['lat' => 59.9705253, 'lon' => 30.3648796], [], [$this->AuthorizationHeader => $this->key]);
        $regs = json_decode($response->getContent());

        $this->assertCount(2, $regs->data);
    }


    public function testGetAddressOrgs()
    {
        $region = new Region(['osmTag' => 'state', 'osmValue' => 'Saint Petersburg']);
        $region->save();

        $region2 = new Region(['osmTag' => 'state_district', 'osmValue' => 'Калининский район']);
        $region2->save();

        $organization = new Organization(['name' => 'УОЖ', 'email'=>'uozh@gov.spb.ru', 'isRegionDefault' => true]);
        $organization->save();
        $organization->regions()->save($region);

        $organization = new Organization(['name' => 'УОЖ2', 'email'=>'uozh2@gov.spb.ru', 'isRegionDefault' => true]);
        $organization->save();
        $organization->regions()->save($region2);

        $response = $this->call('GET', '/api/address/orgs', ['lat' => 59.9705253, 'lon' => 30.3648796], [], [$this->AuthorizationHeader => $this->key]);
        $regs = json_decode($response->getContent());

        $this->assertCount(2, $regs->data);
    }

}