<?php
/**
 * Created by PhpStorm.
 * User: tim
 * Date: 04.04.15
 * Time: 20:15
 */

use League\FactoryMuffin\Facade as FactoryMuffin;
use Guzzle\Service\Command\LocationVisitor\Request as GzRequest;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class VanTest extends TestModelsCase
{
    protected $key;
    protected $rootKey;

    public function setUp()
    {
        parent::setUp();
        $_SESSION = array();
        $_SERVER['REQUEST_URI'] = 'http://krasimir.su/';


        $this->key = ApiAuthController::findOrCreateApiKey($this->user)->key;
        $this->rootKey = ApiAuthController::findOrCreateApiKey($this->root)->key;

        $this->problem->status = Problem::Success; // it grantees that van can be created for problem
        $this->problem->save();
    }

    public function testVanCreateFromProblem()
    {
        $van = Van::createForProblem($this->problem->id);
        $this->assertNotNull($this->problem->van);
        $this->assertEquals($this->problem->id, $van->problem);
        $this->assertNotNull($van->photoAfter);
        $this->assertTrue(Storage::exists($van->photoAfter));
    }

    public function testPostVanForProblem()
    {
        $res = $this->call('POST', '/api/cases/' . $this->problem->id . "/van", [], [], [$this->AuthorizationHeader => $this->rootKey]);
        $van = json_decode($res->getContent());
        $this->assertNotNull($this->problem->van);
        $this->assertNotNull($van);
        $this->assertNotNull($van->data->id);
    }

    public function testCommonUserCantMakeVan()
    {
        $res = $this->call('POST', '/api/cases/' . $this->problem->id . "/van", [], [], [$this->AuthorizationHeader => $this->key]);
        $van = json_decode($res->getContent());
        $this->assertNotNull($van->error);
    }

    public function testPostVanFailsForNotSuccessCases()
    {
        $this->problem->status = Problem::Closed;
        $this->problem->save();
        $res = $this->call('POST', '/api/cases/' . $this->problem->id . "/van", [], [], [$this->AuthorizationHeader => $this->rootKey]);
        $van = json_decode($res->getContent());
        $this->assertNull($this->problem->van);
        $this->assertNotNull($van->error);
    }

    public function testPutVanOk()
    {
        $van1 = Van::createForProblem($this->problem->id);
        $data2 = ['address' => 'Ленина 56', 'text' => 'Забор снесен'];
        $f = new UploadedFile(public_path() . '/../app/tests/indata/www.jpg', 'photo_before', null, null, null, false);
        $res = $this->call('PUT', '/api/vans/' . $van1->id, [], ["photo_before" => $f], [$this->AuthorizationHeader => $this->rootKey], json_encode($data2));
        $van = json_decode($res->getContent());
        $this->assertNotNull($van->data->id);
        $this->assertNotNull($this->problem->van->photoBefore);
        $this->assertEquals($data2["address"], $van->data->address);
    }

    public function testPutVanFailsIfNotExist()
    {
        $data2 = ['address' => 'Ленина 56', 'text' => 'Забор снесен'];
        $f = new UploadedFile(public_path() . '/../app/tests/indata/www.jpg', 'photo_before', null, null, null, false);
        $res = $this->call('PUT', '/api/vans/' . 666, [], ["photo_before" => $f], [$this->AuthorizationHeader => $this->rootKey], json_encode($data2));
        $van = json_decode($res->getContent());
        $this->assertNotNull($van->error);
    }

    public function testPostVan()
    {
        $data2 = ['address' => 'Ленина 56', 'text' => 'Забор снесен'];
        $f = new UploadedFile(public_path() . '/../app/tests/indata/www.jpg', 'photo_before', null, null, null, false);
        $res = $this->call('POST', '/api/vans', [], ["photo_before" => $f], [$this->AuthorizationHeader => $this->rootKey], json_encode($data2));
        $van = json_decode($res->getContent());
        $this->assertNotNull($van->data->id);
        $this->assertFalse(isset($van->data->problem));
        $this->assertEquals($data2["address"], $van->data->address);
    }

    public function testGetVan()
    {
        $van = Van::createForProblem($this->problem->id);
        $res = $this->call('GET', '/api/vans/' . $van->id, [], [], []);
        $vanRes = json_decode($res->getContent());
        $this->assertEquals($vanRes->data->id, $van->id);
        $this->assertNotNull($vanRes->data->owner->data->screen_name);
    }

    public function testGetVanFailsIfNotExist()
    {
        $res = $this->call('GET', '/api/vans/' . 666, [], [], []);
        $vanRes = json_decode($res->getContent());
        $this->assertNotNull($vanRes->error);
    }

    public function testGetVans()
    {
        $vans = FactoryMuffin::seed(20, 'Van');
        $van = $vans[18]; // desc order
        $res = $this->call('GET', '/api/vans', array('count' => '15'), [], []);
        $vanRes = json_decode($res->getContent());
        $this->assertEquals(15, count($vanRes->data));
        $this->assertEquals($vanRes->data[1]->id, $van->id);
    }


    public function testGetRandomVans()
    {
        FactoryMuffin::seed(20, 'Van');
        $res = $this->call('GET', '/api/vans', array('random' => '5'), [], []);
        $vanRes = json_decode($res->getContent());
        $this->assertEquals(5, count($vanRes->data));
    }

    public function testGetRandomVansShouldReturnNotMoreThenInDBNow()
    {
        FactoryMuffin::seed(20, 'Van');
        $res = $this->call('GET', '/api/vans', array('random' => '25'), [], []);
        $vanRes = json_decode($res->getContent());
        $this->assertEquals(20, count($vanRes->data));
    }

    public function testDeleteVan()
    {
        $van = Van::createForProblem($this->problem->id);
        $res = $this->call('DELETE', '/api/vans/' . $van->id, [], [], [$this->AuthorizationHeader => $this->rootKey]);
        $vanRes = json_decode($res->getContent());
        $this->assertEquals($vanRes->result, 'ok');
        $this->assertEquals(Van::find($van->id), null);
    }

    public function testDeleteNotExistedVan()
    {
        $res = $this->call('DELETE', '/api/vans/' . '666', [], [], [$this->AuthorizationHeader => $this->rootKey]);
        $vanRes = json_decode($res->getContent());
        $this->assertEquals($vanRes->result, 'ok');
        $this->assertEquals(Van::find(666), null);
    }
}