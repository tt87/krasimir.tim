// http://ryankent.ca/starting-a-new-project-with-laravel-5-gulp-bower-and-bootstrap/

var gulp = require('gulp'),
    less = require('gulp-less'),
    ln   = require('gulp-sym'),
    glue = require('gulp-sprite-glue');

var paths = {
    'dev': {
        'less': './resources/less/',
        'js': './resources/js/'
    },
    'prod': {
        'css': './public/css/',
        'js': './public/js/'
    }
};

gulp.task('sprites', function () {
    return gulp.src('./resources/sprites/*')
        .pipe(glue({
            img: './public/images/sprites',
            css: './public/css/sprites'
        }));
});

gulp.task('css', function () {
    return gulp.src(paths.dev.less + '**/*.less')
        .pipe(less())
        .pipe(gulp.dest(paths.prod.css));
});

gulp.task('sprites-and-css', ['sprites'], function () {
    return gulp.run('css');
});

gulp.task('js', function () {
    return gulp.src(paths.dev.js + '**/*.js')
        .pipe(ln(function (source) {
            return paths.prod.js + source.relative;
        }, { force: true }));
});

gulp.task('default', ['sprites-and-css', 'js']);