Krasivy Mir
===========

Requirements
------------
* php5
* postgresql
* apache
* npm
* glue (AUR: https://aur.archlinux.org/packages/glue/)

Useful commands
---------------
`setfacl -R -m u:http:rwx app/storage`

`./gulp default`