'use strict';

Krasimir.module('Model', function (Model, App, Backbone, Marionette) {
    Model.NewsItem = Backbone.Model.extend({
        getTitle: function () {
            if (!this.get('text').trim()) {
                return 'Без комментариев.';
            }

            var title = this.get('text').split('<br>')[0],
                sentenceEndSymbols = ['.', '?', '!'],
                correctedTitle = title;

            for (var i = 0, l = sentenceEndSymbols.length; i < l; i++) {
                var currentSymbol = sentenceEndSymbols[i],
                    splitted = correctedTitle.split(currentSymbol + ' ');

                if (splitted.length > 1) {
                    correctedTitle = splitted[0] + currentSymbol;
                }
            }

            return title.replace(/\[\w+\d+\|(.+?)\]/g, '$1');
        },

        getFirstPhoto: function () {
            var photo = _.findWhere(this.get('attachments'), {
                type: 'photo'
            });

            if (!photo) {
                var video = _.findWhere(this.get('attachments'), {
                    type: 'video'
                });

                if (!video) {
                    return '';
                }

                return video.video.image_big;
            }

            return photo.photo.src_big;
        }
    });
});

Krasimir.module('Collection', function (Collection, App, Backbone, Marionette) {
    Collection.News = Backbone.Collection.extend({
        url: '/api/news',

        model: App.Model.NewsItem,

        parse: function (response) {
            // TODO: it looks like a mess, we need to know why there is a number in response
            return _.filter(response.response, function (item) {
                return _.isObject(item);
            });
        }
    });
});