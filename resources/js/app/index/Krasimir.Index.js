'use strict';

Krasimir.module('Index', function (Index, App, Backbone, Marionette) {
    Index.NewsItemView = Marionette.ItemView.extend({
        template: '#template-news-block',

        className: 'side-block quarter',

        serializeData: function () {
            var date  = new Date(this.model.get('date') * 1000),
                formattedDate = [dec(date.getDate()), dec(date.getMonth() + 1), date.getFullYear()].join('.');

            return {
                id: this.model.get('id'),
                date: formattedDate,
                title: this.model.getTitle()
            };

            function dec(number) {
                return number < 10 ? '0' + number : number;
            }
        }
    });

    Index.NewsView = Marionette.CollectionView.extend({
        childView: Index.NewsItemView
    });

    Index.NewsImageView = Marionette.ItemView.extend({
        template: '#template-news-image',

        className: 'side-block quarter',

        serializeData: function () {
            return {
                firstImage: this.model.getFirstPhoto()
            }
        }
    });

    Index.NewsImagesView = Marionette.CollectionView.extend({
        childView: Index.NewsImageView
    });

    Index.Controller = Marionette.Controller.extend({
        initialize: function () {
            this.news = new App.Collection.News();
        },

        start: function () {
            this.showNews(this.news);
            this.news.fetch();
        },

        showNews: function (news) {
            var newsContent = new App.Index.NewsView({
                collection: news
            });
            App.root.newsContent.show(newsContent);

            var newsImages = new App.Index.NewsImagesView({
                collection: news
            });
            App.root.newsImages.show(newsImages);
        }
    });

    App.on('start', function () {
        App.root.addRegions({
            newsContent: '.news .news-content',
            newsImages: '.news .images-content'
        });

        var controller = new Index.Controller();

        controller.start();
    });
});