'use strict';

Krasimir.module('AddressMap', function (AddressMap, App, Backbone, Marionette) {
    var yaReady = $.Deferred();

    AddressMap.MainView = Marionette.ItemView.extend({
        _map: null,

        _mark: null,

        initialize: function (options) {
            var that = this;

            yaReady.done(function () {
                that._addMarkButton = new ymaps.control.Button({
                    data: {content: 'Выбрать адрес'},
                    options: {maxWidth: [200]}
                });

                that._map = new ymaps.Map(options.el, {
                    center: [59.939622, 30.315006], // СПб
                    zoom: 10,
                    controls: ['smallMapDefaultSet']
                });
                that._map.events.add('click', that.addMark.bind(that));
            });
        },

        addMark: function (event) {
            if (this._mark) {
                this._map.geoObjects.remove(this._mark);
            }

            this._mark = new ymaps.GeoObject({
                geometry: {
                    type: 'Point',
                    coordinates: event.get('coords')
                }
            }, {
                draggable: true
            });
            this._map.geoObjects.add(this._mark);
        },

        getAddress: function () {
            // todo: add parsing

            return {
                city: '',
                street: '',
                house: '',
                corpus: ''
            };
        },

        markSet: function () {
            return Boolean(this._mark);
        }
    });

    ymaps.ready(yaReady.resolve.bind(yaReady));
});