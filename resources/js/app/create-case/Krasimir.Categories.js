'use strict';

Krasimir.module('Categories', function (Categories, App, Backbone, Marionette) {
    // Models

    Categories.CollectionRow = Backbone.Model.extend({
        initialize: function (options) {
            this.set('data', new Backbone.Collection(options.data));
        }
    });

    Categories.Collection = Backbone.Collection.extend({
        url: '/api/cats',

        model: Categories.CollectionRow,

        parse: function (response) {
            var result = [],
                recentModel = null;

            for (var i = 0; i < response.data.length; i++) {
                if (i % 4 == 0) {
                    recentModel = { data: [] };
                }

                recentModel.data.push(response.data[i]);

                if ((i + 1) % 4 == 0) {
                    result.push(recentModel);
                }
            }

            return result;
        }
    });

    // Dropdown

    Categories.DropdownItemView = Marionette.ItemView.extend({
        template: '#template-dropdown-item-view',

        className: 'details-row',

        events: {
            click: 'setCategoryChosen'
        },

        setCategoryChosen: function () {
            App.viewModel.trigger('category:set', this.model);
            App.viewModel.trigger('step:set', 2);
        }
    });

    Categories.DropdownView = Marionette.CompositeView.extend({
        template: '#template-dropdown-view',

        className: 'categories-details',

        childView: Categories.DropdownItemView,

        childViewContainer: '.details-wrapper',

        ui: {
            container: '.details-wrapper'
        },

        initialize: function () {
            this.collection = new Backbone.Collection();

            this.on('open', this.open);
            this.on('close', this.close);
        },

        open: function (model) {
            if (this.categoryId) {
                this.$el.removeClass('category-' + this.categoryId + '-open');
            }

            this.categoryId = model.id;

            this.$el.addClass('category-' + this.categoryId + '-open');
            this.$el.addClass('details-open');

            this.recalculateHeight();
        },

        close: function () {
            this.$el.removeClass('details-open');

            this.el.style.height = 0;
        },

        recalculateHeight: function () {
            var height = this.ui.container.outerHeight();

            this.el.style.height = (height) + 'px';
        }
    });

    // Views

    Categories.ItemView = Marionette.ItemView.extend({
        triggers: {
            'click': 'category:toggle'
        },

        template: '#template-categories-item-view',

        className: 'category'
    });

    Categories.CategoryGroupView = Marionette.CompositeView.extend({
        template: '#template-categories-view',

        className: 'categories-wrapper',

        childViewContainer: '.categories-block',

        childView: Categories.ItemView,

        ui: {
            dropdown: '.categories-dropdown'
        },

        initialize: function (options) {
            this.collection = options.model.get('data');
        },

        onRender: function () {
            this.dropdownView = new Categories.DropdownView({
                el: this.ui.dropdown
            }).render();
        },

        onChildviewCategoryToggle: function (data) {
            this._parent.trigger('category:open', data.model);
        },

        toggleCategory: function (model) {
            if (this.currentCategory === model || !this.collection.contains(model)) {
                this.closeDropdown();
            } else {
                this.openDropdown(model);
            }
        },

        openDropdown: function (category) {
            this.currentCategory = category;

            this.dropdownView.collection.reset(category.get('categories').data);
            this.dropdownView.trigger('open', category);
        },

        closeDropdown: function () {
            this.currentCategory = null;
            this.dropdownView.trigger('close');
        }
    });

    Categories.MainView = Marionette.CollectionView.extend({
        childView: Categories.CategoryGroupView,

        initialize: function () {
            this.on('category:open', this.onChildviewCategoryOpen);
        },

        onChildviewCategoryOpen: function (model) {
            _.each(this.children._views, function (child) {
                child.toggleCategory(model);
            });
        }
    });
});