'use strict';

Krasimir.module('Details', function (Details, App, Backbone, Marionette) {
    Details.MainView = Marionette.ItemView.extend({
        alreadyRendered: false,

        mapView: null,

        events: {
            'click .blue-button': 'next',
            'click .input-type-address': 'setTypeAddress',
            'click .input-type-map': 'setTypeMap'
        },

        ui: {
            city: '.input-city',
            street: '.input-street',
            house: '.input-house',
            corpus: '.input-corpus',
            details: '.input-details'
        },

        type: 'map',

        initialize: function () {
            this.on('category:open', this.onChildviewCategoryOpen);
            this.listenTo(App.viewModel, 'step:set', this.renderMap);
            this.bindUIElements();
        },

        renderMap: function (step) {
            if (step != 2 || this.mapView) {
                return
            }

            this.mapView = new Krasimir.AddressMap.MainView({el: 'address_map'});
        },

        _getAddress: function () {
            return {
                city: this.ui.city.val(),
                street: this.ui.street.val(),
                house: this.ui.house.val(),
                corpus: this.ui.corpus.val(),
                details: this.ui.details.val()
            };
        },

        next: function () {
            App.viewModel.trigger('step:set', 3);
            App.viewModel.trigger('address:set', this._getAddress());
        },

        setTypeAddress: function () {
            this.type = 'address';

            this.$el.addClass('type-address').removeClass('type-map');
        },

        setTypeMap: function () {
            this.type = 'map';

            this.$el.addClass('type-map').removeClass('type-address');
        }
    });
});