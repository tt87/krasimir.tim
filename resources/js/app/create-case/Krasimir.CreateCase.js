'use strict';

Krasimir.module('CreateCase', function (CreateCase, App, Backbone, Marionette) {
    CreateCase.Letter = Authorized.Model.extend({
        urlRoot: '/api/cases',

        initialize: function () {
            this.listenTo(App.viewModel, 'category:set', this.setCategory);
            this.listenTo(App.viewModel, 'address:set', this.setAddress);
        },

        parse: function (response) {
            return response.data;
        },

        setCategory: function (category) {
            this.set('category', category.get('id'));
            this.save();
        },

        setAddress: function (address) {
            // todo: handle address correctly
            this.save({'address': JSON.stringify(address)}, {patch: true});
        }
    });

    CreateCase.Controller = Marionette.Controller.extend({
        start: function () {
            this.letter = new CreateCase.Letter();

            new App.StickyHeader.MainView({
                el: '.progress-container'
            });

            var categoriesCollection = new App.Categories.Collection();
            new App.Categories.MainView({
                el: '.case-type',
                collection: categoriesCollection
            }).render();
            categoriesCollection.fetch();

            new App.Details.MainView({
                el: '.case-details'
            });
        }
    });

    App.on('start', function () {
        var controller = new CreateCase.Controller();

        controller.start();
    });
});