'use strict';

Krasimir.module('StickyHeader', function (StickyHeader, App, Backbone, Marionette) {
    StickyHeader.MainView = Marionette.ItemView.extend({
        headerHeight: 53,

        events: {
            'click .step-1-point': 'step1',
            'click .step-2-point': 'step2',
            'click .step-3-point': 'step3'
        },

        initialize: function () {
            $(window).on('scroll', this.reactScrolling.bind(this));
            $(window).on('scroll', this.reactPosition.bind(this));

            this.listenTo(App.viewModel, 'step:set', this.setStep);

            this.invalidateSteps();
            this.currentStep = 1;
            this.maxStep = 1;
            this.$progress = this.$('.progress');
        },

        invalidateSteps: function () {
            this.$steps = [
                $(document).find('.step-1'),
                $(document).find('.step-2'),
                $(document).find('.step-3')
            ];
        },

        reactScrolling: function () {
            var position = $(window).scrollTop();
            this.$el.toggleClass('scroll', position > this.headerHeight);
        },

        _getStepPosition: function (step) {
            var stickerHeight = this.$progress.height(),
                padding = 30,
                stepPosition = this.$steps[step - 1].offset().top;

            return stepPosition - padding - stickerHeight;
        },

        reactPosition: function () {
            var windowPosition = window.scrollY,
                step = 1;

            for (var i = this.maxStep; i >= 1; i--) {
                var stepScroll = this._getStepPosition(i);

                if (windowPosition >= stepScroll) {
                    step = i;

                    break;
                }
            }

            if (this.currentStep != step) {
                this.$progress.removeClass('step-' + this.currentStep + '-now');
                this.currentStep = step;
                this.$progress.addClass('step-' + this.currentStep + '-now');
            }
        },

        setStep: function (step) {
            if (step > this.maxStep) {
                this.maxStep = step;
                this.$steps[step - 1].addClass('shown');
            }

            var position = this._getStepPosition(step);

            $(document.body).animate({scrollTop: position});
        },

        safelySetStep: function (step) {
            if (step > this.maxStep) {
                return;
            }

            this.setStep(step);
        },

        step1: function () {
            this.safelySetStep(1);
        },

        step2: function () {
            this.safelySetStep(2);
        },

        step3: function () {
            this.safelySetStep(3);
        }
    });
});