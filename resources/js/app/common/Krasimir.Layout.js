'use strict';

Krasimir.module('Layout', function (Layout, App, Backbone, Marionette) {
    Layout.Root = Marionette.LayoutView.extend({
        el: 'body',
        regions: {
            header: '#header'
        }
    });

    Layout.Controller = Marionette.Controller.extend({
        initialize: function () {
            this.user = new App.Model.User();
        },

        start: function () {
            this.showHeader(this.user);
            this.fetchAuthentication();
        },

        fetchAuthentication: function () {
            if (localStorage.getItem('token') !== null) {
                App.viewModel.trigger('token:set', localStorage.getItem('token'));
            }
        },

        showHeader: function (user) {
            var header = new App.Header.Root({
                model: user
            });
            App.root.showChildView('header', header);
        }
    });

    App.on('start', function () {
        var controller = new Layout.Controller();

        controller.start();
    });
});