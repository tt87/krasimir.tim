'use strict';

Krasimir.module('Modal', function (Modal, App, Backbone, Marionette) {
    var singleInstance = null;

    Modal.Root = function () {
        if (singleInstance == null) {
            singleInstance = new Modal.Main({
                el: '#body-modal'
            });
        }

        return singleInstance;
    };

    Modal.Main = Marionette.ItemView.extend({
        renderIn: function (item) {
            App.Overlay.Root().show();
            this.$el.empty();
            this.$el.show();
            this.$el.append(item.render().$el);
        },

        hide: function () {
            this.$el.empty();
            this.$el.hide();
            App.Overlay.Root().hide();
        }
    });
});