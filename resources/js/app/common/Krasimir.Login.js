'use strict';

Krasimir.module('Login', function (Login, App, Backbone, Marionette) {
    Login.Main = Marionette.ItemView.extend({
        template: '#template-login-form',

        className: 'login-form',

        ui: {
            'email': '.email-input',
            'password': '.psw-input'
        },

        events: {
            'click .login-button': 'performLogin'
        },

        performLogin: function () {
            var that = this;

            Backbone.ajax({
                url: '/api/login',
                method: 'POST',
                data: JSON.stringify({
                    email: this.ui.email.val(),
                    password: this.ui.password.val()
                })
            }).done(function (response) {
                that.setToken(response.data.key);
            });
        },

        setToken: function (token) {
            // set token
            localStorage.setItem('token', token);

            // trigger "refresh user info"
            App.viewModel.trigger('token:set', token);

            // close window
            App.Modal.Root().hide();
        }
    });
});