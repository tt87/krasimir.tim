'use strict';

Krasimir.module('Model', function (Model, App, Backbone, Marionette) {
    Model.User = Authorized.Model.extend({
        url: '/api/user/info',

        initialize: function () {
            this.listenTo(App.viewModel, 'token:set', this.updateData);
        },

        isAuthorized: function () {
            return !Boolean(this.error);
        },

        updateData: function (token) {
            if (token !== null) {
                this.fetch();
            } else {
                this.clear();
            }
        }
    });
});