'use strict';

window.Krasimir = (function () {
    var ViewModel = Backbone.Model.extend({
        initialize: function () {
            this.on('token:set', this.setToken.bind(this));
        },

        setToken: function (token) {
            this.set('token', token);
        }
    });

    var App = Backbone.Marionette.Application.extend({
        setRootLayout: function () {
            this.root = new Krasimir.Layout.Root();
        }
    });

    var krasimir = new App();
    krasimir.viewModel = new ViewModel();

    return krasimir;
})();

Krasimir.on('start', function () {
    Krasimir.setRootLayout();
    Backbone.history.start();
});

Marionette.TemplateCache.prototype.compileTemplate = function(rawTemplate) {
    return Handlebars.compile(rawTemplate);
};

document.addEventListener('DOMContentLoaded', function () {
    Krasimir.start();
});