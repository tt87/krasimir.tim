'use strict';

// use PUT for PATCH requests
Backbone.sync = (function (Backbone){
    var originalSync = Backbone.sync;

    return function (method, model, options) {
        if (method === 'patch') options.type = 'PUT';
        return originalSync(method, model, options);
    };
})(Backbone);

window.Authorized = {};
Authorized.Model = Backbone.Model.extend({
    fetch: function (options) {
        options = _.extend({
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-Auth-Token', Krasimir.viewModel.get('token'));
            }
        }, options);

        return Backbone.Model.prototype.fetch.call(this, options);
    },

    save: function (key, val, options) {
        // copy-pasted from "save" source
        var attrs;
        if (key == null || typeof key === 'object') {
            attrs = key;
            options = val;
        } else {
            (attrs = {})[key] = val;
        }

        options = _.extend({
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-Auth-Token', Krasimir.viewModel.get('token'));
            }
        }, options);

        return Backbone.Model.prototype.save.call(this, attrs, options);
    }
});

Authorized.ajax = function (options) {
    options = _.extend({
        beforeSend: function (xhr) {
            xhr.setRequestHeader('X-Auth-Token', Krasimir.viewModel.get('token'));
        }
    }, options);

    return Backbone.ajax.call(Backbone, options);
};