'use strict';

Krasimir.module('Overlay', function (Overlay, App, Backbone, Marionette) {
    var singleInstance = null;

    Overlay.Root = function (item) {
        if (singleInstance == null) {
            singleInstance = new Overlay.Main({
                el: '#overlay'
            });
        }
        return singleInstance;
    };

    Overlay.Main = Marionette.ItemView.extend({
        events: {
            'click': 'closeModal'
        },

        show: function () {
            this.$el.addClass('shown');
        },

        hide: function () {
            this.$el.removeClass('shown');
        },

        closeModal: function () {
            App.Modal.Root().hide();
        }
    });
});