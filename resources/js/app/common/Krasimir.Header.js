'use strict';

Krasimir.module('Header', function (Header, App, Backbone, Marionette) {
    Header.Root = Marionette.ItemView.extend({
        template: '#template-header',

        events: {
            'click .login-button': 'showLoginWindow',
            'click .logout': 'logout'
        },

        modelEvents: {
            'change': 'fieldsChanged'
        },

        fieldsChanged: function() {
            this.render();
        },

        showLoginWindow: function () {
            App.Modal.Root().renderIn(new App.Login.Main());
        },

        logout: function () {
            Authorized.ajax({
                url: '/api/logout',
                method: 'POST'
            }).done(function () {
                App.viewModel.trigger('token:set', null);
            });

            localStorage.removeItem('token');
        }
    });
});