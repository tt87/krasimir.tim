<?php
/**
 * An helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace {
/**
 * Admin
 *
 * @property integer $id
 * @property integer $user
 * @property integer $domain
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Admin whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Admin whereUser($value)
 * @method static \Illuminate\Database\Query\Builder|\Admin whereDomain($value)
 * @method static \Illuminate\Database\Query\Builder|\Admin whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Admin whereUpdatedAt($value)
 */
	class Admin {}
}

namespace {
/**
 * Moderator
 *
 * @property integer $id
 * @property integer $user
 * @property integer $region
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Moderator whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Moderator whereUser($value)
 * @method static \Illuminate\Database\Query\Builder|\Moderator whereRegion($value)
 * @method static \Illuminate\Database\Query\Builder|\Moderator whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Moderator whereUpdatedAt($value)
 */
	class Moderator {}
}

namespace {
/**
 * Photo
 *
 * @property integer $id
 * @property string $name
 * @property boolean $before
 * @property boolean $after
 * @property boolean $approved
 * @property integer $user_id
 * @property integer $problem_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \User $owner
 * @property-read \Problem $problem
 * @method static \Illuminate\Database\Query\Builder|\Photo whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Photo whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Photo whereBefore($value)
 * @method static \Illuminate\Database\Query\Builder|\Photo whereAfter($value)
 * @method static \Illuminate\Database\Query\Builder|\Photo whereApproved($value)
 * @method static \Illuminate\Database\Query\Builder|\Photo whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Photo whereProblemId($value)
 * @method static \Illuminate\Database\Query\Builder|\Photo whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Photo whereUpdatedAt($value)
 * @property string $path 
 * @method static \Illuminate\Database\Query\Builder|\Photo wherePath($value)
 */
	class Photo {}
}

namespace {
/**
 * ModerationRequest
 *
 * @property integer $id
 * @property integer $sender
 * @property integer $receiver
 * @property integer $problem
 * @property boolean $isActive
 * @property boolean $isProblemClosed
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\ModerationRequest whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\ModerationRequest whereSender($value)
 * @method static \Illuminate\Database\Query\Builder|\ModerationRequest whereReceiver($value)
 * @method static \Illuminate\Database\Query\Builder|\ModerationRequest whereProblem($value)
 * @method static \Illuminate\Database\Query\Builder|\ModerationRequest whereIsActive($value)
 * @method static \Illuminate\Database\Query\Builder|\ModerationRequest whereIsProblemClosed($value)
 * @method static \Illuminate\Database\Query\Builder|\ModerationRequest whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\ModerationRequest whereUpdatedAt($value)
 */
	class ModerationRequest {}
}

